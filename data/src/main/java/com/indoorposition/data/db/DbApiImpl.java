package com.indoorposition.data.db;

import android.content.Context;
import android.util.Log;

import com.indoorposition.data.entity.MapEntity;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by hayabusa on 04.04.2016.
 */
public class DbApiImpl implements DbApi {

    private Context context;

    public DbApiImpl(Context context) {
    }


    @Override
    public Observable<List<MapEntity>> mapEntityList() {
        return Observable.create(new Observable.OnSubscribe<List<MapEntity>>() {
            @Override
            public void call(Subscriber<? super List<MapEntity>> subscriber) {
                try {
                    ArrayList<MapEntity> emptyList =
                            new ArrayList<MapEntity>();
                    subscriber.onNext(emptyList);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    @Override
    public Observable<MapEntity> mapEntityById(int mapId) {
        Log.d("DbApi", "Create observable");
        return Observable.create(new Observable.OnSubscribe<MapEntity>() {
            @Override
            public void call(Subscriber<? super MapEntity> subscriber) {
                try {
                    MapEntity mapEntity = new MapEntity();
                    mapEntity.setMapId(3);
                    subscriber.onNext(mapEntity);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });

    }
}
