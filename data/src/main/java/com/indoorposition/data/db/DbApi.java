package com.indoorposition.data.db;

import com.indoorposition.data.entity.MapEntity;

import java.util.List;

import rx.Observable;

/**
 * Created by hayabusa on 04.04.2016.
 */
public interface DbApi {

    Observable<List<MapEntity>> mapEntityList();

    Observable<MapEntity> mapEntityById(final int mapId);

}
