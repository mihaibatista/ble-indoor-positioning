package com.indoorposition.data.repository.datasource;

import android.content.Context;

import com.indoorposition.data.db.DbApi;
import com.indoorposition.data.db.DbApiImpl;

/**
 * Created by hayabusa on 04.04.2016.
 */
public class MapDataStoreFactory {

    private final Context context;

    public MapDataStoreFactory(Context context) {
        this.context = context;
    }

    public MapDataStore create(int mapId) {
        MapDataStore mapDataStore = createDbMapDataStore();
        return mapDataStore;
    }

    public MapDataStore createDbMapDataStore() {
        DbApi dbApi = new DbApiImpl(context);
        DbMapDataStore dbMapDataStore = new DbMapDataStore(dbApi);
        return dbMapDataStore;
    }
}
