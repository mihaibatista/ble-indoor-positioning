package com.indoorposition.data.repository;

import android.content.Context;
import android.util.Log;

import com.indoorposition.data.entity.MapEntity;
import com.indoorposition.data.repository.datasource.MapDataStore;
import com.indoorposition.data.repository.datasource.MapDataStoreFactory;
import com.indoorposition.data.repository.datasource.MapRepository;

import java.util.List;

import rx.Observable;


/**
 * Created by hayabusa on 04.04.2016.
 */
public class MapDataRepository implements MapRepository {

    private final MapDataStoreFactory mapDataStoreFactory;
    private Context context;

    public MapDataRepository(Context context) {
        Log.d("MapDataRepo", "Constructor");
        this.context = context;
        mapDataStoreFactory = new MapDataStoreFactory(context);
    }

    @Override
    public Observable<List<MapEntity>> maps() {
        MapDataStore mapDataStore =
                this.mapDataStoreFactory.createDbMapDataStore();
        return mapDataStore.mapEntityList();
    }

    @Override
    public Observable<MapEntity> map(int mapId) {
        MapDataStore mapDataStore = this.mapDataStoreFactory.create(mapId);
        Observable<MapEntity> mapEntityObservable = mapDataStore.mapEntity(mapId);
        Log.d("MapDataRep", "Observable returned is " + mapEntityObservable);
        return mapEntityObservable;
    }
}
