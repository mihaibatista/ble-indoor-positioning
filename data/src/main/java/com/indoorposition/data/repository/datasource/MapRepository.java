package com.indoorposition.data.repository.datasource;

import com.indoorposition.data.entity.MapEntity;

import java.util.List;

import rx.Observable;

/**
 * Created by hayabusa on 03.04.2016.
 */
public interface MapRepository {

    /**
     * Get an {@link rx.Observable} which will emit a List of
     * {@link MapEntity}
     * @return
     */
    Observable<List<MapEntity>> maps();

    Observable<MapEntity> map(final int mapId);

}
