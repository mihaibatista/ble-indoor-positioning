package com.indoorposition.data.repository.datasource;

import com.indoorposition.data.db.DbApi;
import com.indoorposition.data.entity.MapEntity;

import java.util.List;

import rx.Observable;

/**
 * Created by hayabusa on 04.04.2016.
 */
public class DbMapDataStore implements MapDataStore {

    private DbApi dbApi;

    public DbMapDataStore(DbApi dbApi) {
        this.dbApi = dbApi;
    }

    @Override
    public Observable<List<MapEntity>> mapEntityList() {
        return this.dbApi.mapEntityList();
    }

    @Override
    public Observable<MapEntity> mapEntity(int mapId) {
        return this.dbApi.mapEntityById(mapId);
    }
}
