package com.indoorposition.data.repository.datasource;

import com.indoorposition.data.entity.MapEntity;

import java.util.List;

import rx.Observable;


/**
 * Created by hayabusa on 04.04.2016.
 */
public interface MapDataStore {

    Observable<List<MapEntity>> mapEntityList();

    Observable<MapEntity> mapEntity(final int mapId);

}
