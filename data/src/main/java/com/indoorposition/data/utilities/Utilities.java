package com.indoorposition.data.utilities;

import java.util.UUID;

/**
 * Created by hayabusa on 27.11.2015.
 */
public class Utilities {
    public static String convertFmHwVersion(byte[] bytes) {
        String fmHwVersion = String.format("%X%C%X%C%X", bytes[0],
                bytes[1], bytes[2], bytes[3], bytes[4]);
        return fmHwVersion;
    }

    public static String convertSysId(byte[] bytes) {
        String sysid = String.format("%02X%02X%02X%02X%02X%02X",
                bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5]);

        return sysid;
    }

    public static UUID convertUUID(byte[] bytes) {
        if (bytes.length == 16) {
            UUID uuid = UUID.fromString(String.format("%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
                    bytes[0], bytes[1], bytes[2], bytes[3], bytes[4], bytes[5], bytes[6], bytes[7], bytes[8], bytes[9],
                    bytes[10], bytes[11], bytes[12], bytes[13], bytes[14], bytes[15]));
            return uuid;
        } else {
            return null;
        }
    }
}
