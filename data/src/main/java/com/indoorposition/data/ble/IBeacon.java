package com.indoorposition.data.ble;

import android.bluetooth.BluetoothDevice;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Luci on 4/26/2014.
 */
public class IBeacon implements Parcelable {

    /**
     * Less than half a meter away
     */
    public static final int PROXIMITY_IMMEDIATE = 1;
    /**
     * More than half a meter away, but less than four meters away
     */
    public static final int PROXIMITY_NEAR = 2;
    /**
     * More than four meters away
     */
    public static final int PROXIMITY_FAR = 3;
    /**
     * No distance estimate was possible due to a bad RSSI value or measured TX power
     */
    public static final int PROXIMITY_UNKNOWN = 0;

    protected String proximityUuid;
    protected int major;
    protected int minor;
    protected Integer proximity;

    protected Double accuracy;
    protected int rssi;
    protected int txPower;
    protected String bluetoothAddress;
    private BluetoothDevice bluetoothDevice;

    /**
     * If multiple RSSI samples were available, this is the running average
     */
    protected Double runningAverageRssi = null;

    /**
     * A 16 bit integer typically used to represent a group of iBeacons
     */
    public int getMajor() {
        return major;
    }

    /**
     * A 16 bit integer that identifies a specific iBeacon within a group
     */
    public int getMinor() {
        return minor;
    }

    /**
     * An integer with four possible values representing a general idea of how far the iBeacon is away.
     */
    public int getProximity() {
        if (proximity == null) {
            proximity = IBeaconUtils.calculateBeaconProximity(getAccuracy());
        }
        return proximity;
    }

    /**
     * A double that is an estimate of how far the iBeacon is away in meters.
     */
    public double getAccuracy() {
        if (accuracy == null) {
            accuracy = IBeaconUtils.calculateBeaconDistance(txPower,
                    runningAverageRssi != null ? runningAverageRssi : rssi);
        }
        return accuracy;
    }

    /**
     * The measured signal strength of the Bluetooth packet that led do this iBeacon detection.
     */
    public int getRssi() {
        return rssi;
    }

    /**
     * The calibrated measured Tx power of the iBeacon in RSSI
     * This value is baked into an iBeacon when it is manufactured, and
     * it is transmitted with each packet to aid in the distance estimate
     */
    public int getTxPower() {
        return txPower;
    }

    /**
     * An integer with four possible values representing a general idea of how far the iBeacon is away.
     */
    public String getProximityUuid() {
        return proximityUuid;
    }

    @Override
    public int hashCode() {
        return minor;
    }

    /**
     * The bluetooth mac address
     */
    public String getBluetoothAddress() {
        return bluetoothAddress;
    }

    /**
     * Two detected iBeacons are considered equal if they share the same three identifiers, regardless of their
     * distance
     * or RSSI.
     */
    @Override
    public boolean equals(Object that) {
        if (!(that instanceof IBeacon)) {
            return false;
        }
        IBeacon thatIBeacon = (IBeacon) that;
        return (thatIBeacon.getMajor() == this.getMajor()
                && thatIBeacon.getMinor() == this.getMinor()
                && thatIBeacon.getProximityUuid().equals(this.getProximityUuid()));
    }

    @Override public String toString() {
        return "IBeacon{" +
                "proximityUuid='" + proximityUuid + '\'' +
                ", major=" + major +
                ", minor=" + minor +
                ", proximity=" + getProximity() +
                ", accuracy=" + getAccuracy() +
                ", rssi=" + rssi +
                ", txPower=" + txPower +
                ", bluetoothAddress='" + bluetoothAddress + '\'' +
                ", runningAverageRssi=" + runningAverageRssi +
                '}';
    }

    private IBeacon(Parcel in) {
        major = in.readInt();
        minor = in.readInt();
        proximityUuid = in.readString();
        proximity = in.readInt();
        accuracy = in.readDouble();
        rssi = in.readInt();
        txPower = in.readInt();
        bluetoothAddress = in.readString();
        bluetoothDevice = (BluetoothDevice) in.readValue(BluetoothDevice.class.getClassLoader());
    }

    protected IBeacon() {
    }

    protected IBeacon(IBeacon otherIBeacon) {
        this.major = otherIBeacon.major;
        this.minor = otherIBeacon.minor;
        this.accuracy = otherIBeacon.accuracy;
        this.proximity = otherIBeacon.proximity;
        this.rssi = otherIBeacon.rssi;
        this.proximityUuid = otherIBeacon.proximityUuid;
        this.txPower = otherIBeacon.txPower;
        this.bluetoothAddress = otherIBeacon.bluetoothAddress;
        this.bluetoothDevice = otherIBeacon.bluetoothDevice;
    }

    protected IBeacon(String proximityUuid, int major, int minor, int txPower, int rssi) {
        this.proximityUuid = proximityUuid.toLowerCase();
        this.major = major;
        this.minor = minor;
        this.rssi = rssi;
        this.txPower = txPower;
    }

    public IBeacon(String proximityUuid, int major, int minor) {
        this.proximityUuid = proximityUuid.toLowerCase();
        this.major = major;
        this.minor = minor;
        this.txPower = -59;
        this.rssi = 0;
    }

    @Override public int describeContents() {
        return 0;
    }

    @Override public void writeToParcel(Parcel out, int i) {
        out.writeInt(major);
        out.writeInt(minor);
        out.writeString(proximityUuid);
        out.writeInt(getProximity());
        out.writeDouble(getAccuracy());
        out.writeInt(rssi);
        out.writeInt(txPower);
        out.writeString(bluetoothAddress);
        out.writeValue(bluetoothDevice);
    }

    public static final Creator<IBeacon> CREATOR
            = new Creator<IBeacon>() {
        public IBeacon createFromParcel(Parcel in) {
            return new IBeacon(in);
        }

        public IBeacon[] newArray(int size) {
            return new IBeacon[size];
        }
    };

    @Override public Object clone() {
        try {
            super.clone();
        }
        catch (CloneNotSupportedException e) {
        } finally {
            return new IBeacon(this);
        }
    }

    public BluetoothDevice getBluetoothDevice() {
        return bluetoothDevice;
    }

    public void setBluetoothDevice(BluetoothDevice bluetoothDevice) {
        this.bluetoothDevice = bluetoothDevice;
    }
}
