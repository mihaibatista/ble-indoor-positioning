package com.indoorposition.data.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

//import me.around.bleindoor.db.DbManager;


/**
 * Created by hayabusa on 23.08.2015.
 */
public class BleScanner {
    private final static int SCAN_PERIOD = 60000;
    public BluetoothManager bleManager;
    public BluetoothAdapter bleAdapter;
    public Handler handler;
    private boolean isScanning = false;
    private BleListener bleListener;
    private Gson gson = new Gson();
    private Context mContext;
    private HashMap<String, IBeacon> foundBeacons = new HashMap<>();
    //private DbManager dbManager;
    private ScannerToMapListener scannerToMapListener;
    private int counter = 0;
    private ArrayList<IBeacon> foundDuringScanInterval = new ArrayList<>();

    private ScheduledExecutorService mWorker1 = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> mScheduledFutureScan;
    private ArrayList<Observer<List<IBeacon>>> subscribers = new ArrayList<>();

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            IBeacon beacon = BeaconFactory.fromScanData(bytes, i, bluetoothDevice);
            foundBeacons.put(beacon.getMajor() + "/" + beacon.getMinor(), beacon);
            //dbManager.addBeaconInDb(beacon);
            Log.d("BLE", "Found beacon " + gson.toJson(beacon));
            /*if (bleListener != null) {
                bleListener.onBeaconFound(beacon);
            }*/
            /*if (scannerToMapListener != null) {
                if (counter == 0) {
                    scannerToMapListener.onBeaconFound(beacon.getMajor(), beacon.getMinor());
                }
                ++counter;
            }*/

            synchronized (foundDuringScanInterval) {
                foundDuringScanInterval.add(beacon);
            }
        }
    };

    // Singleton
    public BleScanner(Context context) {
        bleManager = (BluetoothManager) context.getSystemService(context.BLUETOOTH_SERVICE);
        bleAdapter = bleManager.getAdapter();
        //dbManager = new DbManager(context);
        handler = new Handler();

        if (bleAdapter != null && bleAdapter.isEnabled()) {

        }
        Log.d("BLE", "BleScanner initialized");
    }

    public void addBeaconMonitor(Observer<List<IBeacon>> subscriber) {
        subscribers.add(subscriber);
    }

    public void scanLeDevice(boolean enable) {
        Log.d("BLE", "Scan LE devices. Enable " + enable);
        if (enable) {
            /*handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isScanning = false;
                    bleAdapter.stopLeScan(leScanCallback);
                    Log.d("BLE", "Stop scan");
                    bleListener.onScanStop();
                }
            }, SCAN_PERIOD);*/
            mScheduledFutureScan = mWorker1.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d("ScanResult", "Check scan result");
                        synchronized (foundDuringScanInterval) {
                            final ArrayList<IBeacon> clonedBeacons = (ArrayList<IBeacon>) foundDuringScanInterval.clone();

                            Observable<List<IBeacon>> observable = Observable.create(new Observable.OnSubscribe<List<IBeacon>>() {
                                @Override
                                public void call(Subscriber<? super List<IBeacon>> subscriber) {
                                    try {
                                        subscriber.onCompleted();
                                        subscriber.onNext(clonedBeacons);

                                    } catch (Exception e) {
                                        subscriber.onError(e);
                                    }
                                }
                            });
                            //Log.d("ScanResult", "No of result subscribers is " + subscribers.size());
                            for (Observer subscriber : subscribers) {
                                observable.subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribe(subscriber);
                            }

                            //scannerToMapListener.onScanResult(clonedBeacons);
                            foundDuringScanInterval.clear();
                        }
                    } catch (Exception e) {
                        Log.e("error", e.getMessage());
                        e.printStackTrace();
                    }
                    //}
                }
            }, 1500, 4000, TimeUnit.MILLISECONDS);

            isScanning = true;
            bleAdapter.startLeScan(leScanCallback);
            Log.d("BLE", "Start scan");
        } else {
            isScanning = false;
            bleAdapter.stopLeScan(leScanCallback);
            Log.d("BLE", "Stop scan");
            if (mScheduledFutureScan != null)
                mScheduledFutureScan.cancel(true);
        }
    }

    public IBeacon getBeaconByIds(int major, int minor) {
        String beaconId = major + "/" + minor;
        if (foundBeacons.containsKey(beaconId)) {
            return foundBeacons.get(beaconId);
        } else {
            return null;
        }
    }

    public void addBleListener(BleListener listener) {
        bleListener = listener;
    }

    public void addScannerToMapListener(ScannerToMapListener scannerToMapListener) {
        this.scannerToMapListener = scannerToMapListener;
    }

    public interface ScannerToMapListener {
        public void onBeaconFound(int major, int minor);

        public void onScanResult(ArrayList<IBeacon> beacons);
    }

}
