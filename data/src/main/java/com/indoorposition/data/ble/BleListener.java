package com.indoorposition.data.ble;

/**
 * Created by hayabusa on 23.08.2015.
 */
public interface BleListener {
    public void onBeaconFound(IBeacon bleDevice);

    public void onScanStop();

}
