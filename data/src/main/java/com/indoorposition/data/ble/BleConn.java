package com.indoorposition.data.ble;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;

import com.indoorposition.data.utilities.Utilities;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by hayabusa on 17.04.2016.
 */
public class BleConn {
    private Context mContext;
    private BluetoothManager bleManager;
    private BluetoothAdapter bleAdapter;
    private HashMap<String, AbstractMap.SimpleEntry<IBeacon, String>> mBeaconsHash =
            new HashMap<String, AbstractMap.SimpleEntry<IBeacon, String>>();
    private BluetoothGatt mBluetoothGatt;
    ScheduledFuture<?> mScheduledFuture;
    private boolean mReadingCharacteristic = false;
    private boolean mConnectionInProgress = false;
    private String mCurrentCommand;
    private IBeacon mCurrentBeacon;

    // Listener
    private CharacteristicsListener charListener;

    private ArrayList<BluetoothGattCharacteristic> mCharacteristicsList
            = new ArrayList<BluetoothGattCharacteristic>();
    private ScheduledExecutorService mWorker = Executors.newSingleThreadScheduledExecutor();
    private static int CONNECTION_TIMEOUT = 40;

    // Commands
    private static final String COMMAND_READ_INFO = "read device info";

    // Beacon services
    private static final UUID ONYX_BEACON_SERVICE =
            UUID.fromString("2aaceb00-c5a5-44fd-0000-3fd42d703a4f");
    // Beacon characteristics
    private static final UUID UUID_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0100-3fd42d703a4f");
    private static final UUID MAJOR_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0200-3fd42d703a4f");
    private static final UUID MINOR_CHARACTERISITC =
            UUID.fromString("2aaceb00-c5a5-44fd-0300-3fd42d703a4f");
    private static final UUID BATTERY_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0400-3fd42d703a4f");
    private static final UUID SYSID_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0500-3fd42d703a4f");
    private static final UUID FIRMWARE_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0600-3fd42d703a4f");
    private static final UUID HARDWARE_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0700-3fd42d703a4f");
    private static final UUID POWER_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0900-3fd42d703a4f");
    private static final UUID FREQUENCY_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0a00-3fd42d703a4f");

    public BleConn(Context context) {
        bleManager = (BluetoothManager) context.getSystemService(context.BLUETOOTH_SERVICE);
        bleAdapter = bleManager.getAdapter();
        mContext = context;
    }

    public void readCharacteristicsForDevice(IBeacon beacon) {
        mBeaconsHash.put(beacon.getBluetoothAddress(),
                new AbstractMap.SimpleEntry<IBeacon, String>(beacon, COMMAND_READ_INFO));
        executeCommand();
    }

    private void readCharacteristic() {
        if (!mReadingCharacteristic) {
            if (mCharacteristicsList.size() > 0) {
                if (mBluetoothGatt != null) {
                    mBluetoothGatt.readCharacteristic(mCharacteristicsList.get(0));
                    mReadingCharacteristic = true;
                } else {
                    mReadingCharacteristic = false;
                }
            }
        }
    }

    private void connectToDevice(IBeacon beacon) {
        BluetoothDevice device = bleAdapter.getRemoteDevice(beacon.getBluetoothAddress());
        device.connectGatt(mContext, false, mGattCallback);
    }

    private void disconnectDevice() {
        mConnectionInProgress = false;
        mBeaconsHash.remove(mBluetoothGatt.getDevice().getAddress());
        if (mBluetoothGatt != null) mBluetoothGatt.disconnect();
        if (mScheduledFuture != null) mScheduledFuture.cancel(true);
        //mOnyxBeaconService.registerProximityUUDIDs(false);
    }

    private void finishReadingCharacteristicsForDeviceSuccess(IBeacon beacon) {
        mReadingCharacteristic = false;

        disconnectDevice();
        executeCommand();
        //if (mBleListener != null) mBleListener.onCharacteristicsReadFinished(beacon, beaconInfo);
        //sendSysInfo(beaconInfo);
    }

    private void finishReadingCharacteristicsForDeviceFailed(String bluetoothAddress) {
        mReadingCharacteristic = false;

        disconnectDevice();
        executeCommand();
    }



    @SuppressLint("NewApi")
    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        disableScanningAfterDelay();
                        mBluetoothGatt = gatt;

                        String deviceName = gatt.getDevice().getName();
                        if (deviceName != null && deviceName.equals("OnyxBeacon")) {
                            gatt.discoverServices();
                        }
                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        disconnectDevice();
                        executeCommand();
                    } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                    } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {
                    }
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        if (mCurrentCommand.equals(COMMAND_READ_INFO)) {
                            BluetoothGattService service = gatt.getService(ONYX_BEACON_SERVICE);
                            if (service != null) {
                                if (service.getCharacteristics().size() != 0) {
                                    BluetoothGattCharacteristic batteryCharacteristic = service.getCharacteristic(BATTERY_CHARACTERISTIC);
                                    BluetoothGattCharacteristic frequencyCharacteristic = service.getCharacteristic(FREQUENCY_CHARACTERISTIC);
                                    BluetoothGattCharacteristic powerCharacteristic = service.getCharacteristic(POWER_CHARACTERISTIC);
                                    BluetoothGattCharacteristic hardwareCharacteristic = service.getCharacteristic(HARDWARE_CHARACTERISTIC);
                                    BluetoothGattCharacteristic firmwareCharacteristic = service.getCharacteristic(FIRMWARE_CHARACTERISTIC);
                                    BluetoothGattCharacteristic sysidCharacteristic = service.getCharacteristic(SYSID_CHARACTERISTIC);

                                    if (!(batteryCharacteristic == null)) {
                                        mCharacteristicsList.add(batteryCharacteristic);
                                    }
                                    if (!(frequencyCharacteristic == null)) {
                                        mCharacteristicsList.add(frequencyCharacteristic);
                                    }
                                    if (!(powerCharacteristic == null)) {
                                        mCharacteristicsList.add(powerCharacteristic);
                                    }
                                    if (!(hardwareCharacteristic == null)) {
                                        mCharacteristicsList.add(hardwareCharacteristic);
                                    }
                                    if (!(firmwareCharacteristic == null)) {
                                        mCharacteristicsList.add(firmwareCharacteristic);
                                    }
                                    if (!(sysidCharacteristic == null)) {
                                        mCharacteristicsList.add(sysidCharacteristic);
                                    }
                                    readCharacteristic();
                                } else {
                                    finishReadingCharacteristicsForDeviceFailed(gatt.getDevice().getAddress());
                                }

                            } else {
                                finishReadingCharacteristicsForDeviceFailed(gatt.getDevice().getAddress());
                            }
                        }
                    } else {
                        finishReadingCharacteristicsForDeviceFailed(gatt.getDevice().getAddress());
                    }
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                    AbstractMap.SimpleEntry<IBeacon, String> beaconData =
                            mBeaconsHash.get(gatt.getDevice().getAddress());
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        if (characteristic.getUuid().toString().equals(BATTERY_CHARACTERISTIC.toString())) {
                            int batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("Battery", String.valueOf(batteryLevel));
                            }
                        } else if (characteristic.getUuid().toString().equals(UUID_CHARACTERISTIC.toString())) {
                            byte[] uuidAsBytes = characteristic.getValue();
                            UUID uuid = Utilities.convertUUID(uuidAsBytes);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("UUID", uuid.toString());
                            }
                        } else if (characteristic.getUuid().toString().equals(FREQUENCY_CHARACTERISTIC.toString())) {
                            int frequency = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("Frequency", String.valueOf(frequency));
                            }
                        } else if (characteristic.getUuid().toString().equals(POWER_CHARACTERISTIC.toString())) {
                            int power = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("Power", String.valueOf(power));
                            }
                        } else if (characteristic.getUuid().toString().equals(HARDWARE_CHARACTERISTIC.toString())) {
                            byte[] hardwareAsBytes = characteristic.getValue();
                            String hardware = Utilities.convertFmHwVersion(hardwareAsBytes);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("Hardware version", hardware);
                            }
                        } else if (characteristic.getUuid().toString().equals(FIRMWARE_CHARACTERISTIC.toString())) {
                            byte[] firmwareAsBytes = characteristic.getValue();
                            String firmware = Utilities.convertFmHwVersion(firmwareAsBytes);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("Firmware version", firmware);
                            }
                        } else if (characteristic.getUuid().toString().equals(SYSID_CHARACTERISTIC.toString())) {
                            byte[] sysidAsBytes = characteristic.getValue();
                            String sysId = Utilities.convertSysId(sysidAsBytes);
                            if (charListener != null) {
                                charListener.onCharacteristicRead("SysId", sysId);
                            }
                            finishReadingCharacteristicsForDeviceSuccess(beaconData.getKey());
                        }
                        mCharacteristicsList.remove(0);
                        mReadingCharacteristic = false;
                        readCharacteristic();
                    }
                }

                @Override
                public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                    super.onCharacteristicChanged(gatt, characteristic);
                }
            };


    private void disableScanningAfterDelay() {
        Runnable disableScanning = new Runnable() {
            public void run() {
                mConnectionInProgress = false;
                mReadingCharacteristic = false;
                //if (mBluetoothGatt != null) mBluetoothGatt.close();
                if (mBluetoothGatt != null) mBluetoothGatt.disconnect();
                mBeaconsHash.remove(mBluetoothGatt.getDevice().getAddress());
                //mOnyxBeaconService.registerProximityUUDIDs(false);
                executeCommand();
            }
        };
        mScheduledFuture = mWorker.schedule(disableScanning, CONNECTION_TIMEOUT, TimeUnit.SECONDS);

    }

    private void executeCommand() {
        if (!mConnectionInProgress) {
            if (mBeaconsHash.size() > 0) {
                //mOnyxBeaconService.unregisterProximityUUIDs();

                mConnectionInProgress = true;
                mCurrentBeacon = mBeaconsHash.entrySet().iterator().next().getValue().getKey();
                mCurrentCommand = mBeaconsHash.entrySet().iterator().next().getValue().getValue();

                connectToDevice(mCurrentBeacon);
            }
        }
    }

    public void addCharListener(CharacteristicsListener listener) {
        charListener = listener;
    }

    public interface CharacteristicsListener {
        public void onCharacteristicRead(String characteristic, String value);
    }
}
