package com.indoorposition.data.entity;

/**
 * Created by hayabusa on 04.04.2016.
 */
public class MapEntity {

    private int mapId;

    public MapEntity() {

    }

    public int getMapId() {
        return mapId;
    }

    public void setMapId(int mapId) {
        this.mapId = mapId;
    }
}
