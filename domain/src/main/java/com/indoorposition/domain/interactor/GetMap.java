package com.indoorposition.domain.interactor;

import com.indoorposition.domain.executor.PostExecutionThread;
import com.indoorposition.domain.executor.ThreadExecutor;
import com.indoorposition.domain.repository.MapRepository;

import rx.Observable;

/**
 * Created by hayabusa on 03.04.2016.
 */
public class GetMap extends UseCase {

    private final int mapId;
    private final MapRepository mapRepository;

    public GetMap(int mapId,
                  ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread) {
        super(threadExecutor, postExecutionThread);
        this.mapId = mapId;
        mapRepository = new M;
    }

    @Override
    protected Observable buildUseCaseObservable() {
        return this.mapRepository.map(mapId);
    }
}
