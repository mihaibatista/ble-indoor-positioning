package com.indoorposition.domain;

/**
 * Created by hayabusa on 03.04.2016.
 */
public class FloorMap {
    private final int mapId;

    public FloorMap(int mapId) {
        this.mapId = mapId;
    }

    public int getMapId() {
        return mapId;
    }
}
