package com.indoorposition.domain.executor;

import java.util.concurrent.Executor;

/**
 * Created by hayabusa on 02.04.2016.
 */
public interface ThreadExecutor extends Executor {
}
