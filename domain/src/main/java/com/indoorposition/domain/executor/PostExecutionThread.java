package com.indoorposition.domain.executor;

import rx.Scheduler;

/**
 * Created by hayabusa on 02.04.2016.
 */
public interface PostExecutionThread {
    Scheduler getScheduler();
}
