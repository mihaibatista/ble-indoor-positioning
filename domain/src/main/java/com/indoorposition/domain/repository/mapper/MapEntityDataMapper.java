package com.indoorposition.domain.repository.mapper;


import com.indoorposition.domain.FloorMap;

import java.util.List;

/**
 * Created by hayabusa on 04.04.2016.
 */
public class MapEntityDataMapper {

    public MapEntityDataMapper() { }

    public FloorMap transform(MapEntity mapEntity) {
        FloorMap floorMap = null;
        if (mapEntity != null) {
            floorMap = new FloorMap(mapEntity.getMapId());
        }
        return floorMap;
    }

    public List<FloorMap> transform(Collection<MapEntity> mapEntityCollection) {
        List<FloorMap> mapList = new ArrayList<>();
        FloorMap floorMap;
        for (MapEntity mapEntity : mapEntityCollection) {
            floorMap = transform(mapEntity);
            if (floorMap != null) {
                mapList.add(floorMap);
            }
        }
        return mapList;
    }
}
