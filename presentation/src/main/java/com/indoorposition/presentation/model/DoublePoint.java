package com.indoorposition.presentation.model;

/**
 * Created by hayabusa on 12.06.2016.
 */
public class DoublePoint {
    public double x;
    public double y;

    public DoublePoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
