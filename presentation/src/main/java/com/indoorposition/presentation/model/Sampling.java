package com.indoorposition.presentation.model;

import com.indoorposition.presentation.db.model.RInteger;
import com.indoorposition.presentation.db.model.RList;
import com.indoorposition.presentation.db.model.RSampling;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class Sampling implements Serializable {
    private UIBeacon beacon;
    private ArrayList<ArrayList<Integer>> rssiList;

    public Sampling(UIBeacon beacon) {
        this.beacon = beacon;
        rssiList = new ArrayList<>();
    }

    public Sampling(RSampling rSampling) {
        this.beacon = new UIBeacon(rSampling.getBeacon());
        this.rssiList = new ArrayList<>();
        RealmList<RList> rRssiList = rSampling.getRssiList();
        for (RList rRssi : rRssiList) {
            ArrayList<Integer> rssiL = new ArrayList<>();
            for (RInteger rInt : rRssi.getRssiList()) {
                rssiL.add(rInt.getValue());
            }
            rssiList.add(rssiL);
        }
    }

    public double computeAverageRssi() {
        if (rssiList.size() == 0) {
            return 0;
        } else {
            ArrayList<Double> averageL = new ArrayList<>();
            for (ArrayList<Integer> rssiL : rssiList) {
                int i = 0;
                int rssiSum = 0;
                for (Integer rssi : rssiL) {
                    rssiSum += rssi;
                    ++i;
                }
                if (i > 0) {
                    averageL.add((double) rssiSum / (double) i);
                }
            }
            double rssiSumDouble = 0.0;
            int i = 0;
            for (Double avg : averageL) {
                rssiSumDouble += avg;
                ++i;
            }

            if (i > 0) {
                return rssiSumDouble / i;
            } else {
                return  -1;
            }
        }
    }

    public double computeBestAverageRssi() {
        if (rssiList.size() == 0) {
            return 0;
        } else {
            ArrayList<Double> averageL = new ArrayList<>();
            double minAvg = Integer.MAX_VALUE;
            for (ArrayList<Integer> rssiL : rssiList) {
                int i = 0;
                int rssiSum = 0;
                for (Integer rssi : rssiL) {
                    rssiSum += rssi;
                    ++i;
                }
                if (i > 0) {
                    double rssiAvg = (double) rssiSum / (double) i;
                    if (rssiAvg < minAvg) {
                        minAvg = rssiAvg;
                    }
                }
            }
            return minAvg;
        }
    }

    public UIBeacon getBeacon() {
        return beacon;
    }

    public ArrayList<ArrayList<Integer>> getRssiList() {
        return rssiList;
    }
}
