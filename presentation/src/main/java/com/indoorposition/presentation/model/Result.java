package com.indoorposition.presentation.model;

import java.util.ArrayList;

/**
 * Created by hayabusa on 12.06.2016.
 */
public class Result {
    private UIPoint computedPosition;
    private UIPoint realPosition;
    private int distance;
    private ArrayList<UIBeacon> beaconsUsed;
    private ArrayList<Integer> realDistanceToBeacons;
    private ArrayList<Integer> computedDistanceToBeacons;

    public Result(UIPoint computedPosition,
                  UIPoint realPosition,
                  int distance,
                  ArrayList<UIBeacon> beaconsUsed,
                  ArrayList<Integer> realDistanceToBeacons,
                  ArrayList<Integer> computedDistanceToBeacons) {
        this.computedPosition = new UIPoint(computedPosition.x, computedPosition.y);
        this.realPosition = new UIPoint(realPosition.x, realPosition.y);
        this.distance = distance;
        this.beaconsUsed = new ArrayList<>();
        for (UIBeacon beacon : beaconsUsed) {
            UIBeacon b = new UIBeacon(beacon.getMajor(), beacon.getMinor(),
                    beacon.getPositionPx());
            ArrayList<Double> distanceList = new ArrayList<>();
            for (Double d : beacon.getDistanceList()) {
                distanceList.add(d);
            }
            b.setDistance(distanceList);
            this.beaconsUsed.add(b);
        }
        this.realDistanceToBeacons = new ArrayList<>();
        for (Integer dist : realDistanceToBeacons) {
            this.getRealDistanceToBeacons().add(dist);
        }
        this.computedDistanceToBeacons = new ArrayList<>();
        for (Integer dist : computedDistanceToBeacons) {
            this.getComputedDistanceToBeacons().add(dist);
        }
    }

    public UIPoint getComputedPosition() {
        return computedPosition;
    }

    public UIPoint getRealPosition() {
        return realPosition;
    }

    public int getDistance() {
        return distance;
    }

    public ArrayList<UIBeacon> getBeaconsUsed() {
        return beaconsUsed;
    }

    public ArrayList<Integer> getRealDistanceToBeacons() {
        return realDistanceToBeacons;
    }

    public ArrayList<Integer> getComputedDistanceToBeacons() {
        return computedDistanceToBeacons;
    }
}
