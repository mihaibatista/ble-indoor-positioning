package com.indoorposition.presentation.model;

import android.graphics.Point;

/**
 * Created by hayabusa on 21.04.2016.
 */
public class UIEntry {
    private Point start;
    private Point end;

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }
}
