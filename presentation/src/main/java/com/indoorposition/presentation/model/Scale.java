package com.indoorposition.presentation.model;

import com.indoorposition.presentation.db.model.RScale;

import java.io.Serializable;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class Scale implements Serializable {
    private UIPoint startPoint = null;
    private UIPoint endPoint = null;
    private int length = -1;

    public Scale() {}

    public Scale(RScale rScale) {
        startPoint = new UIPoint(rScale.getStartPoint().getX(),
                rScale.getStartPoint().getY());
        endPoint = new UIPoint(rScale.getEndPoint().getX(),
                rScale.getEndPoint().getY());
        length = rScale.getLength();
    }

    public void reset() {
        startPoint = null;
        endPoint = null;
        length = -1;
    }

    public UIPoint getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(UIPoint startPoint) {
        this.startPoint = startPoint;
    }

    public UIPoint getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(UIPoint endPoint) {
        this.endPoint = endPoint;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public boolean isValid() {
        if (startPoint != null &&
                endPoint != null &&
                length > -1) {
            return true;
        } else {
            return false;
        }
    }

    private double computeDistanceBetweenTwoPointsPx(UIPoint a, UIPoint b) {
        double distance = (int) Math.sqrt(Math.pow(Math.abs(a.x - b.x), 2) +
                Math.pow(Math.abs(a.y - b.y), 2));
        return distance;
    }

    public int computeDistanceBetweenTwoPointsInCm(UIPoint a, UIPoint b) {
        double standardDistancePx = computeDistanceBetweenTwoPointsPx(startPoint, endPoint);
        double distancePx = computeDistanceBetweenTwoPointsPx(a, b);

        double distanceInCm = distancePx * length / standardDistancePx;
        return (int) distanceInCm;
    }

    public int transformCmInPx(int distance) {
        double standardDistancePx = computeDistanceBetweenTwoPointsPx(startPoint, endPoint);
        double distanceInPx = distance * standardDistancePx / length;
        return  (int) distanceInPx;
    }

    public int transformPxInCm(int distance) {
        double standardDistancePx = computeDistanceBetweenTwoPointsPx(startPoint, endPoint);
        double distanceInCm = distance * length / standardDistancePx;
        return (int) distanceInCm;
    }
}
