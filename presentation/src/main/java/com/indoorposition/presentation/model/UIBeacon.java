package com.indoorposition.presentation.model;

import com.indoorposition.presentation.ble.BeaconData;
import com.indoorposition.presentation.ble.IBeacon;
import com.indoorposition.presentation.db.model.RPoint;
import com.indoorposition.presentation.db.model.RUIBeacon;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by hayabusa on 26.11.2015.
 */
public class UIBeacon implements Serializable {
    private int major = -1;
    private int minor = -1;
    private int samplingStrength = -1;
    private UIPoint positionPx;
    private UIPoint positionCm;
    private UIPoint beaconCenter;
    private IBeacon beacon;
    private int battery = -1;
    private int frequency = -1;
    private int power = -1;
    private String hardware = "";
    private String firmware = "";
    private String sysId = "";
    private ArrayList<Double> distance = new ArrayList<>();
    private int beaconWidth = 50;

    public UIBeacon() {}

    public UIBeacon(int major, int minor,
                    UIPoint positionPx) {
        this.major = major;
        this.minor = minor;
        this.positionPx = positionPx;
    }

    public UIBeacon(int major, int minor) {
        this.major = major;
        this.minor = minor;
    }

    public UIBeacon(RUIBeacon ruiBeacon) {
        this.major = ruiBeacon.getMajor();
        this.minor = ruiBeacon.getMinor();
        this.samplingStrength = ruiBeacon.getSamplingStrength();
        RPoint rPositionPx = ruiBeacon.getPositionPx();
        if (rPositionPx != null) {
            UIPoint positionPx = new UIPoint(rPositionPx.getX(), rPositionPx.getY());
            this.positionPx = positionPx;
        }
        RPoint rPositionCm = ruiBeacon.getPositionCm();
        if (rPositionCm != null) {
            UIPoint positionCm = new UIPoint(rPositionCm.getX(), rPositionCm.getY());
            this.positionCm = positionCm;
        }
        this.battery = ruiBeacon.getBattery();
        this.frequency = ruiBeacon.getFrequency();
        this.power = ruiBeacon.getPower();
        this.hardware = ruiBeacon.getHardware();
        this.firmware = ruiBeacon.getFirmware();
        this.sysId = ruiBeacon.getSysId();
    }

    public UIBeacon(UIPoint positionPx) {
        this.positionPx = positionPx;
    }

    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }

    public int getSamplingStrength() {
        return samplingStrength;
    }

    public void setSamplingStrength(int samplingStrength) {
        this.samplingStrength = samplingStrength;
    }

    public UIPoint getPositionPx() {
        return positionPx;
    }

    public void setPositionPx(UIPoint positionPx) {
        this.positionPx = positionPx;
    }

    public IBeacon getBeacon() {
        return beacon;
    }

    public void setBeacon(IBeacon beacon) {
        this.beacon = beacon;
    }

    public void setBeaconData(BeaconData beaconData) {
        if (beaconData.major == major &&
                beaconData.minor == minor) {
            battery = beaconData.battery;
            frequency = beaconData.frequency;
            power = beaconData.power;
            hardware = beaconData.hardware;
            firmware = beaconData.firmware;
            sysId = beaconData.sysId;
        }
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public UIPoint getPositionCm() {
        return positionCm;
    }

    public void setPositionCm(UIPoint positionCm) {
        this.positionCm = positionCm;
    }

    public ArrayList<Double> getDistanceList() {
        return distance;
    }

    public void setDistance(ArrayList<Double> distance) {
        this.distance = distance;
    }

    public UIPoint getBeaconCenter() {
        return beaconCenter;
    }

    public void setBeaconCenter(UIPoint beaconCenter) {
        this.beaconCenter = beaconCenter;
    }
}
