package com.indoorposition.presentation.model;

import com.indoorposition.presentation.db.model.RFingerprint;
import com.indoorposition.presentation.db.model.RSampling;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class Fingerprint implements Serializable {
    private int xCell;
    private int yCell;
    private int xCellCenter;
    private int yCellCenter;
    private ArrayList<Sampling> samplings;
    private boolean ready;
    private double rssiDiff;

    public Fingerprint(int xCell, int yCell,
                       int xCellCenter, int yCellCenter) {
        this.xCell = xCell;
        this.yCell = yCell;
        this.xCellCenter = xCellCenter;
        this.yCellCenter = yCellCenter;
        samplings = new ArrayList<>();
        ready = false;
    }

    public Fingerprint(RFingerprint rFingerprint) {
        this.xCell = rFingerprint.getxCell();
        this.yCell = rFingerprint.getyCell();
        this.xCellCenter = rFingerprint.getxCellCenter();
        this.yCellCenter = rFingerprint.getyCellCenter();
        this.samplings = new ArrayList<>();
        RealmList<RSampling> rSamplings = rFingerprint.getSamplings();
        for (RSampling rSampling : rSamplings) {
            this.samplings.add(new Sampling(rSampling));
        }

    }

    public int getxCell() {
        return xCell;
    }

    public int getyCell() {
        return yCell;
    }

    public int getxCellCenter() {
        return xCellCenter;
    }

    public int getyCellCenter() {
        return yCellCenter;
    }

    public ArrayList<Sampling> getSamplings() {
        return samplings;
    }

    public boolean isReady(int noOfSamples, int noOfBeacons) {
        boolean isReady = true;
        if (samplings.size() == noOfBeacons) {
            for (Sampling sampling : samplings) {
                for (ArrayList<Integer> rssiList : sampling.getRssiList()) {
                    if (rssiList.size() < noOfSamples) {
                        isReady = false;
                    }
                }
            }
        } else {
            isReady = false;
        }
        return isReady;
    }

    public Sampling getSamplingForBeacon(int major, int minor) {
        Sampling foundSampling = null;
        for (Sampling sampling : samplings) {
            if (sampling.getBeacon().getMajor() == major &&
                    sampling.getBeacon().getMinor() == minor) {
                foundSampling = sampling;
            }
        }
        return foundSampling;
    }

    public double getRssiDiff() {
        return rssiDiff;
    }

    public void setRssiDiff(double rssiDiff) {
        this.rssiDiff = rssiDiff;
    }
}
