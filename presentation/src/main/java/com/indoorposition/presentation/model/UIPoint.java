package com.indoorposition.presentation.model;

import java.io.Serializable;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class UIPoint implements Serializable {
    public int x;
    public int y;
    public int r;

    public UIPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
