package com.indoorposition.presentation.model;

import com.indoorposition.presentation.ble.IBeacon;
import com.indoorposition.presentation.db.model.RFingerprint;
import com.indoorposition.presentation.db.model.RFloorPlan;
import com.indoorposition.presentation.db.model.RPoint;
import com.indoorposition.presentation.db.model.RUIBeacon;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmList;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class FloorPlan implements Serializable {
    private String name;
    private ArrayList<UIBeacon> beacons;
    private ArrayList<UIPoint> corners;
    private final static String NO_NAME = "no_name";
    private Scale scale;
    private int wallIndex = -1;
    private int gridCellSizePx = 30;
    private ArrayList<Fingerprint> fingerprints;
    private int noOfSamples = 5;
    private FingerprintingListener fingerprintingListener;

    public final static int FEW_CORNERS_WARN = 0;
    public final static int SCALE_NOT_SET_WARN = 1;
    public final static int FEW_BEACONS_WARN = 2;

    public FloorPlan(RFloorPlan rFloorPlan) {
        this.setScale(new Scale());
        this.name = rFloorPlan.getName();
        this.beacons = new ArrayList<>();
        this.corners = new ArrayList<>();
        this.fingerprints = new ArrayList<>();
        RealmList<RUIBeacon> ruiBeacons = rFloorPlan.getBeacons();
        for (RUIBeacon ruiBeacon : ruiBeacons) {
            beacons.add(new UIBeacon(ruiBeacon));
        }
        RealmList<RPoint> rCorners = rFloorPlan.getCorners();
        for (RPoint rCorner : rCorners) {
            corners.add(new UIPoint(rCorner.getX(), rCorner.getY()));
        }
        scale = new Scale(rFloorPlan.getScale());
        setGridCellSizePx(rFloorPlan.getGridCellSizePx());
        RealmList<RFingerprint> rFingerprints = rFloorPlan.getFingerprints();
        for (RFingerprint rFingerprint : rFingerprints) {
            fingerprints.add(new Fingerprint(rFingerprint));
        }
    }

    public FloorPlan() {
        this.setScale(new Scale());
        name = NO_NAME;
        beacons = new ArrayList<>();
        corners = new ArrayList<>();
        fingerprints = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isNameSet() {
        if (!name.equals(NO_NAME)) {
            return true;
        } else {
            return false;
        }
    }

    public void removeLastBeacon() {
        if (beacons.size() > 0) {
            beacons.remove(beacons.size() - 1);
        }
    }

    public ArrayList<UIBeacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(ArrayList<UIBeacon> beacons) {
        this.beacons = beacons;
    }

    public int getNoOfWalls() {
        return corners.size() - 1;
    }

    public ArrayList<UIPoint> getCorners() {
        return corners;
    }

    public void setCorners(ArrayList<UIPoint> corners) {
        this.corners = corners;
    }

    public void setCurrentLineLength(double length) {

    }

    public Integer incrementWallIndex() {
        ++wallIndex;
        return wallIndex;
    }

    public void resetWallIndex() {
        wallIndex = -1;
    }

    public int getWallIndex() {
        return wallIndex;
    }

    public void removeLastCorner() {
        if (corners.size() > 0) {
            corners.remove(corners.size() - 1);
            resetScale();
        }
    }

    public void setLastBeaconMajorAndMinor(int major, int minor) {
        UIBeacon beacon = beacons.get(beacons.size() - 1);
        beacon.setMajor(major);
        beacon.setMinor(minor);

        // Compute pos in cm
        UIPoint pointX = new UIPoint(0, beacon.getPositionPx().y + 25);
        UIPoint beaconCenter = new UIPoint(beacon.getPositionPx().x + 25, beacon.getPositionPx().y + 25);
        int beaconPositionXCm = scale.computeDistanceBetweenTwoPointsInCm(beaconCenter,
                pointX);
        UIPoint pointY = new UIPoint(beacon.getPositionPx().x + 25, 0);
        int beaconPositionYCm = scale.computeDistanceBetweenTwoPointsInCm(beaconCenter,
                pointY);
        UIPoint pointCm = new UIPoint(beaconPositionXCm, beaconPositionYCm);
        beacon.setPositionCm(pointCm);
        beacon.setBeaconCenter(beaconCenter);
    }

    public void validateBeacons() {
        ArrayList<UIBeacon> invalidBeacons = new ArrayList<>();
        for (UIBeacon uiBeacon : beacons) {
            if (uiBeacon.getMajor() == -1 ||
                    uiBeacon.getMinor() == -1)
            invalidBeacons.add(uiBeacon);
        }

        for (UIBeacon beaconToDelete : invalidBeacons) {
            beacons.remove(beaconToDelete);
        }
    }

    public boolean isScaleSet() {
        return getScale().isValid();
    }

    public void setWallLength(int length) {
        int currentWall = wallIndex % corners.size();
        if (currentWall < corners.size()) {
            if (currentWall == corners.size() - 1) {
                getScale().setStartPoint(corners.get(currentWall));
                getScale().setEndPoint(corners.get(0));
            } else {
                getScale().setStartPoint(corners.get(currentWall));
                getScale().setEndPoint(corners.get(currentWall + 1));
            }
            getScale().setLength(length);
        }
        if (isScaleSet()) {
            for (UIBeacon beacon : beacons) {
                UIPoint pointX = new UIPoint(0, beacon.getPositionPx().y);
                int beaconPositionXCm = scale.computeDistanceBetweenTwoPointsInCm(beacon.getPositionPx(),
                        pointX);
                UIPoint pointY = new UIPoint(beacon.getPositionPx().x, 0);
                int beaconPositionYCm = scale.computeDistanceBetweenTwoPointsInCm(beacon.getPositionPx(),
                        pointY);
                UIPoint pointCm = new UIPoint(beaconPositionXCm, beaconPositionYCm);
                beacon.setPositionCm(pointCm);
            }
        }
    }

    public int getCurrentWall() {
        int currentWallNormalized = wallIndex % corners.size();
        return currentWallNormalized;
    }

    public int getCurrentWallLength() {
        int currentWall = getCurrentWall();
        if (currentWall < corners.size()) {
            if (getScale().isValid()) {
                if (currentWall == corners.size() - 1) {
                    return getScale().computeDistanceBetweenTwoPointsInCm(corners.get(currentWall),
                            corners.get(0));
                } else {
                    return getScale().computeDistanceBetweenTwoPointsInCm(corners.get(currentWall),
                            corners.get(currentWall + 1));
                }
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    public void resetScale() {
        getScale().reset();
    }

    private ArrayList<Integer> evaluate() {
        ArrayList<Integer> evaluationWarnings = new ArrayList<>();
        if (corners.size() < 3) {
            evaluationWarnings.add(FEW_CORNERS_WARN);
        }

        if (!getScale().isValid()) {
            evaluationWarnings.add(SCALE_NOT_SET_WARN);
        }

        if (beacons.size() < 3) {
            evaluationWarnings.add(FEW_BEACONS_WARN);
        }
        return evaluationWarnings;
    }

    public boolean isValid() {
        return evaluate().size() > 0 ? false : true;
    }

    public String getWarningsMessage() {
        String warningMessage = "";
        ArrayList<Integer> warnings = evaluate();
        if (warnings.size() > 0) {
            warningMessage += "Planul are urmatoarele probleme: \n";
            for (Integer warning : warnings) {
                if (warning == FEW_CORNERS_WARN) {
                    warningMessage += "\n - Forma planului este invalida";
                } else if (warning == FEW_BEACONS_WARN) {
                    warningMessage += "\n - Sunt prea putini beaconi pentru localizare";
                } else if (warning == SCALE_NOT_SET_WARN) {
                    warningMessage += "\n - Scala planului nu a fost setata";
                }
            }
            warningMessage += "\n \n Planul nu va putea fii folosit pana nu sunt rezolvate!";
        }
        return warningMessage;
    }

    public Scale getScale() {
        return scale;
    }

    public void setScale(Scale scale) {
        this.scale = scale;
    }

    public int getGridCellSizePx() {
        return gridCellSizePx;
    }

    public void setGridCellSizePx(int gridCellSizePx) {
        this.gridCellSizePx = gridCellSizePx;
    }

    public ArrayList<Fingerprint> getFingerprints() {
        return fingerprints;
    }

    public Fingerprint getFingerprint(int x, int y) {
        Fingerprint foundFingerprint = null;
        for (Fingerprint fingerprint : fingerprints) {
            if (fingerprint.getxCell() == x &&
                    fingerprint.getyCell() == y) {
                foundFingerprint = fingerprint;
                break;
            }
        }
        return foundFingerprint;
    }

    public void addFingerprint(Fingerprint fingerprint) {
        Fingerprint existingFingerpint = getFingerprint(fingerprint.getxCell(),
                fingerprint.getyCell());
        if (existingFingerpint == null) {
            fingerprints.add(fingerprint);
        } else {
            existingFingerpint.getSamplings().addAll(fingerprint.getSamplings());
        }
    }

    public void setNoOfSamples(int noOfSamples) {
        this.noOfSamples = noOfSamples;
    }

    public int getNoOfSamples() {
        return noOfSamples;
    }

    public void nextSamplingDirection(int x, int y) {
        Fingerprint fingerprint = getFingerprint(x, y);
        ArrayList<Sampling> samplings = fingerprint.getSamplings();
        for (Sampling s : samplings) {
            s.getRssiList().add(new ArrayList<Integer>());
        }
    }

    public int getNoOfDirectionsSampledForFingerprint(int x, int y) {
        Fingerprint f = getFingerprint(x, y);
        ArrayList<Sampling> samplings = f.getSamplings();
        if (samplings != null) {
            if (samplings.size() > 0) {
                Sampling s = samplings.get(0);
                if (s.getRssiList() != null) {
                    return s.getRssiList().size();
                }
            }
        }
        return -1;
    }

    public void onScanResults(ArrayList<IBeacon> receivedBeacons, int x, int y) {
        Fingerprint fingerprint = getFingerprint(x, y);
        if (fingerprint != null) {
            if (!fingerprint.isReady(noOfSamples, this.beacons.size())) {
                for (IBeacon beacon : receivedBeacons) {
                    if (isBeaconInFloorPlan(beacon)) {
                        Sampling sampling = fingerprint.getSamplingForBeacon(beacon.getMajor(),
                                beacon.getMinor());
                        if (sampling != null) {
                            if (sampling.getRssiList().size() == 0) {
                                ArrayList<Integer> rL = new ArrayList<>();
                                rL.add(beacon.getRssi());
                                sampling.getRssiList().add(rL);
                            } else {
                                ArrayList<Integer> rL = sampling.getRssiList().get(sampling.getRssiList().size() - 1);
                                rL.add(beacon.getRssi());
                            }
                        } else {
                            UIBeacon uiBeacon = new UIBeacon(beacon.getMajor(), beacon.getMinor());
                            sampling = new Sampling(uiBeacon);
                            ArrayList<Integer> rL = new ArrayList<>();
                            rL.add(beacon.getRssi());
                            sampling.getRssiList().add(rL);
                            fingerprint.getSamplings().add(sampling);
                        }

                        if (fingerprintingListener != null) {
                            fingerprintingListener.onSamplingUpdated(sampling.getBeacon(), sampling.getRssiList().size(), x, y);
                        }
                    }

                }
            }
        } else {
            fingerprint = new Fingerprint(x, y,
                    x + gridCellSizePx / 2, y + gridCellSizePx / 2);
            for (IBeacon beacon : receivedBeacons) {
                if (isBeaconInFloorPlan(beacon)) {
                    Sampling sampling = fingerprint.getSamplingForBeacon(beacon.getMajor(),
                            beacon.getMinor());
                    if (sampling != null) {
                        if (sampling.getRssiList().size() == 0) {
                            ArrayList<Integer> rL = new ArrayList<>();
                            rL.add(beacon.getRssi());
                            sampling.getRssiList().add(rL);
                        } else {
                            ArrayList<Integer> rL = sampling.getRssiList().get(sampling.getRssiList().size() - 1);
                            rL.add(beacon.getRssi());
                        }
                    } else {
                        UIBeacon uiBeacon = new UIBeacon(beacon.getMajor(), beacon.getMinor());
                        sampling = new Sampling(uiBeacon);
                        ArrayList<Integer> rL = new ArrayList<>();
                        rL.add(beacon.getRssi());
                        sampling.getRssiList().add(rL);
                        fingerprint.getSamplings().add(sampling);
                    }
                    if (fingerprintingListener != null) {
                        fingerprintingListener.onSamplingUpdated(sampling.getBeacon(), sampling.getRssiList().size(), x, y);
                    }

                }
            }
            fingerprints.add(fingerprint);
        }
        if (fingerprint.isReady(noOfSamples, this.beacons.size())) {
            if (fingerprintingListener != null) {
                fingerprintingListener.onFingerprintFinished(x, y);
            }
        }
    }

    public void removeFingerprint(int x, int y) {
        Fingerprint fingerprintToDelete = null;
        for (Fingerprint fingerprint : fingerprints) {
            if (fingerprint.getxCell() == x &&
                    fingerprint.getyCell() == y) {
                fingerprintToDelete = fingerprint;
            }
        }
        if (fingerprintToDelete != null) {
            fingerprints.remove(fingerprintToDelete);
        }
    }

    public boolean isBeaconInFloorPlan(IBeacon beacon) {
        boolean beaconIsInFloorPlan = false;
        for (UIBeacon b : beacons) {
            if (b.getMajor() == beacon.getMajor() &&
                    b.getMinor() == beacon.getMinor()) {
                beaconIsInFloorPlan = true;
            }
        }
        return beaconIsInFloorPlan;
    }

    public interface FingerprintingListener {
        public void onSamplingUpdated(UIBeacon beacon, int noOfSamples, int x, int y);

        public void onFingerprintFinished(int x, int y);
    }

    public void setFingerprintingListener(FingerprintingListener listener) {
        fingerprintingListener = listener;
    }

    public void clearFingerprints() {
        fingerprints.clear();
    }


}
