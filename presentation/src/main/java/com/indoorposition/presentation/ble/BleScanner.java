package com.indoorposition.presentation.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

//import me.around.bleindoor.db.DbManager;


/**
 * Created by hayabusa on 23.08.2015.
 */
public class BleScanner {
    private AppCompatActivity activity;
    public BluetoothManager bleManager;
    public BluetoothAdapter bleAdapter;
    public Handler handler;
    private HashMap<String, IBeacon> foundBeacons = new HashMap<>();
    private BleResultListener bleResultListener;
    private ArrayList<IBeacon> foundDuringScanInterval = new ArrayList<>();
    private boolean scanningEnabled = false;
    private int SCAN_INTERVAL = 4000;


    private ScheduledExecutorService mWorker1 = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> mScheduledFutureScan;

    private BluetoothAdapter.LeScanCallback leScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            IBeacon beacon = BeaconFactory.fromScanData(bytes, i, bluetoothDevice);
            foundBeacons.put(beacon.getMajor() + "/" + beacon.getMinor(), beacon);
            synchronized (foundDuringScanInterval) {
                foundDuringScanInterval.add(beacon);
            }
        }
    };

    // Singleton
    public BleScanner(AppCompatActivity activity) {
        this.activity = activity;
        bleManager = (BluetoothManager) activity.getSystemService(activity.BLUETOOTH_SERVICE);
        bleAdapter = bleManager.getAdapter();
        handler = new Handler();

        if (bleAdapter != null && bleAdapter.isEnabled()) {

        }
    }

    public void scanLeDevice(boolean enable) {
        scanningEnabled = enable;
        if (enable) {
            mScheduledFutureScan = mWorker1.scheduleWithFixedDelay(new Runnable() {
                @Override
                public void run() {
                    try {
                        synchronized (foundDuringScanInterval) {
                            final ArrayList<IBeacon> clonedBeacons = (ArrayList<IBeacon>) foundDuringScanInterval.clone();

                            activity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (bleResultListener != null) {
                                        bleResultListener.onScanResults(clonedBeacons);
                                    }
                                }
                            });

                            foundDuringScanInterval.clear();
                        }
                    } catch (Exception e) {
                        Log.e("error", e.getMessage());
                        e.printStackTrace();
                    }
                    //}
                }
            }, 1500, SCAN_INTERVAL, TimeUnit.MILLISECONDS);
            bleAdapter.startLeScan(leScanCallback);
        } else {
            bleAdapter.stopLeScan(leScanCallback);
            if (mScheduledFutureScan != null)
                mScheduledFutureScan.cancel(true);
        }
    }

    public IBeacon getBeaconByIds(int major, int minor) {
        String beaconId = major + "/" + minor;
        if (foundBeacons.containsKey(beaconId)) {
            return foundBeacons.get(beaconId);
        } else {
            return null;
        }
    }

    public void setBleResultListener(BleResultListener bleResultListener) {
        this.bleResultListener = bleResultListener;
    }

    public boolean getScanningEnabled() {
        return scanningEnabled;
    }

}
