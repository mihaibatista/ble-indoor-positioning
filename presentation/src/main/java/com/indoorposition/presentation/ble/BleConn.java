package com.indoorposition.presentation.ble;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by hayabusa on 17.04.2016.
 */
public class BleConn {
    private Context mContext;
    private BluetoothManager bleManager;
    private BluetoothAdapter bleAdapter;
    private HashMap<String, AbstractMap.SimpleEntry<IBeacon, String>> mBeaconsHash =
            new HashMap<String, AbstractMap.SimpleEntry<IBeacon, String>>();
    private BluetoothGatt mBluetoothGatt;
    ScheduledFuture<?> mScheduledFuture;
    private boolean mReadingCharacteristic = false;
    private boolean mConnectionInProgress = false;
    private String mCurrentCommand;
    private IBeacon mCurrentBeacon;
    private BeaconData mCurrentBeaconData;
    private BleConnListener bleConnListener;
    private AppCompatActivity activity;

    private ArrayList<BluetoothGattCharacteristic> mCharacteristicsList
            = new ArrayList<BluetoothGattCharacteristic>();
    private ScheduledExecutorService mWorker = Executors.newSingleThreadScheduledExecutor();
    private static int CONNECTION_TIMEOUT = 15;

    // Commands
    private static final String COMMAND_READ_INFO = "read device info";

    // Beacon services
    private static final UUID ONYX_BEACON_SERVICE =
            UUID.fromString("2aaceb00-c5a5-44fd-0000-3fd42d703a4f");
    // Beacon characteristics
    private static final UUID UUID_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0100-3fd42d703a4f");
    private static final UUID MAJOR_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0200-3fd42d703a4f");
    private static final UUID MINOR_CHARACTERISITC =
            UUID.fromString("2aaceb00-c5a5-44fd-0300-3fd42d703a4f");
    private static final UUID BATTERY_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0400-3fd42d703a4f");
    private static final UUID SYSID_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0500-3fd42d703a4f");
    private static final UUID FIRMWARE_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0600-3fd42d703a4f");
    private static final UUID HARDWARE_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0700-3fd42d703a4f");
    private static final UUID POWER_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0900-3fd42d703a4f");
    private static final UUID FREQUENCY_CHARACTERISTIC =
            UUID.fromString("2aaceb00-c5a5-44fd-0a00-3fd42d703a4f");

    public BleConn(AppCompatActivity activity) {
        this.activity = activity;
        bleManager = (BluetoothManager) activity.getSystemService(activity.BLUETOOTH_SERVICE);
        bleAdapter = bleManager.getAdapter();
        mContext = activity;
    }

    public void readCharacteristicsForDevice(IBeacon beacon) {
        mBeaconsHash.put(beacon.getBluetoothAddress(),
                new AbstractMap.SimpleEntry<IBeacon, String>(beacon, COMMAND_READ_INFO));
        executeCommand();
    }

    private void readCharacteristic() {
        if (!mReadingCharacteristic) {
            if (mCharacteristicsList.size() > 0) {
                if (mBluetoothGatt != null) {
                    mBluetoothGatt.readCharacteristic(mCharacteristicsList.get(0));
                    mReadingCharacteristic = true;
                } else {
                    mReadingCharacteristic = false;
                }
            }
        }
    }


    public void readBeaconInfo(IBeacon beacon) {
        mCurrentCommand = COMMAND_READ_INFO;
        mCurrentBeacon = beacon;
        mCurrentBeaconData = new BeaconData();
        mCurrentBeaconData.major = beacon.getMajor();
        mCurrentBeaconData.minor = beacon.getMinor();
        passEvent(BleConnEvent.CONNECTING);
        connectToDevice(mCurrentBeacon);
    }

    private void connectToDevice(IBeacon beacon) {
        BluetoothDevice device = bleAdapter.getRemoteDevice(beacon.getBluetoothAddress());
        device.connectGatt(mContext, false, mGattCallback);
    }

    private void disconnectDevice() {
        mConnectionInProgress = false;
        mBeaconsHash.remove(mBluetoothGatt.getDevice().getAddress());
        if (mBluetoothGatt != null) mBluetoothGatt.disconnect();
        if (mScheduledFuture != null) mScheduledFuture.cancel(true);
    }

    private void finishReadingCharacteristicsForDeviceSuccess(IBeacon beacon) {
        mReadingCharacteristic = false;
        if (bleConnListener != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bleConnListener.onBeaconDataRead(mCurrentBeaconData);
                }
            });
        }
        passEvent(BleConnEvent.BLE_CONN_DONE);

        disconnectDevice();
        executeCommand();
    }

    public void passEvent(final int event) {
        if (bleConnListener != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    bleConnListener.onBleConnEvent(event);
                }
            });
        }
    }

    private void finishReadingCharacteristicsForDeviceFailed(String bluetoothAddress) {
        mReadingCharacteristic = false;
        passEvent(BleConnEvent.BLE_CONN_DONE);
        disconnectDevice();
        executeCommand();
    }



    @SuppressLint("NewApi")
    private final BluetoothGattCallback mGattCallback =
            new BluetoothGattCallback() {
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    if (newState == BluetoothProfile.STATE_CONNECTED) {
                        passEvent(BleConnEvent.CONNECTED);
                        disableScanningAfterDelay();
                        mBluetoothGatt = gatt;
                        gatt.discoverServices();
                    } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                        passEvent(BleConnEvent.DISCONNECTED);
                        disconnectDevice();
                        executeCommand();
                    } else if (newState == BluetoothProfile.STATE_CONNECTING) {
                    } else if (newState == BluetoothProfile.STATE_DISCONNECTING) {

                    }
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        passEvent(BleConnEvent.SERVICE_DISCOVERED);
                        if (mCurrentCommand.equals(COMMAND_READ_INFO)) {
                            BluetoothGattService service = gatt.getService(ONYX_BEACON_SERVICE);
                            if (service != null) {
                                if (service.getCharacteristics().size() != 0) {
                                    passEvent(BleConnEvent.CHARS_DISCOVERED);
                                    BluetoothGattCharacteristic batteryCharacteristic = service.getCharacteristic(BATTERY_CHARACTERISTIC);
                                    BluetoothGattCharacteristic frequencyCharacteristic = service.getCharacteristic(FREQUENCY_CHARACTERISTIC);
                                    BluetoothGattCharacteristic powerCharacteristic = service.getCharacteristic(POWER_CHARACTERISTIC);
                                    BluetoothGattCharacteristic hardwareCharacteristic = service.getCharacteristic(HARDWARE_CHARACTERISTIC);
                                    BluetoothGattCharacteristic firmwareCharacteristic = service.getCharacteristic(FIRMWARE_CHARACTERISTIC);
                                    BluetoothGattCharacteristic sysidCharacteristic = service.getCharacteristic(SYSID_CHARACTERISTIC);

                                    if (!(batteryCharacteristic == null)) {
                                        mCharacteristicsList.add(batteryCharacteristic);
                                    }
                                    if (!(frequencyCharacteristic == null)) {
                                        mCharacteristicsList.add(frequencyCharacteristic);
                                    }
                                    if (!(powerCharacteristic == null)) {
                                        mCharacteristicsList.add(powerCharacteristic);
                                    }
                                    if (!(hardwareCharacteristic == null)) {
                                        mCharacteristicsList.add(hardwareCharacteristic);
                                    }
                                    if (!(firmwareCharacteristic == null)) {
                                        mCharacteristicsList.add(firmwareCharacteristic);
                                    }
                                    if (!(sysidCharacteristic == null)) {
                                        mCharacteristicsList.add(sysidCharacteristic);
                                    }
                                    readCharacteristic();
                                } else {
                                    finishReadingCharacteristicsForDeviceFailed(gatt.getDevice().getAddress());
                                }

                            } else {
                                finishReadingCharacteristicsForDeviceFailed(gatt.getDevice().getAddress());
                            }
                        }
                    } else {
                        finishReadingCharacteristicsForDeviceFailed(gatt.getDevice().getAddress());
                    }
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        if (characteristic.getUuid().toString().equals(BATTERY_CHARACTERISTIC.toString())) {
                            int batteryLevel = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                            Log.d("Char", "Battery level " + batteryLevel);
                            mCurrentBeaconData.battery = batteryLevel;
                            passEvent(BleConnEvent.CHAR_READ);
                        } else if (characteristic.getUuid().toString().equals(UUID_CHARACTERISTIC.toString())) {
                            byte[] uuidAsBytes = characteristic.getValue();
                            UUID uuid = Utilities.convertUUID(uuidAsBytes);
                            mCurrentBeaconData.uuid = uuid.toString();
                            Log.d("Char", "UUID " + uuid.toString());
                            passEvent(BleConnEvent.CHAR_READ);
                        } else if (characteristic.getUuid().toString().equals(FREQUENCY_CHARACTERISTIC.toString())) {
                            int frequency = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                            mCurrentBeaconData.frequency = frequency;
                            Log.d("Char", "Frequency is " + frequency);
                            passEvent(BleConnEvent.CHAR_READ);
                        } else if (characteristic.getUuid().toString().equals(POWER_CHARACTERISTIC.toString())) {
                            int power = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                            mCurrentBeaconData.power = power;
                            Log.d("Char", "Power is " + power);
                            passEvent(BleConnEvent.CHAR_READ);
                        } else if (characteristic.getUuid().toString().equals(HARDWARE_CHARACTERISTIC.toString())) {
                            byte[] hardwareAsBytes = characteristic.getValue();
                            String hardware = Utilities.convertFmHwVersion(hardwareAsBytes);
                            mCurrentBeaconData.hardware = hardware;
                            Log.d("Char", "Hardware is " + hardware);
                            passEvent(BleConnEvent.CHAR_READ);
                        } else if (characteristic.getUuid().toString().equals(FIRMWARE_CHARACTERISTIC.toString())) {
                            byte[] firmwareAsBytes = characteristic.getValue();
                            String firmware = Utilities.convertFmHwVersion(firmwareAsBytes);
                            mCurrentBeaconData.firmware = firmware;
                            Log.d("Char", "Firmware is " + firmware);
                            passEvent(BleConnEvent.CHAR_READ);
                        } else if (characteristic.getUuid().toString().equals(SYSID_CHARACTERISTIC.toString())) {
                            byte[] sysidAsBytes = characteristic.getValue();
                            String sysId = Utilities.convertSysId(sysidAsBytes);
                            mCurrentBeaconData.sysId = sysId;
                            Log.d("Char", "Sysid " + sysId);
                            passEvent(BleConnEvent.CHAR_READ);
                            finishReadingCharacteristicsForDeviceSuccess(mCurrentBeacon);
                        }
                        mCharacteristicsList.remove(0);
                        mReadingCharacteristic = false;
                        readCharacteristic();
                    }
                }

                @Override
                public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
                    super.onCharacteristicChanged(gatt, characteristic);
                }
            };


    private void disableScanningAfterDelay() {
        Runnable disableScanning = new Runnable() {
            public void run() {
                passEvent(BleConnEvent.BLE_CONN_DONE);
                mConnectionInProgress = false;
                mReadingCharacteristic = false;
                //if (mBluetoothGatt != null) mBluetoothGatt.close();
                if (mBluetoothGatt != null) mBluetoothGatt.disconnect();
                mBeaconsHash.remove(mBluetoothGatt.getDevice().getAddress());
                //mOnyxBeaconService.registerProximityUUDIDs(false);
                executeCommand();
            }
        };
        mScheduledFuture = mWorker.schedule(disableScanning, CONNECTION_TIMEOUT, TimeUnit.SECONDS);

    }

    private void executeCommand() {
        if (!mConnectionInProgress) {
            if (mBeaconsHash.size() > 0) {
                //mOnyxBeaconService.unregisterProximityUUIDs();

                mConnectionInProgress = true;
                mCurrentBeacon = mBeaconsHash.entrySet().iterator().next().getValue().getKey();
                mCurrentCommand = mBeaconsHash.entrySet().iterator().next().getValue().getValue();

                connectToDevice(mCurrentBeacon);
            }
        }
    }

    public void setBleConnListener(BleConnListener bleConnListener) {
        this.bleConnListener = bleConnListener;
    }

}
