package com.indoorposition.presentation.ble;

import android.util.Log;

/**
 * Set of utils methods used in this client library.
 * Created by Luci on 4/26/2014.
 */
public class IBeaconUtils {

    /**
     * Computes iBeacon proximity based on accuracy computed from LE scan.
     *
     * @param accuracy computed from LE scan
     * @return proximity
     */
    public static int calculateBeaconProximity(double accuracy) {
        if (accuracy < 0.5) {
            return IBeacon.PROXIMITY_IMMEDIATE;
        }

        // forums say 3.0 is the near/far threshold, but it looks to be based on experience that this is 4.0
        if (accuracy <= 4.0) {
            return IBeacon.PROXIMITY_NEAR;
        }

        if (accuracy <= 8.0) {
            return IBeacon.PROXIMITY_FAR;
        }

        return IBeacon.PROXIMITY_UNKNOWN;

    }

    /**
     * @param txPower
     * @param rssi
     * @return distance from txPower and rssi got from LE scan
     */
    public static double calculateBeaconDistance(int txPower, double rssi) {
        if (rssi == 0) {
            return -1.0; // if we cannot determine accuracy, return -1.
        }
        Log.d("BLE", "calculating accuracy based on rssi of " + rssi);
        double ratio = rssi * 1.0 / txPower;
        if (ratio < 1.0) {
            return Math.pow(ratio, 10);
        } else {
            double accuracy = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            Log.d("BLE", " avg rssi: " + rssi + " accuracy: " + accuracy);
            return accuracy;
        }
    }

    /**
     * Puts string to a normalized UUID format, or throws a runtime exception if it contains non-hex digits
     * other than dashes or spaces, or if it doesn't contain exactly 32 hex digits
     *
     * @param proximityUuid uuid with any combination of upper/lower case hex characters, dashes and spaces
     * @return a normalized string, all lower case hex characters with dashes in the form
     * e2c56db5-dffb-48d2-b060-d0f5a71096e0
     */
    public static String normalizeProximityUuid(String proximityUuid) {
        if (proximityUuid == null) {
            return null;
        }
        String dashlessUuid = proximityUuid.toLowerCase().replaceAll("[\\-\\s]", "");
        if (dashlessUuid.length() != 32) {
            // TODO: make this a specific exception
            throw new RuntimeException("UUID: "
                    + proximityUuid
                    + " is too short.  Must contain exactly 32 hex digits, and there are this value has "
                    + dashlessUuid.length()
                    + " digits.");
        }
        if (!dashlessUuid.matches("^[a-fA-F0-9]*$")) {
            // TODO: make this a specific exception
            throw new RuntimeException("UUID: "
                    + proximityUuid
                    + " contains invalid characters.  Must be dashes, a-f and 0-9 characters only.");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(dashlessUuid.substring(0, 8));
        sb.append('-');
        sb.append(dashlessUuid.substring(8, 12));
        sb.append('-');
        sb.append(dashlessUuid.substring(12, 16));
        sb.append('-');
        sb.append(dashlessUuid.substring(16, 20));
        sb.append('-');
        sb.append(dashlessUuid.substring(20, 32));
        return sb.toString();
    }
}
