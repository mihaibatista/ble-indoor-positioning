package com.indoorposition.presentation.ble;

/**
 * Created by hayabusa on 08.05.2016.
 */
public class BeaconData {
    public int major;
    public int minor;
    public int battery;
    public String uuid;
    public int frequency;
    public int power;
    public String hardware;
    public String firmware;
    public String sysId;
}
