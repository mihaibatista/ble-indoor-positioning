package com.indoorposition.presentation.ble;

/**
 * Created by hayabusa on 08.05.2016.
 */
public interface BleConnListener {

    public void onBeaconDataRead(BeaconData beaconData);

    public void onBleConnEvent(int event);

}
