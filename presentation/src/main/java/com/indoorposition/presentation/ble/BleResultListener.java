package com.indoorposition.presentation.ble;

import java.util.ArrayList;

/**
 * Created by hayabusa on 08.05.2016.
 */
public interface BleResultListener {

    public void onScanResults(ArrayList<IBeacon> beacons);

}
