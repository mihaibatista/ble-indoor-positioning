package com.indoorposition.presentation.ble;

/**
 * Created by hayabusa on 08.05.2016.
 */
public class BleConnEvent {
    public static int CONNECTING = 1;
    public static int CONNECTED = 2;
    public static int SERVICE_DISCOVERED = 3;
    public static int CHARS_DISCOVERED = 4;
    public static int CHAR_READ = 5;
    public static int DISCONNECTED = 6;
    public static int BLE_CONN_DONE = 7;
}
