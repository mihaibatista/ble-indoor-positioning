package com.indoorposition.presentation.db.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by hayabusa on 21.06.2016.
 */
public class RList extends RealmObject {

    private RealmList<RInteger> rssiList;

    public RList() {
        rssiList = new RealmList<>();
    }

    public RealmList<RInteger> getRssiList() {
        return rssiList;
    }

    public void setRssiList(RealmList<RInteger> rssiList) {
        this.rssiList = rssiList;
    }


}
