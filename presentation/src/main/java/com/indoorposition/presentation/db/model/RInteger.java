package com.indoorposition.presentation.db.model;

import io.realm.RealmObject;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class RInteger extends RealmObject {
    private Integer value;

    public RInteger() {}

    public RInteger(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }
}
