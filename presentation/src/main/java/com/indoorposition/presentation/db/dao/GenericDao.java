package com.indoorposition.presentation.db.dao;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class GenericDao {

    protected RealmConfiguration realmConfiguration;
    protected Realm realm;
    private File EXPORT_REALM_PATH = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOWNLOADS);
    private String EXPORT_REALM_FILE_NAME = "positioning.realm";
    private String IMPORT_REALM_FILE_NAME = "default.realm";
    private Context context;

    public GenericDao(Context context) {
        this.context = context;
        realmConfiguration = new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
                .build();
    }

    public void backup() {
        try {
            File exportedRealmFile;
            exportedRealmFile = new File(EXPORT_REALM_PATH, EXPORT_REALM_FILE_NAME);
            exportedRealmFile.delete();
            realm = Realm.getInstance(this.realmConfiguration);
            realm.writeCopyTo(exportedRealmFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void restore() {
        String restroreFilePath = EXPORT_REALM_PATH + "/" +
                EXPORT_REALM_FILE_NAME;
        copyBundledRealmFile(restroreFilePath, IMPORT_REALM_FILE_NAME);
    }

    private String copyBundledRealmFile(String oldFilePath,
                                        String outFileName) {
        try {
            File file = new File(context.getApplicationContext().getFilesDir(), outFileName);
            FileOutputStream outputStream = new FileOutputStream(file);
            FileInputStream inputStream = new FileInputStream(new File(oldFilePath));

            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
