package com.indoorposition.presentation.db.model;

import com.indoorposition.presentation.model.Scale;

import io.realm.RealmObject;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class RScale extends RealmObject {
    private RPoint startPoint;
    private RPoint endPoint;
    private Integer length;

    public RScale() {}

    public RScale(Scale scale) {
        startPoint = new RPoint(scale.getStartPoint());
        endPoint = new RPoint(scale.getEndPoint());
        length = scale.getLength();
    }

    public RPoint getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(RPoint startPoint) {
        this.startPoint = startPoint;
    }

    public RPoint getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(RPoint endPoint) {
        this.endPoint = endPoint;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }
}
