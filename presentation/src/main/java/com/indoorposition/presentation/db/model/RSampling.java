package com.indoorposition.presentation.db.model;

import com.indoorposition.presentation.model.Sampling;

import java.util.ArrayList;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class RSampling extends RealmObject {
    private RUIBeacon beacon;
    private RealmList<RList> rssiList;

    public RSampling() {}

    public RSampling(Sampling sampling) {
        RUIBeacon b = new RUIBeacon(sampling.getBeacon());
        beacon = b;
        rssiList = new RealmList<>();
        for (ArrayList<Integer> rssiListInt : sampling.getRssiList()) {
            RList rList = new RList();
            for (Integer rssi : rssiListInt) {
                RInteger rInteger = new RInteger(rssi);
                rList.getRssiList().add(rInteger);
            }
            rssiList.add(rList);
        }
    }

    public RUIBeacon getBeacon() {
        return beacon;
    }

    public void setBeacon(RUIBeacon beacon) {
        this.beacon = beacon;
    }

    public RealmList<RList> getRssiList() {
        return rssiList;
    }

    public void setRssiList(RealmList<RList> rssiList) {
        this.rssiList = rssiList;
    }
}
