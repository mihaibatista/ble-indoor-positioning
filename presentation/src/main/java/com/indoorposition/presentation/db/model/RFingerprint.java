package com.indoorposition.presentation.db.model;

import com.indoorposition.presentation.model.Fingerprint;
import com.indoorposition.presentation.model.Sampling;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class RFingerprint extends RealmObject {
    private int xCell;
    private int yCell;
    private int xCellCenter;
    private int yCellCenter;
    private RealmList<RSampling> samplings;

    public RFingerprint() {}

    public RFingerprint(Fingerprint fingerprint) {
        xCell = fingerprint.getxCell();
        yCell = fingerprint.getyCell();
        xCellCenter = fingerprint.getxCellCenter();
        yCellCenter = fingerprint.getyCellCenter();
        samplings = new RealmList<>();
        for (Sampling sampling : fingerprint.getSamplings()) {
            RSampling rSampling = new RSampling(sampling);
            samplings.add(rSampling);
        }
    }

    public int getxCell() {
        return xCell;
    }

    public void setxCell(int xCell) {
        this.xCell = xCell;
    }

    public int getyCell() {
        return yCell;
    }

    public void setyCell(int yCell) {
        this.yCell = yCell;
    }

    public int getxCellCenter() {
        return xCellCenter;
    }

    public void setxCellCenter(int xCellCenter) {
        this.xCellCenter = xCellCenter;
    }

    public int getyCellCenter() {
        return yCellCenter;
    }

    public void setyCellCenter(int yCellCenter) {
        this.yCellCenter = yCellCenter;
    }

    public RealmList<RSampling> getSamplings() {
        return samplings;
    }

    public void setSamplings(RealmList<RSampling> samplings) {
        this.samplings = samplings;
    }
}
