package com.indoorposition.presentation.db.model;

import com.indoorposition.presentation.model.UIBeacon;

import io.realm.RealmObject;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class RUIBeacon extends RealmObject {
    private Integer major;
    private Integer minor;
    private Integer samplingStrength;
    private RPoint positionPx;
    private RPoint positionCm;
    private int battery;
    private int frequency;
    private int power;
    private String hardware;
    private String firmware;
    private String sysId;

    public RUIBeacon() {}

    public RUIBeacon(UIBeacon uiBeacon) {
        major = uiBeacon.getMajor();
        minor = uiBeacon.getMinor();
        samplingStrength = uiBeacon.getSamplingStrength();
        positionPx = new RPoint(uiBeacon.getPositionPx());
        positionCm = new RPoint(uiBeacon.getPositionCm());
        battery = uiBeacon.getBattery();
        frequency = uiBeacon.getFrequency();
        power = uiBeacon.getPower();
        hardware = uiBeacon.getHardware();
        firmware = uiBeacon.getFirmware();
        sysId = uiBeacon.getSysId();
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public Integer getSamplingStrength() {
        return samplingStrength;
    }

    public void setSamplingStrength(Integer samplingStrength) {
        this.samplingStrength = samplingStrength;
    }

    public RPoint getPositionPx() {
        return positionPx;
    }

    public void setPositionPx(RPoint positionPx) {
        this.positionPx = positionPx;
    }

    public int getBattery() {
        return battery;
    }

    public void setBattery(int battery) {
        this.battery = battery;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public RPoint getPositionCm() {
        return positionCm;
    }

    public void setPositionCm(RPoint positionCm) {
        this.positionCm = positionCm;
    }
}
