package com.indoorposition.presentation.db.dao;

import android.content.Context;
import android.util.Log;

import com.indoorposition.presentation.db.model.RFingerprint;
import com.indoorposition.presentation.db.model.RFloorPlan;
import com.indoorposition.presentation.db.model.RPoint;
import com.indoorposition.presentation.db.model.RScale;
import com.indoorposition.presentation.db.model.RUIBeacon;
import com.indoorposition.presentation.model.Fingerprint;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class FloorPlanDao extends GenericDao implements IFloorPlanDao {

    public FloorPlanDao(Context context) {
        super(context);
    }

    public void createOrUpdateFloorPlan(FloorPlan floorPlan) {
        realm = null;
        try {
            realm = Realm.getInstance(this.realmConfiguration);
            realm.beginTransaction();
            RealmList rRoomShape = new RealmList();
            ArrayList<UIPoint> corners = floorPlan.getCorners();
            for (UIPoint corner : corners) {
                RPoint rPoint = new RPoint(corner);
                rRoomShape.add(rPoint);
            }
            RealmList<RUIBeacon> rBeacons = new RealmList<RUIBeacon>();
            ArrayList<UIBeacon> beacons = floorPlan.getBeacons();
            for (UIBeacon beacon : beacons) {
                RUIBeacon ruiBeacon = new RUIBeacon(beacon);
                rBeacons.add(ruiBeacon);
            }
            RealmList<RFingerprint> rFingerprints = new RealmList<>();
            ArrayList<Fingerprint> fingerprints = floorPlan.getFingerprints();
            for (Fingerprint fingerprint : fingerprints) {
                RFingerprint rFingerprint = new RFingerprint(fingerprint);
                rFingerprints.add(rFingerprint);
            }
            RScale rScale = new RScale(floorPlan.getScale());
            RFloorPlan rFloorPlan = new RFloorPlan();
            rFloorPlan.setCorners(rRoomShape);
            rFloorPlan.setBeacons(rBeacons);
            rFloorPlan.setName(floorPlan.getName());
            rFloorPlan.setScale(rScale);
            rFloorPlan.setGridCellSizePx(floorPlan.getGridCellSizePx());
            rFloorPlan.setFingerprints(rFingerprints);

            realm.copyToRealmOrUpdate(rFloorPlan);
            realm.commitTransaction();
        } catch (Exception e) {
            Log.e("Exceptie", e.getMessage(), e);
            realm.cancelTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public void deleteFloorPlan(FloorPlan floorPlan) {
        realm = null;
        try {
            realm = Realm.getInstance(this.realmConfiguration);
            realm.beginTransaction();
            RFloorPlan rFloorPlan = realm.where(RFloorPlan.class)
                    .equalTo("name", floorPlan.getName())
                    .findFirst();
            if (rFloorPlan != null) {
                rFloorPlan.removeFromRealm();
            }
            realm.commitTransaction();
        } catch (Exception e) {
            Log.e("Exception", e.getMessage(), e);
            realm.cancelTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    public ArrayList<FloorPlan> fetchAllFloorPlans() {
        ArrayList<FloorPlan> floorPlanList = new ArrayList<>();
        realm = Realm.getInstance(this.realmConfiguration);
        RealmResults<RFloorPlan> results = realm.where(RFloorPlan.class)
                .findAll();
        for (RFloorPlan rFloorPlan : results) {
            floorPlanList.add(new FloorPlan(rFloorPlan));
        }
        realm.close();
        return floorPlanList;
    }



}
