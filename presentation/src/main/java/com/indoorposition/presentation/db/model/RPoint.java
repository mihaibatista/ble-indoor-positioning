package com.indoorposition.presentation.db.model;

import com.indoorposition.presentation.model.UIPoint;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class RPoint extends RealmObject {
    @PrimaryKey
    private String id;
    private Integer x;
    private Integer y;

    public RPoint() {}

    public RPoint(UIPoint p) {
        if (p != null) {
            id = String.valueOf(p.x) + String.valueOf(p.y);
            x = p.x;
            y = p.y;
        } else {
            id = "-1";
            x = -1;
            y = -1;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }
}
