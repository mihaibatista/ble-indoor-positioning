package com.indoorposition.presentation.db.dao;

import com.indoorposition.presentation.model.FloorPlan;

import java.util.ArrayList;

/**
 * Created by hayabusa on 07.05.2016.
 */
public interface IFloorPlanDao {

    public void createOrUpdateFloorPlan(FloorPlan floorPlan);

    public void deleteFloorPlan(FloorPlan floorPlan);

    public ArrayList<FloorPlan> fetchAllFloorPlans();

    public void backup();

    public void restore();

}
