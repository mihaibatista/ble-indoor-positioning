package com.indoorposition.presentation.db.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class RFloorPlan extends RealmObject {
    @PrimaryKey
    private String name;
    private RealmList<RPoint> corners;
    private RealmList<RUIBeacon> beacons;
    private RScale scale;
    private Integer gridCellSizePx;
    private RealmList<RFingerprint> fingerprints;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<RPoint> getCorners() {
        return corners;
    }

    public void setCorners(RealmList<RPoint> corners) {
        this.corners = corners;
    }

    public RealmList<RUIBeacon> getBeacons() {
        return beacons;
    }

    public void setBeacons(RealmList<RUIBeacon> beacons) {
        this.beacons = beacons;
    }

    public RScale getScale() {
        return scale;
    }

    public void setScale(RScale scale) {
        this.scale = scale;
    }

    public Integer getGridCellSizePx() {
        return gridCellSizePx;
    }

    public void setGridCellSizePx(Integer gridCellSizePx) {
        this.gridCellSizePx = gridCellSizePx;
    }


    public RealmList<RFingerprint> getFingerprints() {
        return fingerprints;
    }

    public void setFingerprints(RealmList<RFingerprint> fingerprints) {
        this.fingerprints = fingerprints;
    }
}
