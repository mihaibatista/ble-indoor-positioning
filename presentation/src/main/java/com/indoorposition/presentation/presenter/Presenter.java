package com.indoorposition.presentation.presenter;

/**
 * Created by hayabusa on 02.04.2016.
 */
public interface Presenter {

    /** Method that control the lifecycle of the view. It should be called in
     * the activity/fragment onResume() method
     */
    void resume();

    /** Method that control the lifecycle of the view. It should be called in
     * the activiy/fragment onPause() method
     */
    void pause();

    /** Method that control the lifecycle of the view. It should be called in
     * the activity/fragment onDestroy() method
     */
    void destroy();
}
