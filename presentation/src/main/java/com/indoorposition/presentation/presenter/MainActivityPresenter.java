package com.indoorposition.presentation.presenter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.ble.BeaconData;
import com.indoorposition.presentation.ble.BleConn;
import com.indoorposition.presentation.ble.BleConnListener;
import com.indoorposition.presentation.ble.BleResultListener;
import com.indoorposition.presentation.ble.BleScanner;
import com.indoorposition.presentation.ble.IBeacon;
import com.indoorposition.presentation.db.dao.FloorPlanDao;
import com.indoorposition.presentation.db.dao.IFloorPlanDao;
import com.indoorposition.presentation.positioning.FingerprintingMethod;
import com.indoorposition.presentation.positioning.Trilateration;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hayabusa on 24.05.2016.
 */
public class MainActivityPresenter {

    // Scanner
    private BleConn bleConn;
    private BleScanner bleScanner;
    private UIBeacon selectedBeacon;
    private FloorPlan floorPlan;
    private FingerprintingMethod fingerprintingMethod;
    private Trilateration trilateration;
    private MainActivityView mainActivityView;
    private IFloorPlanDao floorPlanDao;

    public final static int TRILATERATION1_METHOD = 1;
    public final static int TRILATERATION2_METHOD = 2;
    public final static int FINGERPRINTING = 3;

    private int currentMethod = FINGERPRINTING;

    public MainActivityPresenter(MainActivityView mainActivityView,
                                 FloorPlan floorPlan) {
        bleConn = new BleConn((AppCompatActivity) mainActivityView);
        bleConn.setBleConnListener(bleConnListener);
        bleScanner = new BleScanner((AppCompatActivity) mainActivityView);
        bleScanner.setBleResultListener(bleResultListener);
        this.floorPlan = floorPlan;
        fingerprintingMethod = new FingerprintingMethod(floorPlan);
        trilateration = new Trilateration(floorPlan);
        this.mainActivityView = mainActivityView;

        // Db
        floorPlanDao = new FloorPlanDao((Context) mainActivityView);
    }

    public void onButtonClicked(Button button) {
      if (button.getId() == R.id.read_beacon_info) {
            if (selectedBeacon.getBeacon() != null) {
                bleConn.readBeaconInfo(selectedBeacon.getBeacon());
                mainActivityView.readBeaconDataSuccess();
            } else {

            }
        } else if (button.getId() == R.id.fab_close_beacon_info) {
            mainActivityView.readBeaconDataFailed();
        }
    }

    private BleResultListener bleResultListener = new BleResultListener() {
        @Override
        public void onScanResults(ArrayList<IBeacon> beacons) {

            for (IBeacon beacon : beacons) {
                if ((beacon.getMajor() == 125 &&
                        beacon.getMinor() == 52) ||
                        (beacon.getMajor() == 24 &&
                        beacon.getMinor() == 8817) ||
                        (beacon.getMajor() == 146 &&
                        beacon.getMinor() == 34426)
                ) {
                    Date now = new Date();
                    Log.d("SampleRSSI,", ", " + beacon.getMajor() + "/" + beacon.getMinor() + ", " + beacon.getRssi() + ", " + beacon.getAccuracy() + ", "  + now.getTime());
                    break;
                }
            }


            if (trilateration.isValid(beacons)) {
                ArrayList<UIBeacon> uiBeacons = floorPlan.getBeacons();
                for (UIBeacon uiBeacon : uiBeacons) {
                    uiBeacon.setSamplingStrength(0);
                }
                for (IBeacon beacon : beacons) {


                    for (UIBeacon uiBeacon : uiBeacons) {
                        if (beacon.getMajor() == uiBeacon.getMajor() &&
                                beacon.getMinor() == uiBeacon.getMinor()) {
                            int currentSamplingStrength = uiBeacon.getSamplingStrength();
                            uiBeacon.setSamplingStrength(currentSamplingStrength + 1);
                            if (uiBeacon.getBeacon() == null) {
                                uiBeacon.setBeacon(beacon);
                            }
                        }
                    }
                }

                UIPoint userLocation;
                if (currentMethod == TRILATERATION1_METHOD) {
                    userLocation = trilateration.computeLocation(beacons);
                } else if (currentMethod == TRILATERATION2_METHOD) {
                    userLocation = trilateration.computeLocationFiltered(beacons);
                } else {
                    userLocation = fingerprintingMethod.computeLocation(beacons);
                }
                //UIPoint userLocation = fingerprintingMethod.computeLocation(beacons);
                //UIPoint userLocation = fingerprintingMethod.computeLocation4Dir(beacons);
                //UIPoint userLocation = trilateration.computeLocationFiltered(beacons);
                //UIPoint userLocation = trilateration.computeLocation(beacons);
                mainActivityView.displayUserLocation(userLocation);
                mainActivityView.beaconsUsedForLocating(trilateration.getBeaconsUsed(beacons));
            }
        }
    };

    public void setPositioningMethod(int method) {
        currentMethod = method;
    }

    public void startScanner() {
        bleScanner.scanLeDevice(true);
    }

    public void stopScanner() {
        bleScanner.scanLeDevice(false);
    }

    private BleConnListener bleConnListener = new BleConnListener() {
        @Override
        public void onBeaconDataRead(BeaconData beaconData) {
            String beaconDataString = "Major: " + beaconData.major + "\n" +
                    "Minor: " + beaconData.minor + "\n" +
                    "Battery: " + beaconData.battery + "\n" +
                    "Frequency: " + beaconData.frequency + "\n" +
                    "Power: " + beaconData.power + "\n" +
                    "Hardware: " + beaconData.hardware + "\n" +
                    "Firmware: " + beaconData.firmware + "\n" +
                    "Sysid: " + beaconData.sysId + "\n";

            selectedBeacon.setBeaconData(beaconData);
            floorPlanDao.createOrUpdateFloorPlan(floorPlan);
            mainActivityView.displayDataReadFromBeacon(beaconDataString);
        }

        @Override
        public void onBleConnEvent(int event) {
            mainActivityView.displayBleConnEvent(event);
        }
    };

    public void setSelectedBeacon(UIBeacon selectedBeacon) {
        this.selectedBeacon = selectedBeacon;
    }

    public boolean toggleScanner() {
        if (!bleScanner.getScanningEnabled()) {
            bleScanner.scanLeDevice(true);
            return false;
        } else {
            bleScanner.scanLeDevice(false);
            return true;
        }
    }


}
