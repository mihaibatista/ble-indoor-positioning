package com.indoorposition.presentation.presenter;

import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;

import java.util.ArrayList;

/**
 * Created by hayabusa on 24.05.2016.
 */
public interface MainActivityView {

    public void displayUserLocation(UIPoint userLocation);

    public void displayDataReadFromBeacon(String beaconData);

    public void beaconsUsedForLocating(ArrayList<UIBeacon> beacons);

    public void displayBleConnEvent(int event);

    public void readBeaconDataSuccess();

    public void readBeaconDataFailed();

}
