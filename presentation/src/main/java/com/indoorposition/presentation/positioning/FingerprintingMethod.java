package com.indoorposition.presentation.positioning;

import com.indoorposition.presentation.ble.IBeacon;
import com.indoorposition.presentation.model.Fingerprint;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.Sampling;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by hayabusa on 18.05.2016.
 */
public class FingerprintingMethod {

    private FloorPlan floorPlan;

    public FingerprintingMethod(FloorPlan floorPlan) {
        this.floorPlan = floorPlan;
    }

    public boolean isValid(ArrayList<IBeacon> beacons) {
        if (beacons.size() > 1) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isBeaconOnMap(IBeacon beacon) {
        boolean isValid = false;
        for (UIBeacon b : floorPlan.getBeacons()) {
            if (b.getMajor() == beacon.getMajor() &&
                    b.getMinor() == beacon.getMinor()) {
                isValid = true;
            }
        }
        return isValid;
    }

    public UIPoint computeLocation(ArrayList<IBeacon> beacons) {
        ArrayList<Sampling> samplings = new ArrayList<>();
        for (IBeacon beacon : beacons) {
            if (isBeaconOnMap(beacon)) {
                Sampling foundSampling = null;
                for (Sampling sampling : samplings) {
                    if (sampling.getBeacon().getMajor() == beacon.getMajor() &&
                            sampling.getBeacon().getMinor() == beacon.getMinor()) {
                        foundSampling = sampling;
                        break;
                    }
                }
                if (foundSampling != null) {
                    ArrayList<ArrayList<Integer>> rList = foundSampling.getRssiList();
                    if (rList.size() > 0) {
                        rList.get(0).add(beacon.getRssi());
                    }
                } else {
                    UIBeacon uiBeacon = new UIBeacon(beacon.getMajor(), beacon.getMinor());
                    Sampling sampling = new Sampling(uiBeacon);
                    ArrayList<Integer> rL = new ArrayList<>();
                    rL.add(beacon.getRssi());
                    sampling.getRssiList().add(rL);
                    samplings.add(sampling);
                }
            }
        }

        for (Fingerprint fingerprint : floorPlan.getFingerprints()) {
            Double rssiDiff = 0.0;
            for (Sampling onlineSampling : samplings) {
                Sampling fingerprintSampling = fingerprint.getSamplingForBeacon(
                        onlineSampling.getBeacon().getMajor(),
                        onlineSampling.getBeacon().getMinor());
                rssiDiff += Math.abs(onlineSampling.computeAverageRssi() -
                        fingerprintSampling.computeAverageRssi());
            }
            fingerprint.setRssiDiff(rssiDiff);
        }

        Collections.sort(floorPlan.getFingerprints(), new Comparator<Fingerprint>() {
            @Override
            public int compare(Fingerprint lhs, Fingerprint rhs) {
                if (lhs.getRssiDiff() < rhs.getRssiDiff()) {
                    return -1;
                } else if (lhs.getRssiDiff() == rhs.getRssiDiff()) {
                    return  0;
                } else {
                    return 1;
                }
            }

            @Override
            public boolean equals(Object object) {
                return false;
            }
        });


        double xMultiplied = 1;
        double yMultiplied = 1;
        for (int i=0; i<3; ++i) {
            if (i < floorPlan.getFingerprints().size()) {
                Fingerprint fingerprint = floorPlan.getFingerprints().get(i);
                xMultiplied *= fingerprint.getxCellCenter();
                yMultiplied *= fingerprint.getyCellCenter();
            }
        }

        double xFinal = Math.pow(xMultiplied, 0.333);
        double yFinal = Math.pow(yMultiplied, 0.333);

        UIPoint userPoint = new UIPoint((int) xFinal, (int) yFinal);
        userPoint.r = 10;
        return userPoint;
    }

    public UIPoint computeLocation4Dir(ArrayList<IBeacon> beacons) {
        ArrayList<Sampling> samplings = new ArrayList<>();
        for (IBeacon beacon : beacons) {
            if (isBeaconOnMap(beacon)) {
                Sampling foundSampling = null;
                for (Sampling sampling : samplings) {
                    if (sampling.getBeacon().getMajor() == beacon.getMajor() &&
                            sampling.getBeacon().getMinor() == beacon.getMinor()) {
                        foundSampling = sampling;
                        break;
                    }
                }
                if (foundSampling != null) {
                    ArrayList<ArrayList<Integer>> rList = foundSampling.getRssiList();
                    if (rList.size() > 0) {
                        rList.get(0).add(beacon.getRssi());
                    }
                } else {
                    UIBeacon uiBeacon = new UIBeacon(beacon.getMajor(), beacon.getMinor());
                    Sampling sampling = new Sampling(uiBeacon);
                    ArrayList<Integer> rL = new ArrayList<>();
                    rL.add(beacon.getRssi());
                    sampling.getRssiList().add(rL);
                    samplings.add(sampling);
                }
            }
        }

        for (Fingerprint fingerprint : floorPlan.getFingerprints()) {
            Double rssiDiff = 0.0;
            for (Sampling onlineSampling : samplings) {
                Sampling fingerprintSampling = fingerprint.getSamplingForBeacon(
                        onlineSampling.getBeacon().getMajor(),
                        onlineSampling.getBeacon().getMinor());
                rssiDiff += Math.abs(onlineSampling.computeBestAverageRssi() -
                        fingerprintSampling.computeBestAverageRssi());
            }
            fingerprint.setRssiDiff(rssiDiff);
        }

        Collections.sort(floorPlan.getFingerprints(), new Comparator<Fingerprint>() {
            @Override
            public int compare(Fingerprint lhs, Fingerprint rhs) {
                if (lhs.getRssiDiff() < rhs.getRssiDiff()) {
                    return -1;
                } else if (lhs.getRssiDiff() == rhs.getRssiDiff()) {
                    return  0;
                } else {
                    return 1;
                }
            }

            @Override
            public boolean equals(Object object) {
                return false;
            }
        });


        double xMultiplied = 1;
        double yMultiplied = 1;
        for (int i=0; i<3; ++i) {
            if (i < floorPlan.getFingerprints().size()) {
                Fingerprint fingerprint = floorPlan.getFingerprints().get(i);
                xMultiplied *= fingerprint.getxCellCenter();
                yMultiplied *= fingerprint.getyCellCenter();
            }
        }

        double xFinal = Math.pow(xMultiplied, 0.333);
        double yFinal = Math.pow(yMultiplied, 0.333);

        UIPoint userPoint = new UIPoint((int) xFinal, (int) yFinal);
        userPoint.r = 10;
        return userPoint;
    }
}
