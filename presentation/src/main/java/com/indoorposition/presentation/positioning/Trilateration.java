package com.indoorposition.presentation.positioning;

import com.indoorposition.presentation.ble.IBeacon;
import com.indoorposition.presentation.model.DoublePoint;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.Scale;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by hayabusa on 09.05.2016.
 */
public class Trilateration {

    private FloorPlan floorPlan;
    private ArrayList<UIBeacon> beaconsUsed = new ArrayList<>();

    public Trilateration(FloorPlan floorPlan) {
        this.floorPlan = floorPlan;
    }

    private ArrayList<UIBeacon> computeBeaconsForTrilateration(ArrayList<IBeacon> beacons) {
        ArrayList<UIBeacon> uiBeacons = floorPlan.getBeacons();
        for (UIBeacon uiBeacon : uiBeacons) {
            uiBeacon.getDistanceList().clear();
        }
        for (IBeacon beacon : beacons) {
            for (UIBeacon uiBeacon : uiBeacons) {
                if (beacon.getMajor() == uiBeacon.getMajor() &&
                        beacon.getMinor() == uiBeacon.getMinor()) {
                    uiBeacon.getDistanceList().add(beacon.getAccuracy() * 100);
                    break;
                }
            }
        }
        ArrayList<UIBeacon> beaconsForTrilateration = new ArrayList<>();
        for (UIBeacon uiBeacon : uiBeacons) {
            if (uiBeacon.getDistanceList().size() > 0) {
                beaconsForTrilateration.add(uiBeacon);
            }
        }
        return beaconsForTrilateration;
    }

    public ArrayList<UIBeacon> getBeaconsUsed(ArrayList<IBeacon> beacons) {
        return computeBeaconsForTrilateration(beacons);
    }

    public boolean isValid(ArrayList<IBeacon> beacons) {
        if (computeBeaconsForTrilateration(beacons).size() > 2) {
            return true;
        } else {
            return false;
        }
    }

    /*public UIPoint computeLocation2(ArrayList<IBeacon> beacons) {
        ArrayList<UIBeacon> beaconsForTrilateration = computeBeaconsForTrilateration(beacons);

        if (beaconsForTrilateration.size() > 2) {
            double[][] positions = new double[beaconsForTrilateration.size()][2];
            double[] distances = new double[beaconsForTrilateration.size()];

            for (int i = 0; i < beaconsForTrilateration.size(); ++i) {
                UIBeacon beacon = beaconsForTrilateration.get(i);
                double dist = 0;
                for (Double d : beacon.getDistanceList()) {
                    dist += d;
                }

                distances[i] = dist / beacon.getDistanceList().size();

                Log.d("UPo", "Beacon " + beacon.getMajor() + "/" +
                beacon.getMinor() + " distance is " + beacon.getDistanceList());
                positions[i][0] = beacon.getPositionCm().x;
                positions[i][1] = beacon.getPositionCm().y;
                Log.d("UPo", "Beacon pos is x=" + positions[i][0] +
                " and y= " + positions[i][1]);
            }

            NonLinearLeastSquaresSolver solver =
                    new NonLinearLeastSquaresSolver(new TrilaterationFunction(positions, distances), new LevenbergMarquardtOptimizer());
            LeastSquaresOptimizer.Optimum optimum = solver.solve();
            double[] calculatedPosition = optimum.getPoint().toArray();
            for (int i = 0; i < calculatedPosition.length; ++i) {
                Log.d("UserPos",
                        "User pos" + i + " is " + calculatedPosition[i]);
            }

            Scale scale = floorPlan.getScale();

            RealVector standardDeviation = optimum.getSigma(0);
            double[] standardDev = standardDeviation.toArray();
            int standardDevCm = (int) standardDev[0];
            int standardDevPx = scale.transformCmInPx(standardDevCm);
            Log.d("UPo", "Stand dev original " + standardDev[0]);
            Log.d("UPo", "Stand dev " + scale.transformCmInPx(standardDevPx));


            double xcm = calculatedPosition[0];
            double ycm = calculatedPosition[1];


            int xpx = scale.transformCmInPx((int)xcm);
            int ypx = scale.transformCmInPx((int)ycm);
            UIPoint location = new UIPoint(xpx, ypx);
            location.r = standardDevPx;
            return location;
        }

        return new UIPoint(-1, -1);
    }*/

    public UIPoint computeLocation(ArrayList<IBeacon> beacons) {
        ArrayList<UIBeacon> pointsForTrilateration = computeBeaconsForTrilateration(beacons);
        beaconsUsed.clear();
        if (pointsForTrilateration.size() > 2) {
            UIBeacon beacon1 = pointsForTrilateration.get(0);
            UIBeacon beacon2 = pointsForTrilateration.get(1);
            UIBeacon beacon3 = pointsForTrilateration.get(2);
            beaconsUsed.add(beacon1);
            beaconsUsed.add(beacon2);
            beaconsUsed.add(beacon3);

            double[] distances = new double[pointsForTrilateration.size()];

            for (int i = 0; i < pointsForTrilateration.size(); ++i) {
                UIBeacon beacon = pointsForTrilateration.get(i);
                double dist = 0;
                for (Double d : beacon.getDistanceList()) {
                    dist += d;
                }

                distances[i] = dist / beacon.getDistanceList().size();
            }

            Scale scale = floorPlan.getScale();

            UIPoint P2MinusP1 = new UIPoint(beacon2.getPositionCm().x - beacon1.getPositionCm().x,
                    beacon2.getPositionCm().y - beacon1.getPositionCm().y);
            UIPoint P3MinusP1 = new UIPoint(beacon3.getPositionCm().x - beacon1.getPositionCm().x,
                    beacon3.getPositionCm().y - beacon1.getPositionCm().y);
            double magP2MinusP1 = Math.sqrt(Math.pow(P2MinusP1.x, 2) + Math.pow(P2MinusP1.y, 2));
            DoublePoint eX = new DoublePoint(P2MinusP1.x / magP2MinusP1,
                    P2MinusP1.y / magP2MinusP1);
            double i = eX.x * P3MinusP1.x + eX.y * P3MinusP1.y;
            DoublePoint eXTimesI = new DoublePoint(eX.x * i, eX.y * i);
            DoublePoint P3MinusP1MinusITimesEX = new DoublePoint(P3MinusP1.x - eXTimesI.x, P3MinusP1.y - eXTimesI.y);
            double magP3MinusP1MinusITimesEX = Math.sqrt(Math.pow(P3MinusP1MinusITimesEX.x, 2) + Math.pow(P3MinusP1MinusITimesEX.y, 2));
            DoublePoint eY = new DoublePoint(P3MinusP1MinusITimesEX.x / magP3MinusP1MinusITimesEX, P3MinusP1MinusITimesEX.y / magP3MinusP1MinusITimesEX);
            double j = eY.x * P3MinusP1.x + eY.y * P3MinusP1.y;
            double x = (Math.pow(distances[0], 2) - Math.pow(distances[1], 2) + Math.pow(magP2MinusP1, 2)) / (2 * magP2MinusP1);
            double y = (Math.pow(distances[0], 2) - Math.pow(distances[2], 2) + Math.pow(i, 2) + Math.pow(j, 2)) / (2 * j) - (i * x) / j;

            UIPoint userLocation = new UIPoint(scale.transformCmInPx((int)(beacon1.getPositionCm().x + x * eX.x + y * eY.x)),
                    scale.transformCmInPx((int) (beacon1.getPositionCm().y + x * eX.y + y * eY.y)));
            return userLocation;
        }
        return null;
    }

    public UIPoint computeLocationFiltered(ArrayList<IBeacon> beacons) {
        ArrayList<UIBeacon> pointsForTrilateration = computeBeaconsForTrilateration(beacons);
        beaconsUsed.clear();
        if (pointsForTrilateration.size() > 2) {
            UIBeacon beacon1 = pointsForTrilateration.get(0);
            UIBeacon beacon2 = pointsForTrilateration.get(1);
            UIBeacon beacon3 = pointsForTrilateration.get(2);

            beaconsUsed.add(beacon1);
            beaconsUsed.add(beacon2);
            beaconsUsed.add(beacon3);

            double[] distances = new double[pointsForTrilateration.size()];

            boolean executeTrilateration = true;

            for (int i = 0; i < pointsForTrilateration.size(); ++i) {
                UIBeacon beacon = pointsForTrilateration.get(i);
                double dist = 0;

                ArrayList<Double> distanceList = beacon.getDistanceList();
                Collections.sort(distanceList, new Comparator<Double>() {
                    @Override
                    public int compare(Double lhs, Double rhs) {
                        int res = -1;
                        if (lhs < rhs) {
                            res = -1;
                        } else if (lhs == rhs) {
                            res = 0;
                        } else {
                            res = 1;
                        }
                        return res;
                    }
                });

                if (distanceList.size() > 2) {
                    distanceList.remove(0);
                    distanceList.remove(distanceList.size() - 1);

                    for (Double d : distanceList) {
                        dist += d;
                    }

                    distances[i] = dist / distanceList.size();
                } else {
                    executeTrilateration = false;
                }

            }


            if (executeTrilateration) {
                Scale scale = floorPlan.getScale();

                UIPoint P2MinusP1 = new UIPoint(beacon2.getPositionCm().x - beacon1.getPositionCm().x,
                        beacon2.getPositionCm().y - beacon1.getPositionCm().y);
                UIPoint P3MinusP1 = new UIPoint(beacon3.getPositionCm().x - beacon1.getPositionCm().x,
                        beacon3.getPositionCm().y - beacon1.getPositionCm().y);
                double magP2MinusP1 = Math.sqrt(Math.pow(P2MinusP1.x, 2) + Math.pow(P2MinusP1.y, 2));
                DoublePoint eX = new DoublePoint(P2MinusP1.x / magP2MinusP1,
                        P2MinusP1.y / magP2MinusP1);
                double i = eX.x * P3MinusP1.x + eX.y * P3MinusP1.y;
                DoublePoint eXTimesI = new DoublePoint(eX.x * i, eX.y * i);
                DoublePoint P3MinusP1MinusITimesEX = new DoublePoint(P3MinusP1.x - eXTimesI.x, P3MinusP1.y - eXTimesI.y);
                double magP3MinusP1MinusITimesEX = Math.sqrt(Math.pow(P3MinusP1MinusITimesEX.x, 2) + Math.pow(P3MinusP1MinusITimesEX.y, 2));
                DoublePoint eY = new DoublePoint(P3MinusP1MinusITimesEX.x / magP3MinusP1MinusITimesEX, P3MinusP1MinusITimesEX.y / magP3MinusP1MinusITimesEX);
                double j = eY.x * P3MinusP1.x + eY.y * P3MinusP1.y;
                double x = (Math.pow(distances[0], 2) - Math.pow(distances[1], 2) + Math.pow(magP2MinusP1, 2)) / (2 * magP2MinusP1);
                double y = (Math.pow(distances[0], 2) - Math.pow(distances[2], 2) + Math.pow(i, 2) + Math.pow(j, 2)) / (2 * j) - (i * x) / j;

                UIPoint userLocation = new UIPoint(scale.transformCmInPx((int) (beacon1.getPositionCm().x + x * eX.x + y * eY.x)),
                        scale.transformCmInPx((int) (beacon1.getPositionCm().y + x * eX.y + y * eY.y)));
                return userLocation;
            } else {
                return null;
            }
        }
        return null;
    }
}
