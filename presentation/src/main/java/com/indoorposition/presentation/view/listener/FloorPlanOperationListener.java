package com.indoorposition.presentation.view.listener;

import com.indoorposition.presentation.model.FloorPlan;

/**
 * Created by hayabusa on 08.05.2016.
 */
public interface FloorPlanOperationListener {

    public void loadFloorPlan(FloorPlan floorPlan);

    public void editFloorPlan(FloorPlan floorPlan);

    public void deleteFloorPlan(FloorPlan floorPlan);

    public void trainFloorPlan(FloorPlan floorPlan);

}
