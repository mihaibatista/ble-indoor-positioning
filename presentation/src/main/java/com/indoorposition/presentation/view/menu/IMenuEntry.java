package com.indoorposition.presentation.view.menu;

import android.view.MenuItem;

/**
 * Created by hayabusa on 07.05.2016.
 */
public interface IMenuEntry {

    public void select();

    public void deselect();

    public void setMenuItem(MenuItem menuItem);

}
