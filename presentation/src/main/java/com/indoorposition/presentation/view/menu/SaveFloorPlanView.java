package com.indoorposition.presentation.view.menu;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.indoorposition.presentation.db.dao.FloorPlanDao;
import com.indoorposition.presentation.db.dao.IFloorPlanDao;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.view.dialog.SaveDialogFragment;
import com.indoorposition.presentation.view.dialog.WarningsDialogFragment;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class SaveFloorPlanView implements IMenuEntry {

    private AppCompatActivity activity;
    private FloorPlan floorPlan;
    private SaveDialogFragment saveDialog;
    private WarningsDialogFragment warningsDialog;
    private IFloorPlanDao floorPlanDao;
    private PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
    private MenuItem menuItem;

    public SaveFloorPlanView(AppCompatActivity activity, FloorPlan floorPlan) {
        this.activity = activity;
        this.floorPlan = floorPlan;
        this.saveDialog = new SaveDialogFragment();
        this.warningsDialog = new WarningsDialogFragment();
        this.floorPlanDao = new FloorPlanDao(this.activity);
    }

    @Override
    public void select() {
        if (floorPlan.isNameSet()) {
            floorPlanDao.createOrUpdateFloorPlan(floorPlan);
            String floorPlanName = floorPlan.getName();
            Snackbar.make(activity.findViewById(android.R.id.content),
                    "Floor plan " + floorPlanName + " saved!",
                    Snackbar.LENGTH_LONG)
                    .setActionTextColor(Color.WHITE)
                    .show();
        } else {
            saveDialog.setType(SaveDialogFragment.SAVE_MAP);
            saveDialog.show(activity.getSupportFragmentManager(),
                    "save_floor_plan");
        }
        if (!floorPlan.isValid()) {
            warningsDialog.setWarningMessage(floorPlan.getWarningsMessage());
            warningsDialog.show(activity.getSupportFragmentManager(),
                    "warnings_floor_plan");
        }
    }

    @Override
    public void deselect() {
        menuItem.getIcon().setColorFilter(Color.BLACK, mode);
    }

    @Override
    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
}
