package com.indoorposition.presentation.view.listener;

import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;

/**
 * Created by hayabusa on 06.05.2016.
 */
public interface FPCanvasInteractionListener {

    public void onBeaconPlacedOnMap(UIBeacon beacon);

    public void onBeaconSelected(UIBeacon beacon);

    public void onPositionSelected(UIPoint uiPoint);

}
