package com.indoorposition.presentation.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.view.listener.FloorPlanOperationListener;

import java.util.ArrayList;

/**
 * Created by hayabusa on 06.05.2016.
 */
public class FloorPlanAdapter extends BaseAdapter {

    private ArrayList<FloorPlan> floorPlanList = new ArrayList<FloorPlan>();
    private LayoutInflater mInflater;
    private Context context;
    private FloorPlanOperationListener floorPlanOperationListener;

    public FloorPlanAdapter(Context context) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
    }

    public void deleteItem(FloorPlan item) {
        floorPlanList.remove(item);
        notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView floorPlanTitle;
        public Button loadFloorPlanBtn;
        public Button editFloorPlanBtn;
        public Button deleteFloorPlanBtn;
        public Button trainPlanBtn;
        public ImageView floorPlanValidIcon;
    }

    public void addFloorPlanList(ArrayList<FloorPlan> floorPlanList) {
        this.floorPlanList.addAll(floorPlanList);
        notifyDataSetChanged();
    }

    public void clearFloorPlanList() {
        this.floorPlanList.clear();
        notifyDataSetChanged();
    }

    public void setFloorPlanOperationListener(FloorPlanOperationListener listener) {
        this.floorPlanOperationListener = listener;
    }

    @Override
    public int getCount() {
        return floorPlanList.size();
    }

    @Override
    public FloorPlan getItem(int position) {
        return floorPlanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        ViewHolder holder;

        if (rowView == null) {
            rowView = mInflater.inflate(R.layout.item_floor_plan, parent, false);
            holder = new ViewHolder();
            holder.floorPlanTitle = (TextView) rowView.findViewById(R.id.floor_plan_title);
            holder.loadFloorPlanBtn = (Button) rowView.findViewById(R.id.load_floor_plan_btn);
            holder.editFloorPlanBtn = (Button) rowView.findViewById(R.id.edit_floor_plan_btn);
            holder.deleteFloorPlanBtn = (Button) rowView.findViewById(R.id.delete_floor_plan_btn);
            holder.trainPlanBtn = (Button) rowView.findViewById(R.id.train_plan_btn);
            holder.floorPlanValidIcon = (ImageView) rowView.findViewById(R.id.floor_plan_icon);

            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        final FloorPlan floorPlan = floorPlanList.get(position);
        if (!floorPlan.isValid()) {
            holder.loadFloorPlanBtn.setVisibility(View.GONE);
            holder.trainPlanBtn.setVisibility(View.GONE);
            Drawable invalidIcon = context.getResources().getDrawable(
                    R.drawable.ic_report_problem_black_24dp);
            holder.floorPlanValidIcon.setImageDrawable(invalidIcon);
            holder.floorPlanValidIcon.setColorFilter(Color.argb(255, 255, 153, 0));
        } else {
            Drawable validIcon = context.getResources().getDrawable(
                    R.drawable.ic_done_black_24dp);
            holder.floorPlanValidIcon.setImageDrawable(validIcon);
            holder.floorPlanValidIcon.setColorFilter(Color.argb(255, 170, 242, 0));

            holder.loadFloorPlanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (floorPlanOperationListener != null) {
                        floorPlanOperationListener.loadFloorPlan(floorPlan);
                    }
                }
            });

            holder.trainPlanBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (floorPlanOperationListener != null) {
                        floorPlanOperationListener.trainFloorPlan(floorPlan);
                    }
                }
            });

        }

        String title = floorPlan.getName();
        holder.floorPlanTitle.setText(title);

        holder.editFloorPlanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (floorPlanOperationListener != null) {
                    floorPlanOperationListener.editFloorPlan(floorPlan);
                }
            }
        });

        holder.deleteFloorPlanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (floorPlanOperationListener != null) {
                    floorPlanOperationListener.deleteFloorPlan(floorPlan);
                }
            }
        });

        return rowView;
    }
}
