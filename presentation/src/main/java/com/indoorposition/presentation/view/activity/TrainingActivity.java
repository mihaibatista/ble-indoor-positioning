package com.indoorposition.presentation.view.activity;

import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.ble.BleResultListener;
import com.indoorposition.presentation.ble.BleScanner;
import com.indoorposition.presentation.ble.IBeacon;
import com.indoorposition.presentation.db.dao.FloorPlanDao;
import com.indoorposition.presentation.db.dao.IFloorPlanDao;
import com.indoorposition.presentation.model.Fingerprint;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.Scale;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.view.canvas.TrainingCanvas;
import com.indoorposition.presentation.view.dialog.NumberPickerDialogFragment;
import com.indoorposition.presentation.view.util.ProgressBarWrapper;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hayabusa on 16.05.2016.
 */
public class TrainingActivity extends AppCompatActivity
        implements NumberPickerDialogFragment.CellSizeDialogListener,
        NumberPickerDialogFragment.NoOfSamplesListener,
        BleResultListener, FloorPlan.FingerprintingListener {

    @Bind(R.id.training_canvas) public TrainingCanvas trainingCanvas;
    private NavigationElement navigationElement;
    @Bind(R.id.cell_nav_up) public Button cellNavUpBtn;
    @Bind(R.id.cell_nav_down) public Button cellNavDownBtn;
    @Bind(R.id.cell_nav_left) public Button cellNavLeftBtn;
    @Bind(R.id.cell_nav_right) public Button cellNavRightBtn;
    @Bind(R.id.cell_navigation_details) public LinearLayout cellNavSection;
    @Bind(R.id.cell_size_info) public TextView cellSizeInfo;
    @Bind(R.id.gather_fingerprint) public Button gatherFingerprintBtn;
    @Bind(R.id.reset_fingerprint) public Button resetFingerprintBtn;
    @Bind(R.id.fingerprinting_progress_section) public LinearLayout fingerprintingProgressSection;
    @Bind(R.id.cancel_fingerprint) public Button cancelFingerprintBtn;
    @Bind(R.id.next_direction_fingerprint) public Button nextDirectionFingerprint;

    // Db
    private IFloorPlanDao floorPlanDao;

    private FloorPlan floorPlan;
    private Menu menu;
    private NumberPickerDialogFragment numberPickerDialog;
    private Point currentCell;

    private ArrayList<ProgressBarWrapper> progressBarWrappers = new ArrayList<>();

    private BleScanner bleScanner;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        FloorPlan floorPlanExtra = (FloorPlan)
                intent.getSerializableExtra("floorPlan");
        if (floorPlanExtra != null) {
            this.floorPlan = floorPlanExtra;
        } else {
            this.floorPlan = new FloorPlan();
        }
        floorPlan.setFingerprintingListener(this);

        numberPickerDialog = new NumberPickerDialogFragment();
        numberPickerDialog.setDefaultCellSize(floorPlan.getGridCellSizePx());
        numberPickerDialog.setDefaultSamplingNo(floorPlan.getNoOfSamples());

        trainingCanvas.setFloorPlan(floorPlan);
        trainingCanvas.setGridCellSizePx(floorPlan.getGridCellSizePx());
        trainingCanvas.refreshCanvas();

        // Db
        this.floorPlanDao = new FloorPlanDao(this);

        bleScanner = new BleScanner(this);
        bleScanner.setBleResultListener(this);

        // Setup menu
        navigationElement = new NavigationElement();
        navigationElement.onCreate(this);

        cellNavDownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentCell = trainingCanvas.selectDownVerticalCell();
                Fingerprint fingerprint = floorPlan.getFingerprint(currentCell.x,
                        currentCell.y);
                displayFingerprintSection(fingerprint);
            }
        });

        cellNavRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentCell = trainingCanvas.selectRightHorizontalCell();
                Fingerprint fingerprint = floorPlan.getFingerprint(currentCell.x,
                        currentCell.y);
                displayFingerprintSection(fingerprint);
            }
        });
        cellNavUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentCell = trainingCanvas.selectUpVerticalCell();
                Fingerprint fingerprint = floorPlan.getFingerprint(currentCell.x,
                        currentCell.y);
                displayFingerprintSection(fingerprint);
            }
        });
        cellNavLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentCell = trainingCanvas.selectLeftHorizontalCell();
                Fingerprint fingerprint = floorPlan.getFingerprint(currentCell.x,
                        currentCell.y);
                displayFingerprintSection(fingerprint);
            }
        });
        gatherFingerprintBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bleScanner.scanLeDevice(true);
                gatherFingerprintBtn.setVisibility(View.GONE);
                cancelFingerprintBtn.setVisibility(View.VISIBLE);
                resetFingerprintBtn.setVisibility(View.GONE);
                nextDirectionFingerprint.setVisibility(View.GONE);

                cellNavUpBtn.setEnabled(false);
                cellNavDownBtn.setEnabled(false);
                cellNavLeftBtn.setEnabled(false);
                cellNavRightBtn.setEnabled(false);
            }
        });

        nextDirectionFingerprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floorPlan.nextSamplingDirection(currentCell.x, currentCell.y);
                gatherFingerprintBtn.setVisibility(View.VISIBLE);
                cancelFingerprintBtn.setVisibility(View.GONE);
                resetFingerprintBtn.setVisibility(View.VISIBLE);

                cellNavUpBtn.setEnabled(false);
                cellNavDownBtn.setEnabled(false);
                cellNavLeftBtn.setEnabled(false);
                cellNavRightBtn.setEnabled(false);
            }
        });

        cancelFingerprintBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bleScanner.scanLeDevice(false);
                fingerprintingProgressSection.removeAllViews();
                progressBarWrappers.clear();
                floorPlan.removeFingerprint(currentCell.x, currentCell.y);
                trainingCanvas.refreshCanvas();

                cellNavUpBtn.setEnabled(true);
                cellNavDownBtn.setEnabled(true);
                cellNavLeftBtn.setEnabled(true);
                cellNavRightBtn.setEnabled(true);


                resetFingerprintBtn.setVisibility(View.GONE);
                gatherFingerprintBtn.setVisibility(View.VISIBLE);
                cancelFingerprintBtn.setVisibility(View.GONE);
            }
        });

        resetFingerprintBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                floorPlan.removeFingerprint(currentCell.x, currentCell.y);
                trainingCanvas.refreshCanvas();

                resetFingerprintBtn.setVisibility(View.GONE);
                gatherFingerprintBtn.setVisibility(View.VISIBLE);
                cancelFingerprintBtn.setVisibility(View.GONE);
            }
        });
    }

    private void displayFingerprintSection(Fingerprint fingerprint) {
        if (fingerprint != null) {
            if (fingerprint.isReady(floorPlan.getNoOfSamples(),
                    floorPlan.getBeacons().size())) {
                resetFingerprintBtn.setVisibility(View.VISIBLE);
                gatherFingerprintBtn.setVisibility(View.GONE);
                nextDirectionFingerprint.setVisibility(View.VISIBLE);
            } else {
                gatherFingerprintBtn.setVisibility(View.VISIBLE);
                resetFingerprintBtn.setVisibility(View.GONE);
                nextDirectionFingerprint.setVisibility(View.GONE);
            }
        } else {
            gatherFingerprintBtn.setVisibility(View.VISIBLE);
            resetFingerprintBtn.setVisibility(View.GONE);
            nextDirectionFingerprint.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        navigationElement.onPostCreate();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.select_grid_cell:
                trainingCanvas.selectFirstCell();
                cellNavSection.setVisibility(View.VISIBLE);
                Scale scale = floorPlan.getScale();
                int cellDimInCm = scale.transformPxInCm(trainingCanvas.getGridCellSizePx());
                cellSizeInfo.setText("Dimensiunea unei celule este " + cellDimInCm +
                        " x " + cellDimInCm + " cm");
                trainingCanvas.enableCellNavigation();
                break;
            case R.id.setup_grid_cell_size:
                numberPickerDialog.setDialogType(NumberPickerDialogFragment.CELL_SIZE_DIALOG);
                numberPickerDialog.setDefaultCellSize(floorPlan.getGridCellSizePx());
                numberPickerDialog.show(getFragmentManager(), "cell size frag");
                break;
            case R.id.reset_training:
                floorPlan.clearFingerprints();
                trainingCanvas.refreshCanvas();
                break;
            case R.id.save_training:
                floorPlanDao.createOrUpdateFloorPlan(floorPlan);
                String floorPlanName = floorPlan.getName();
                Snackbar.make(this.findViewById(android.R.id.content),
                        "Floor plan " + floorPlanName + " saved!",
                        Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.WHITE)
                        .show();
                break;
            case R.id.no_of_samples_item:
                numberPickerDialog.setDialogType(NumberPickerDialogFragment.NO_OF_SAMPLES);
                numberPickerDialog.setDefaultSamplingNo(floorPlan.getNoOfSamples());
                numberPickerDialog.show(getFragmentManager(), "cell size frag");
                break;
            default:

                break;

        }

        if (navigationElement.onOptionsItemSelected(item)) {
            return true;
        }

        return true;
    }

    public void initializeToolbarSelection() {
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
        MenuItem selectGridCellMenuItem = menu.findItem(R.id.select_grid_cell);
        selectGridCellMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_training, menu);
        this.menu = menu;
        initializeToolbarSelection();
        return true;
    }

    @Override
    public void onCellDialogPositiveClick(DialogFragment dialog, int cellSize) {
        floorPlan.setGridCellSizePx(cellSize);
        trainingCanvas.setGridCellSizePx(cellSize);
        Scale scale = floorPlan.getScale();
        int cellDimInCm = scale.transformPxInCm(trainingCanvas.getGridCellSizePx());
        cellSizeInfo.setText("Dimensiunea unei celule este " + cellDimInCm +
                " x " + cellDimInCm + " cm");
    }

    @Override
    public void onSamplesDialogPositiveClick(DialogFragment dialog, int noOfSamples) {
        floorPlan.setNoOfSamples(noOfSamples);
    }

    @Override
    public void onScanResults(ArrayList<IBeacon> beacons) {
        floorPlan.onScanResults(beacons, currentCell.x, currentCell.y);
    }

    @Override
    public void onSamplingUpdated(UIBeacon beacon, int noOfSamples, int x, int y) {
        ProgressBarWrapper foundPbw = null;
        for (ProgressBarWrapper progressBarWrapper : progressBarWrappers) {
            if (beacon.getMajor() == progressBarWrapper.getUiBeacon().getMajor() &&
                    beacon.getMinor() == progressBarWrapper.getUiBeacon().getMinor()) {
                foundPbw = progressBarWrapper;
                break;
            }
        }
        if (foundPbw != null) {
            ProgressBar progressBar = foundPbw.getProgressBar();
            int progress = (int) ((double) noOfSamples / floorPlan.getNoOfSamples() * 100);
            progressBar.setProgress(progress);
        } else {
            ProgressBar progressBar = new ProgressBar(fingerprintingProgressSection.getContext(),
                    null, android.R.attr.progressBarStyleHorizontal);
            int progress = (int) ((double) noOfSamples / floorPlan.getNoOfSamples() * 100);
            progressBar.setProgress(progress);
            TextView beaconName = new TextView(fingerprintingProgressSection.getContext());
            fingerprintingProgressSection.addView(beaconName);
            fingerprintingProgressSection.addView(progressBar);
            beaconName.setText("Beacon " + beacon.getMajor() + "/" + beacon.getMinor());
            progressBar.setMinimumHeight(50);
            progressBar.setMinimumWidth(200);
            progressBar.setMax(100);
            progressBar.setIndeterminate(false);
            progressBar.setProgress(progress);
            ProgressBarWrapper progressBarWrapper = new ProgressBarWrapper(progressBar, beacon);
            progressBarWrappers.add(progressBarWrapper);
        }
    }

    @Override
    public void onFingerprintFinished(int x, int y) {
        bleScanner.scanLeDevice(false);
        fingerprintingProgressSection.removeAllViews();
        progressBarWrappers.clear();
        trainingCanvas.refreshCanvas();

        resetFingerprintBtn.setVisibility(View.VISIBLE);
        nextDirectionFingerprint.setVisibility(View.VISIBLE);
        gatherFingerprintBtn.setVisibility(View.GONE);
        cancelFingerprintBtn.setVisibility(View.GONE);

        cellNavUpBtn.setEnabled(true);
        cellNavDownBtn.setEnabled(true);
        cellNavLeftBtn.setEnabled(true);
        cellNavRightBtn.setEnabled(true);
    }
}
