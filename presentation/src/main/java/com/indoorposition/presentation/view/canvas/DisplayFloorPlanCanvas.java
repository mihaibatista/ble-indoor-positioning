package com.indoorposition.presentation.view.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;
import com.indoorposition.presentation.presenter.MainActivityPresenter;
import com.indoorposition.presentation.view.listener.FPCanvasInteractionListener;

import java.util.ArrayList;

/**
 * Created by hayabusa on 08.05.2016.
 */
public class DisplayFloorPlanCanvas extends BasicCanvas {

    private boolean beaconVisibility = false;
    private Bitmap mBeaconBmp;
    private Bitmap mBeaconBmpSignal1;
    private Bitmap mBeaconBmpSignal2;
    private Bitmap mBeaconBmpSignal3;
    private Bitmap mBlueDot;
    private FPCanvasInteractionListener interactionListener;
    private UIPoint userLocation;
    private Boolean collectData = false;
    private UIPoint realPosition;
    private ArrayList<UIPoint> computedLocationList;
    private ArrayList<UIPoint> realLocationList;
    private ArrayList<ArrayList<UIPoint>> allComputedLocationList;
    private ArrayList<ArrayList<UIPoint>> allRealLocationList;
    private ArrayList<Integer> colors;
    private ArrayList<Integer> availableColors;
    private boolean displayCollectedData = false;
    private int indexColor = 0;
    private int currentMethod = MainActivityPresenter.FINGERPRINTING;


    public DisplayFloorPlanCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        computedLocationList = new ArrayList<>();
        realLocationList = new ArrayList<>();
        allComputedLocationList = new ArrayList<>();
        allRealLocationList = new ArrayList<>();
        colors = new ArrayList<>();
        availableColors = new ArrayList<>();
        availableColors.add(Color.BLUE);
        availableColors.add(Color.DKGRAY);
        availableColors.add(Color.GREEN);
        availableColors.add(Color.MAGENTA);
        availableColors.add(Color.RED);
        availableColors.add(Color.YELLOW);
        availableColors.add(Color.CYAN);
        availableColors.add(Color.BLACK);
        mBeaconBmp = BitmapFactory.decodeResource(getResources(), R.drawable.beacon_50px);
        mBeaconBmpSignal1 = BitmapFactory.decodeResource(getResources(), R.drawable.beacon_50px_signal1);
        mBeaconBmpSignal2 = BitmapFactory.decodeResource(getResources(), R.drawable.beacon_50px_signal2);
        mBeaconBmpSignal3 = BitmapFactory.decodeResource(getResources(), R.drawable.beacon_50px_signal3);
        mBlueDot = BitmapFactory.decodeResource(getResources(), R.drawable.bluedot_50px);
    }

    public void resetRealPosition() {
        realPosition = null;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void setBeaconsVisibility(boolean visibility) {
        beaconVisibility = visibility;
        invalidate();
    }

    private ArrayList<UIPoint> copyList(ArrayList<UIPoint> originalList) {
        ArrayList<UIPoint> listCopied = new ArrayList<>();
        for (UIPoint point : originalList) {
            UIPoint copiedPoint = new UIPoint(point.x, point.y);
            listCopied.add(copiedPoint);
        }
        return listCopied;
    }

    public void nextCollectPosition() {
        ArrayList copiedComputedLocationList = copyList(computedLocationList);
        allComputedLocationList.add(copiedComputedLocationList);
        ArrayList copiedRealLocationList = copyList(realLocationList);
        allRealLocationList.add(copiedRealLocationList);
        colors.add(availableColors.get(allComputedLocationList.size() - 1 % (availableColors.size())));
        computedLocationList.clear();
        realLocationList.clear();
    }

    public boolean getBeaconVisibility() {
        return beaconVisibility;
    }

    public void setFPCanvasInteractionListener(FPCanvasInteractionListener listener) {
        this.interactionListener = listener;
    }

    public void setDisplayCollectedData(boolean flag) {
        displayCollectedData = flag;
    }

    public void setCollectData(boolean flag) {
        collectData = flag;
    }

    public void setPositioningMethod(int method) {
        this.currentMethod = method;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (beaconVisibility) {
            ArrayList<UIBeacon> uiBeacons = floorPlan.getBeacons();
            // Draw beacons
            for (UIBeacon beacon : uiBeacons) {
                Bitmap bitmapToDraw = mBeaconBmp;
                if (beacon.getSamplingStrength() == 1) {
                    bitmapToDraw = mBeaconBmpSignal1;
                } else if (beacon.getSamplingStrength() == 2) {
                    bitmapToDraw = mBeaconBmpSignal2;
                } else if (beacon.getSamplingStrength() > 2) {
                    bitmapToDraw = mBeaconBmpSignal3;
                }

                int beaconDistanceAvg = (int) computeAverage(beacon.getDistanceList());
                int beaconDistanceAvgPx = floorPlan.getScale().transformCmInPx(beaconDistanceAvg);

                if (currentMethod == MainActivityPresenter.TRILATERATION1_METHOD ||
                        currentMethod == MainActivityPresenter.TRILATERATION2_METHOD) {
                    /*canvas.drawCircle(beacon.getPositionPx().x + 25,
                            beacon.getPositionPx().y + 25,
                            beaconDistanceAvgPx,
                            paint);*/
                }
                canvas.drawBitmap(bitmapToDraw,
                        beacon.getPositionPx().x,
                        beacon.getPositionPx().y, null);
            }
        }

        if (userLocation != null) {
            paint.setColor(Color.argb(100, 51, 181, 229));
            canvas.drawCircle(userLocation.x,
                    userLocation.y, 10,
                    paint);
        }

        if (realPosition != null) {
            paint.setColor(Color.RED);
            canvas.drawCircle(realPosition.x,
                    realPosition.y, 10, paint);
        }

        if (displayCollectedData) {
            int i = 0;
            for (ArrayList<UIPoint> realPList : allRealLocationList) {
                paint.setColor(colors.get(i % colors.size()));
                paint.setAlpha(255);
                for (UIPoint p : realPList) {
                    canvas.drawCircle(p.x,
                            p.y, 10, paint);
                }

                ++i;
            }

            i = 0;
            for (ArrayList<UIPoint> computedList : allComputedLocationList) {
                paint.setColor(colors.get(i % colors.size()));
                paint.setAlpha(120);
                for (UIPoint computedPos : computedList) {
                    canvas.drawCircle(computedPos.x,
                            computedPos.y, 10,
                            paint);
                }

                ++i;
            }
        }
    }

    private double computeAverage(ArrayList<Double> list) {
        if (list.size() > 0) {
            double sum = 0.0;
            for (double d : list) {
                sum +=d;
            }
            return sum/list.size();
        } else {
            return  -1.0;
        }

    }

    public void cancelLastCollectedData() {
        this.computedLocationList.remove(this.computedLocationList.size() - 1);
        this.realLocationList.remove(this.realLocationList.size() - 1);
        this.realPosition = null;
    }

    public void setUserLocation(UIPoint userLocation) {
        this.userLocation = userLocation;
        if (collectData) {
            computedLocationList.add(userLocation);
        }
    }

    public void resetCollectedData() {
        computedLocationList.clear();
        realLocationList.clear();
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (beaconVisibility) {
                ArrayList<UIBeacon> uiBeacons = floorPlan.getBeacons();
                for (UIBeacon beacon : uiBeacons) {
                    if (Math.abs(beacon.getPositionPx().x - x) < 50 &&
                            Math.abs(beacon.getPositionPx().y - y) < 50) {
                        if (interactionListener != null) {
                            interactionListener.onBeaconSelected(beacon);
                        }
                    }
                }
            }
            if (collectData) {
                UIPoint realPos = new UIPoint((int) x, (int) y);
                realPosition = realPos;
                realLocationList.add(realPosition);
                if (interactionListener != null) {
                    interactionListener.onPositionSelected(realPosition);
                }
                invalidate();
            }
        }
        return true;
    }

}
