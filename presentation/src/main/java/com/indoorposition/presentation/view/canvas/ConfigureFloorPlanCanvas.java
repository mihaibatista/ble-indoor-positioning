package com.indoorposition.presentation.view.canvas;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;
import com.indoorposition.presentation.view.listener.FPCanvasInteractionListener;

import java.util.ArrayList;

/**
 * Created by hayabusa on 23.04.2016.
 */
public class ConfigureFloorPlanCanvas extends BasicCanvas {

    private boolean isDrawRoom = false;
    private boolean isBeaconPlacement = false;
    private FPCanvasInteractionListener floorPlanCanvasInteractionListener;
    private Bitmap mBeaconBmp;
    public static final int FREE_MODE = 0;
    public static final int SNAP_TO_GRID = 1;
    public static final int KEEP_X = 2;
    public static final int KEEP_Y = 3;
    private int drawMode = SNAP_TO_GRID;

    public ConfigureFloorPlanCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        mBeaconBmp = BitmapFactory.decodeResource(getResources(), R.drawable.beacon_50px);
    }

    public void enableBeaconPlacement() {
        isBeaconPlacement = true;
        disableFloorPlanDraw();
    }

    public void disableBeaconPlacement() {
        isBeaconPlacement = false;
    }

    public void enableFloorPlanDraw() {
        isDrawRoom = true;
        disableBeaconPlacement();
    }

    public void disableFloorPlanDraw() {
        isDrawRoom = false;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    public void addConfigureCanvasListener(FPCanvasInteractionListener listener) {
        this.floorPlanCanvasInteractionListener = listener;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        ArrayList<UIPoint> corners = floorPlan.getCorners();
        int lineIndex = floorPlan.getWallIndex();
        if (corners.size() > 1) {
            if (lineIndex > -1) {
                int i = floorPlan.getCurrentWall();
                Log.d("Highlight", "Line index is " + i + " and lines no " + corners.size());
                if (i < corners.size()) {
                    if (i < corners.size() - 1) {
                        UIPoint point1 = corners.get(i);
                        UIPoint point2 = corners.get(i + 1);
                        Path selectedLine = new Path();
                        selectedLine.moveTo(point1.x, point1.y);
                        selectedLine.lineTo(point2.x, point2.y);

                        paint.setColor(Color.argb(255, 255, 64, 129));
                        canvas.drawPath(selectedLine, paint);
                    } else if (i == (corners.size() - 1)) {
                        UIPoint point1 = corners.get(i);
                        UIPoint point2 = corners.get(0);
                        Path selectedLine = new Path();
                        selectedLine.moveTo(point1.x, point1.y);
                        selectedLine.lineTo(point2.x, point2.y);

                        paint.setColor(Color.argb(255, 255, 64, 129));
                        canvas.drawPath(selectedLine, paint);
                    }
                }
            }
        }

        ArrayList<UIBeacon> uiBeacons = floorPlan.getBeacons();
        // Draw beacons
        for (UIBeacon beacon : uiBeacons) {
            canvas.drawBitmap(mBeaconBmp,
                    beacon.getPositionPx().x,
                    beacon.getPositionPx().y, null);
        }

    }

    public void setDrawMode(int drawMode) {
        this.drawMode = drawMode;
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();


        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (isDrawRoom) {
                if (drawMode == SNAP_TO_GRID) {
                    int touchX = (int) x;
                    int touchY = (int) y;

                    int errorY = 100;
                    int errorX = 100;
                    int posX = -1;
                    int posY = -1;

                    for (Point gridPoint : gridPoints) {
                        int currentErrorX = Math.abs(touchX - gridPoint.x);
                        int currentErrorY = Math.abs(touchY - gridPoint.y);
                        if (currentErrorX <= gridCellSizePx
                                && currentErrorY <= gridCellSizePx) {
                            if (currentErrorX + currentErrorY < errorX + errorY) {
                                errorX = currentErrorX;
                                errorY = currentErrorY;
                                posX = gridPoint.x;
                                posY = gridPoint.y;
                            }
                        }
                    }

                    if (posX != -1 && posY != -1) {
                        touchX = posX;
                        touchY = posY;
                    }

                    Log.d("ComputedTouch", "Snap to grid x = " + touchX +
                            " and y = " + touchY);

                    floorPlan.getCorners().add(new UIPoint(touchX, touchY));
                    invalidate();
                } else if (drawMode == FREE_MODE) {
                    floorPlan.getCorners().add(new UIPoint((int) x, (int) y));
                    invalidate();
                } else if (drawMode == KEEP_X) {
                    ArrayList<UIPoint> corners = floorPlan.getCorners();
                    if (corners.size() > 0) {
                        UIPoint previousPoint = corners.get(corners.size() - 1);
                        corners.add(new UIPoint(previousPoint.x, (int) y));
                    }
                    invalidate();
                } else if (drawMode == KEEP_Y) {
                    ArrayList<UIPoint> corners = floorPlan.getCorners();
                    if (corners.size() > 0) {
                        UIPoint previousPoint = corners.get(corners.size() - 1);
                        corners.add(new UIPoint((int) x, previousPoint.y));
                    }
                    invalidate();
                }
            } else if (isBeaconPlacement) {
                int touchX = (int) x - 25;
                int touchY = (int) y - 25;
                UIPoint beaconPosition = new UIPoint(touchX, touchY);
                UIBeacon uiBeacon = new UIBeacon(beaconPosition);
                floorPlan.getBeacons().add(uiBeacon);
                if (floorPlanCanvasInteractionListener != null) {
                    floorPlanCanvasInteractionListener.onBeaconPlacedOnMap(uiBeacon);
                }
                invalidate();
            }
        }

        return true;
    }

}
