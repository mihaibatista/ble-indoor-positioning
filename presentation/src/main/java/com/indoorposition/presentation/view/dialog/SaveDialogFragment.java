package com.indoorposition.presentation.view.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.indoorposition.presentation.R;

/**
 * Created by hayabusa on 06.05.2016.
 */
public class SaveDialogFragment extends DialogFragment {

    public static String SAVE_MAP = "save map";
    public static String SAVE_CSV = "save csv";

    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String title);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    private NoticeDialogListener mListener;
    private String type = SAVE_CSV;

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_save_floor, null);
        final EditText floorPlanEditText = (EditText) view.findViewById(R.id.floor_plan_name);
        String question = "Vreti sa salvati harta?";
        if (type.equals(SAVE_MAP)) {
            question = "Vreti sa salvati harta?";
        } else if (type.equals(SAVE_CSV)) {
            question = "Vreti sa salvati datele colectate in timpul localizarii?";
        }

        builder.setTitle(question)
                .setView(view)
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String floorPlanEditTextString = floorPlanEditText.getText().toString();
                        mListener.onDialogPositiveClick(SaveDialogFragment.this,
                                floorPlanEditTextString);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onDialogNegativeClick(SaveDialogFragment.this);
                    }
                });
        return builder.create();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}
