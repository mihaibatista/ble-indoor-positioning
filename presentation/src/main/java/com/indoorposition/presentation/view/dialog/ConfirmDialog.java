package com.indoorposition.presentation.view.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;

/**
 * Created by hayabusa on 11.06.2016.
 */
public class ConfirmDialog extends DialogFragment {

    public interface BackupDialogListener {
        public void onBackupDialogPositiveClick();
    }

    public interface RestoreDialogListener {
        public void onRestoreDialogPositiveClick();
    }

    public static String CONFIRM_BACKUP = "confirmBackup";
    public static String CONFIRM_RESTORE = "confirmRestore";

    private BackupDialogListener backupDialogListener;
    private RestoreDialogListener restoreDialogListener;

    private String confirmType = CONFIRM_BACKUP;

    public void setConfirmType(String confirmType) {
        this.confirmType = confirmType;
    }

    public Dialog onCreateDialog(Bundle savedInstanceBundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String confirmText = "Doriti sa faceti backup la date?";
        if (confirmType == CONFIRM_BACKUP) {
            confirmText = "Doriti sa faceti backup la date?";
        } else {
            confirmText = "Doriti sa faceti restore la date?";
        }

        builder.setMessage(confirmText)
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (confirmType == CONFIRM_BACKUP) {
                            if (backupDialogListener != null) {
                                backupDialogListener.onBackupDialogPositiveClick();
                            }
                        } else {
                            if (restoreDialogListener != null) {
                                restoreDialogListener.onRestoreDialogPositiveClick();
                            }
                        }
                    }
                })
                .setNegativeButton("Nu", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            backupDialogListener = (BackupDialogListener) activity;
            restoreDialogListener = (RestoreDialogListener) activity;
        } catch (ClassCastException e) {
            Log.e("Exception", e.getMessage(), e);
        }
    }

}
