package com.indoorposition.presentation.view.menu;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.view.canvas.ConfigureFloorPlanCanvas;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class DrawFloorPlanView implements IMenuEntry {

    private AppCompatActivity activity;
    private FloorPlan floorPlan;
    private ConfigureFloorPlanCanvas configureCanvas;
    private FloatingActionButton fabAcceptRoomShape;
    private FloatingActionButton fabUndoDrawRoom;
    private LinearLayout roomDrawDetails;
    private RadioGroup drawStyleRG;
    private PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
    private MenuItem menuItem;

    public DrawFloorPlanView(AppCompatActivity a,
                             FloorPlan fp,
                             ConfigureFloorPlanCanvas configureFloorPlanCanvas,
                             FloatingActionButton fabAcceptRoomShape,
                             FloatingActionButton fabUndoDrawRoom,
                             LinearLayout roomDrawDetails,
                             RadioGroup drawStyleRG) {
        this.activity = a;
        this.floorPlan = fp;
        this.configureCanvas = configureFloorPlanCanvas;
        this.fabAcceptRoomShape = fabAcceptRoomShape;
        this.fabUndoDrawRoom = fabUndoDrawRoom;
        this.roomDrawDetails = roomDrawDetails;
        this.drawStyleRG = drawStyleRG;

        // initialize
        roomDrawDetails.setVisibility(View.GONE);
        fabAcceptRoomShape.setVisibility(View.GONE);
        fabAcceptRoomShape.setOnClickListener(acceptFloorPlanShapeListener);
        fabUndoDrawRoom.setVisibility(View.GONE);
        fabUndoDrawRoom.setOnClickListener(undoFloorPlanShapeListener);
        drawStyleRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.freeDrawRadio) {
                    configureCanvas.setDrawMode(ConfigureFloorPlanCanvas.FREE_MODE);
                } else if (checkedId == R.id.snapToGridRadio) {
                    configureCanvas.setDrawMode(ConfigureFloorPlanCanvas.SNAP_TO_GRID);
                } else if (checkedId == R.id.keepXRadio) {
                    configureCanvas.setDrawMode(ConfigureFloorPlanCanvas.KEEP_X);
                } else if (checkedId == R.id.keepYRadio) {
                    configureCanvas.setDrawMode(ConfigureFloorPlanCanvas.KEEP_Y);
                }
            }
        });
    }

    private View.OnClickListener acceptFloorPlanShapeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            configureCanvas.disableFloorPlanDraw();
            menuItem.getIcon().setColorFilter(Color.BLACK, mode);
            fabAcceptRoomShape.setVisibility(View.GONE);
            fabUndoDrawRoom.setVisibility(View.GONE);
        }
    };

    private View.OnClickListener undoFloorPlanShapeListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            floorPlan.removeLastCorner();
            configureCanvas.refreshCanvas();
        }
    };

    @Override
    public void select() {
        roomDrawDetails.setVisibility(View.VISIBLE);
        configureCanvas.enableFloorPlanDraw();
        menuItem.getIcon().setColorFilter(Color.WHITE, mode);
        fabAcceptRoomShape.setVisibility(View.VISIBLE);
        fabUndoDrawRoom.setVisibility(View.VISIBLE);
    }

    @Override
    public void deselect() {
        menuItem.getIcon().setColorFilter(Color.BLACK, mode);
        configureCanvas.disableFloorPlanDraw();
        fabAcceptRoomShape.setVisibility(View.GONE);
        fabUndoDrawRoom.setVisibility(View.GONE);
        roomDrawDetails.setVisibility(View.GONE);
    }

    @Override
    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
}
