package com.indoorposition.presentation.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.db.dao.FloorPlanDao;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.view.canvas.ConfigureFloorPlanCanvas;
import com.indoorposition.presentation.view.dialog.NumberPickerDialogFragment;
import com.indoorposition.presentation.view.menu.DrawFloorPlanView;
import com.indoorposition.presentation.view.menu.IMenuEntry;
import com.indoorposition.presentation.view.menu.PlaceBeaconsView;
import com.indoorposition.presentation.view.menu.SaveFloorPlanView;
import com.indoorposition.presentation.view.menu.SelectWallView;
import com.indoorposition.presentation.view.dialog.SaveDialogFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hayabusa on 23.04.2016.
 */
public class ConfigureActivity extends AppCompatActivity implements
        SaveDialogFragment.NoticeDialogListener, NumberPickerDialogFragment.CellSizeDialogListener{

    // Draw floor plan
    @Bind(R.id.fab) public FloatingActionButton fabAcceptRoomShape;
    @Bind(R.id.fab_undo) public FloatingActionButton fabUndoDrawRoom;
    @Bind(R.id.room_draw_details) public LinearLayout roomDrawDetails;

    private Menu menu;
    // Menu
    private NavigationElement navigationElement;

    // Beacon details
    @Bind(R.id.beacon_details) public LinearLayout beaconDetails;
    @Bind(R.id.beacon_major) public EditText beaconMajorEditText;
    @Bind(R.id.beacon_minor) public EditText beaconMinorEditText;
    @Bind(R.id.fab2) public FloatingActionButton fabAcceptBeaconIdentification;
    @Bind(R.id.fab_undo_beacon) public FloatingActionButton fabUndoBeaconPlacement;
    @Bind(R.id.beacon_hint_details) public LinearLayout placeBeaconHintDetails;

    // Wall details
    @Bind(R.id.fab1) public FloatingActionButton fabAcceptLineMeasurement;
    @Bind(R.id.wall_dimension_txt) public EditText wallLengthEditText;
    @Bind(R.id.measurements_valid_content) public TextView measurementsContent;
    @Bind(R.id.fab_cancel) public FloatingActionButton fabCancelLineMeasurement;
    @Bind(R.id.wall_title) public TextView wallTitle;
    @Bind(R.id.measurements_details) public LinearLayout measurementsDetails;
    @Bind(R.id.measurements_valid) public LinearLayout measurementsValid;
    @Bind(R.id.radioGroup) public RadioGroup drawStyleRG;

    @Bind(R.id.configure_canvas) public ConfigureFloorPlanCanvas configureCanvas;

    private FloorPlan floorPlan;

    private FloorPlanDao floorPlanDao;

    private IMenuEntry saveFloorPlanEntry;
    private IMenuEntry placeBeaconsEntry;
    private IMenuEntry drawFloorPlanEntry;
    private IMenuEntry selectWallEntry;

    private NumberPickerDialogFragment cellSizeDialogFragment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        FloorPlan floorPlanExtra = (FloorPlan)
                intent.getSerializableExtra("floorPlan");
        if (floorPlanExtra != null) {
            this.floorPlan = floorPlanExtra;
        } else {
            this.floorPlan = new FloorPlan();
        }
        configureCanvas.setFloorPlan(this.floorPlan);
        configureCanvas.setGridCellSizePx(this.floorPlan.getGridCellSizePx());

        saveFloorPlanEntry = new SaveFloorPlanView(this, floorPlan);
        placeBeaconsEntry = new PlaceBeaconsView(this, floorPlan, configureCanvas,
                beaconDetails, beaconMajorEditText, beaconMinorEditText,
                fabAcceptBeaconIdentification, fabUndoBeaconPlacement,
                placeBeaconHintDetails);
        drawFloorPlanEntry = new DrawFloorPlanView(this, floorPlan, configureCanvas,
                fabAcceptRoomShape, fabUndoDrawRoom, roomDrawDetails, drawStyleRG);
        selectWallEntry = new SelectWallView(this, floorPlan, configureCanvas,
                fabAcceptLineMeasurement, wallLengthEditText, measurementsContent,
                fabCancelLineMeasurement, wallTitle, measurementsDetails, measurementsValid);

        // Db
        floorPlanDao = new FloorPlanDao(this.getApplicationContext());

        // Dialog
        cellSizeDialogFragment = new NumberPickerDialogFragment();
        cellSizeDialogFragment.setDefaultCellSize(floorPlan.getGridCellSizePx());

        // Setup menu
        navigationElement = new NavigationElement();
        navigationElement.onCreate(this);


    }

    public void initializeToolbarSelection() {
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
        MenuItem selectLineMenuItem = menu.findItem(R.id.draw_room_btn_menu);
        selectWallEntry.setMenuItem(selectLineMenuItem);
        selectLineMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
        MenuItem drawRoomMenuItem = menu.findItem(R.id.draw_room_btn_menu);
        drawFloorPlanEntry.setMenuItem(drawRoomMenuItem);
        drawRoomMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
        MenuItem placeBeaconsMenuItem = menu.findItem(R.id.place_beacon_menu);
        placeBeaconsEntry.setMenuItem(placeBeaconsMenuItem);
        placeBeaconsMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
        MenuItem saveMenuItem = menu.findItem(R.id.save_map_menu);
        saveFloorPlanEntry.setMenuItem(saveMenuItem);
        saveMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        navigationElement.onPostCreate();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        this.menu = menu;
        initializeToolbarSelection();
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setup_grid_cell_size:
                cellSizeDialogFragment.show(getFragmentManager(),
                        "grid cell frag");
                break;
            case R.id.select_line_menu:
                selectWallEntry.select();

                drawFloorPlanEntry.deselect();
                placeBeaconsEntry.deselect();
                saveFloorPlanEntry.deselect();
                break;
            case R.id.draw_room_btn_menu:
                drawFloorPlanEntry.select();

                placeBeaconsEntry.deselect();
                selectWallEntry.deselect();
                saveFloorPlanEntry.deselect();
                break;
            case R.id.place_beacon_menu:
                placeBeaconsEntry.select();

                drawFloorPlanEntry.deselect();
                selectWallEntry.deselect();
                saveFloorPlanEntry.deselect();
                break;
            case R.id.save_map_menu:
                saveFloorPlanEntry.select();

                break;
            default:
                break;
        }

        if (navigationElement.onOptionsItemSelected(item)) {
            return true;
        }

        return true;
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String title) {
        floorPlan.setName(title);
        Snackbar.make(findViewById(android.R.id.content), "Floor plan " + title + " saved!", Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.WHITE)
                .show();
        floorPlanDao.createOrUpdateFloorPlan(floorPlan);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }

    @Override
    public void onCellDialogPositiveClick(android.app.DialogFragment dialog, int cellSize) {
        this.floorPlan.setGridCellSizePx(cellSize);
        this.configureCanvas.setGridCellSizePx(cellSize);
    }
}
