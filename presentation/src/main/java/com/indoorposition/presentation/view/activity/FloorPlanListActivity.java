package com.indoorposition.presentation.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.db.dao.FloorPlanDao;
import com.indoorposition.presentation.db.dao.IFloorPlanDao;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.view.adapter.FloorPlanAdapter;
import com.indoorposition.presentation.view.dialog.ConfirmDialog;
import com.indoorposition.presentation.view.listener.FloorPlanOperationListener;

import butterknife.ButterKnife;

/**
 * Created by hayabusa on 06.05.2016.
 */
public class FloorPlanListActivity extends AppCompatActivity implements ConfirmDialog.BackupDialogListener,
        ConfirmDialog.RestoreDialogListener {

    private ListView floorPlanListView;
    private FloorPlanAdapter floorPlanAdapter;
    private FloatingActionButton fabCreateFloor;
    private NavigationElement navigationElement;
    private IFloorPlanDao floorPlanDao;
    private ConfirmDialog confirmDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan_list);
        ButterKnife.bind(this);

        floorPlanDao = new FloorPlanDao(this);

        navigationElement = new NavigationElement();
        navigationElement.onCreate(this);

        floorPlanListView = (ListView) findViewById(R.id.floor_plan_list);
        fabCreateFloor = (FloatingActionButton) findViewById(R.id.fab_create_floor);
        fabCreateFloor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent k = new Intent(FloorPlanListActivity.this,
                        ConfigureActivity.class);
                startActivity(k);
            }
        });

        floorPlanAdapter = new FloorPlanAdapter(this.getApplicationContext());
        floorPlanAdapter.setFloorPlanOperationListener(floorPlanOperationListener);
        floorPlanListView.setAdapter(floorPlanAdapter);
        floorPlanAdapter.addFloorPlanList(floorPlanDao.fetchAllFloorPlans());

        confirmDialog = new ConfirmDialog();
    }

    private FloorPlanOperationListener floorPlanOperationListener = new FloorPlanOperationListener() {

        @Override
        public void loadFloorPlan(FloorPlan floorPlan) {
            Intent loadIntent = new Intent(FloorPlanListActivity.this,
                    MainActivity.class);
            loadIntent.putExtra("floorPlan", floorPlan);
            FloorPlanListActivity.this.startActivity(loadIntent);
        }

        @Override
        public void editFloorPlan(FloorPlan floorPlan) {
            Intent editIntent = new Intent(FloorPlanListActivity.this,
                    ConfigureActivity.class);
            editIntent.putExtra("floorPlan", floorPlan);
            FloorPlanListActivity.this.startActivity(editIntent);
        }

        @Override
        public void deleteFloorPlan(FloorPlan floorPlan) {
            floorPlanDao.deleteFloorPlan(floorPlan);
            floorPlanAdapter.deleteItem(floorPlan);
        }

        @Override
        public void trainFloorPlan(FloorPlan floorPlan) {
            Intent trainingIntent = new Intent(FloorPlanListActivity.this,
                    TrainingActivity.class);
            trainingIntent.putExtra("floorPlan", floorPlan);
            FloorPlanListActivity.this.startActivity(trainingIntent);
        }
    };

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        navigationElement.onPostCreate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map_list, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.backup_maps:
                confirmDialog.setConfirmType(ConfirmDialog.CONFIRM_BACKUP);
                confirmDialog.show(getFragmentManager(), "backup maps");
                break;
            case R.id.restore_maps:
                confirmDialog.setConfirmType(ConfirmDialog.CONFIRM_RESTORE);
                confirmDialog.show(getFragmentManager(), "restore maps");
                break;
            default:

                break;
        }

        if (navigationElement.onOptionsItemSelected(item)) {
            return true;
        }

        return true;
    }

    @Override
    public void onBackupDialogPositiveClick() {
        floorPlanDao.backup();
        Snackbar.make(this.findViewById(android.R.id.content),
                "O copie de rezerva a fost creata",
                Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.WHITE)
                .show();
    }

    @Override
    public void onRestoreDialogPositiveClick() {
        floorPlanDao.restore();
        floorPlanAdapter.clearFloorPlanList();
        floorPlanAdapter.addFloorPlanList(
                floorPlanDao.fetchAllFloorPlans());
        Snackbar.make(this.findViewById(android.R.id.content),
                "A fost incarcata copia de rezerva",
                Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.WHITE)
                .show();
    }
}
