package com.indoorposition.presentation.view.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;

import com.indoorposition.presentation.R;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class NumberPickerDialogFragment extends DialogFragment {

    public static final int CELL_SIZE_DIALOG = 0;
    public static final int NO_OF_SAMPLES = 1;

    public interface CellSizeDialogListener {
        public void onCellDialogPositiveClick(DialogFragment dialog, int cellSize);
    }

    public interface NoOfSamplesListener {
        public void onSamplesDialogPositiveClick(DialogFragment dialog, int noOfSamples);
    }

    private int defaultCellSize = 10;
    private int defaultSamplingNo = 5;
    private CellSizeDialogListener cellSizeListener;
    private NoOfSamplesListener noOfSamplesListener;
    private int type = 0;

    public void setDialogType(int type) {
        this.type = type;
    }

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_cell_size, null);
        final NumberPicker cellSizePicker = (NumberPicker) view.findViewById(R.id.cell_size_picker);

        String dialogTitle = "Dimensiunea unei celule din grid (px)";
        if (type == NO_OF_SAMPLES) {
            dialogTitle = "Numarul de sampling-uri per beacon";
            cellSizePicker.setValue(defaultSamplingNo);
            cellSizePicker.setMinValue(1);
            cellSizePicker.setMaxValue(20);
        } else {
            cellSizePicker.setValue(defaultCellSize);
            cellSizePicker.setMinValue(5);
            cellSizePicker.setMaxValue(80);
        }

        builder.setView(view)
                .setTitle(dialogTitle)
                .setPositiveButton("Actualizeaza", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int pickedSize = cellSizePicker.getValue();
                        if (type == CELL_SIZE_DIALOG) {
                            if (cellSizeListener != null) {
                                cellSizeListener.onCellDialogPositiveClick(NumberPickerDialogFragment.this, pickedSize);
                            }
                        } else if (type == NO_OF_SAMPLES) {
                            if (noOfSamplesListener != null) {
                                noOfSamplesListener.onSamplesDialogPositiveClick(NumberPickerDialogFragment.this, pickedSize);
                            }
                        }
                    }
                })
                .setNegativeButton("Anuleaza", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        return builder.create();
    }

    public void setDefaultCellSize(int cellSize) {
        defaultCellSize = cellSize;
    }

    public void setDefaultSamplingNo(int samplingNo) {
        this.defaultSamplingNo = samplingNo;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            cellSizeListener = (CellSizeDialogListener) activity;
            noOfSamplesListener = (NoOfSamplesListener) activity;
        } catch (ClassCastException e) {
            Log.e("Exception", e.getMessage(), e);
        }
    }
}
