package com.indoorposition.presentation.view.menu;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;
import com.indoorposition.presentation.view.listener.FPCanvasInteractionListener;
import com.indoorposition.presentation.view.canvas.ConfigureFloorPlanCanvas;
import com.indoorposition.presentation.view.activity.ConfigureActivity;

import butterknife.ButterKnife;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class PlaceBeaconsView implements IMenuEntry, FPCanvasInteractionListener {

    public LinearLayout beaconDetails;
    public EditText beaconMajorEditText;
    public EditText beaconMinorEditText;
    public FloatingActionButton fabAcceptBeaconIdentification;
    public FloatingActionButton fabUndoBeaconPlacement;
    public LinearLayout placeBeaconHintDetails;

    private AppCompatActivity activity;
    private FloorPlan floorPlan;
    private MenuItem menuItem;
    private PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
    private ConfigureFloorPlanCanvas configureCanvas;

    public PlaceBeaconsView(ConfigureActivity a,
                            FloorPlan fp,
                            ConfigureFloorPlanCanvas configureCanvas,
                            LinearLayout beaconDetails,
                            EditText beaconMajorEditText,
                            EditText beaconMinorEditText,
                            FloatingActionButton fabAcceptBeaconIdentification,
                            FloatingActionButton fabUndoBeaconPlacement,
                            LinearLayout placeBeaconHintDetails) {
        ButterKnife.bind(a);
        this.activity = a;
        this.floorPlan = fp;
        this.configureCanvas = configureCanvas;
        this.beaconDetails = beaconDetails;
        this.beaconMajorEditText = beaconMajorEditText;
        this.beaconMinorEditText = beaconMinorEditText;
        this.fabAcceptBeaconIdentification = fabAcceptBeaconIdentification;
        this.fabUndoBeaconPlacement = fabUndoBeaconPlacement;
        this.placeBeaconHintDetails = placeBeaconHintDetails;
        configureCanvas.addConfigureCanvasListener(this);


        // initialize
        placeBeaconHintDetails.setVisibility(View.GONE);
        beaconDetails.setVisibility(View.GONE);
        fabAcceptBeaconIdentification.setVisibility(View.GONE);
        fabUndoBeaconPlacement.setVisibility(View.GONE);
        fabAcceptBeaconIdentification.setOnClickListener(acceptBeaconIdentificationListener);
        fabUndoBeaconPlacement.setOnClickListener(undoBeaconPlacementListener);
    }

    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }

    private View.OnClickListener acceptBeaconIdentificationListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String beaconMajorString = beaconMajorEditText.getText().toString();
            String beaconMinorString = beaconMinorEditText.getText().toString();
            Integer beaconMajor = -1;
            Integer beaconMinor = -1;
            try {
                beaconMajor = Integer.valueOf(beaconMajorString);
                beaconMinor = Integer.valueOf(beaconMinorString);
            } catch (Exception e) {
                Log.d("Exception", e.getMessage(), e);
            }

            // Validate identification
            if (beaconMajor != - 1 && beaconMinor != -1) {
                floorPlan.setLastBeaconMajorAndMinor(beaconMajor,
                        beaconMinor);
                beaconMajorEditText.setText("");
                beaconMinorEditText.setText("");
                configureCanvas.enableBeaconPlacement();
                beaconDetails.setVisibility(View.GONE);
                fabAcceptBeaconIdentification.setVisibility(View.GONE);
                placeBeaconHintDetails.setVisibility(View.VISIBLE);
            } else {
                Snackbar.make(activity.findViewById(android.R.id.content),
                        "Datele de identificare ale beaconului sunt invalide. " +
                                "Verificati-le si reintroduceti-le!", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        }
    };

    private View.OnClickListener undoBeaconPlacementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            beaconMajorEditText.setText("");
            beaconMinorEditText.setText("");
            configureCanvas.enableBeaconPlacement();
            beaconDetails.setVisibility(View.GONE);
            placeBeaconHintDetails.setVisibility(View.VISIBLE);
            fabAcceptBeaconIdentification.setVisibility(View.GONE);
            floorPlan.removeLastBeacon();
            configureCanvas.refreshCanvas();
            configureCanvas.enableBeaconPlacement();
        }
    };

    @Override
    public void select() {
        configureCanvas.enableBeaconPlacement();
        menuItem.getIcon().setColorFilter(Color.WHITE, mode);
        fabUndoBeaconPlacement.setVisibility(View.VISIBLE);
        placeBeaconHintDetails.setVisibility(View.VISIBLE);
    }

    @Override
    public void deselect() {
        menuItem.getIcon().setColorFilter(Color.BLACK, mode);
        fabAcceptBeaconIdentification.setVisibility(View.GONE);
        fabUndoBeaconPlacement.setVisibility(View.GONE);
        beaconDetails.setVisibility(View.GONE);
        placeBeaconHintDetails.setVisibility(View.GONE);
        floorPlan.validateBeacons();
        configureCanvas.refreshCanvas();
    }

    @Override
    public void onBeaconPlacedOnMap(UIBeacon beacon) {
        fabAcceptBeaconIdentification.setVisibility(View.VISIBLE);
        placeBeaconHintDetails.setVisibility(View.GONE);
        beaconDetails.setVisibility(View.VISIBLE);
        configureCanvas.disableBeaconPlacement();
    }

    @Override
    public void onBeaconSelected(UIBeacon beacon) {

    }

    @Override
    public void onPositionSelected(UIPoint uiPoint) {

    }
}
