package com.indoorposition.presentation.view.menu;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.view.canvas.ConfigureFloorPlanCanvas;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class SelectWallView implements IMenuEntry {

    public FloatingActionButton fabAcceptLineMeasurement;
    public EditText wallLengthEditText;
    public TextView measurementsContent;
    public FloatingActionButton fabCancelLineMeasurement;
    public TextView wallTitle;
    public LinearLayout measurementsDetails;
    public LinearLayout measurementsValid;

    private AppCompatActivity activity;
    private FloorPlan floorPlan;
    private ConfigureFloorPlanCanvas configureCanvas;
    private MenuItem menuItem;
    private PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;

    public SelectWallView(AppCompatActivity a,
                          FloorPlan fp,
                          ConfigureFloorPlanCanvas configureCanvas,
                          FloatingActionButton fabAcceptLineMeasurement,
                          EditText wallLengthET,
                          TextView measurementsCont,
                          FloatingActionButton fabCancelLineMeasurement,
                          TextView wallTitle,
                          LinearLayout measurementsDetails,
                          LinearLayout measurementsValid) {
        this.activity = a;
        this.floorPlan = fp;
        this.configureCanvas = configureCanvas;
        this.fabAcceptLineMeasurement = fabAcceptLineMeasurement;
        this.fabCancelLineMeasurement = fabCancelLineMeasurement;
        this.wallLengthEditText = wallLengthET;
        this.measurementsContent = measurementsCont;
        this.wallTitle = wallTitle;
        this.measurementsDetails = measurementsDetails;
        this.measurementsValid = measurementsValid;

        measurementsValid.setVisibility(View.GONE);
        measurementsDetails.setVisibility(View.GONE);
        fabCancelLineMeasurement.setVisibility(View.GONE);
        fabAcceptLineMeasurement.setVisibility(View.GONE);
        fabAcceptLineMeasurement.setOnClickListener(acceptLineMeasurementListener);
        fabCancelLineMeasurement.setOnClickListener(cancelLineMeasurementListener);
    }

    private View.OnClickListener acceptLineMeasurementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String wallLengthString = wallLengthEditText.getText().toString();
            int wallLenght = -1;
            try {
                wallLenght = Integer.valueOf(wallLengthString);
            } catch (Exception e) {
                Log.d("Exception", e.getMessage(), e);
            }

            if (wallLenght != -1) {
                floorPlan.setWallLength(wallLenght);
                fabCancelLineMeasurement.setVisibility(View.VISIBLE);
                measurementsValid.setVisibility(View.VISIBLE);
                measurementsContent.setText("Peretele " + (floorPlan.getCurrentWall() + 1) + " are " +
                        floorPlan.getCurrentWallLength() + " cm");

                fabAcceptLineMeasurement.setVisibility(View.GONE);
                measurementsDetails.setVisibility(View.GONE);
            } else {
                Snackbar.make(activity.findViewById(android.R.id.content),
                        "Dimensiunea introdusa este invalida. Verificati-o si reintroduceti-o!", Snackbar.LENGTH_LONG)
                        .setActionTextColor(Color.RED)
                        .show();
            }
        }
    };

    private View.OnClickListener cancelLineMeasurementListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            floorPlan.resetScale();
            wallTitle.setText("Perete " + (floorPlan.getCurrentWall() + 1));
            wallLengthEditText.setText("");
            fabAcceptLineMeasurement.setVisibility(View.VISIBLE);
            fabCancelLineMeasurement.setVisibility(View.GONE);
            measurementsDetails.setVisibility(View.VISIBLE);
            measurementsValid.setVisibility(View.GONE);

        }
    };

    @Override
    public void select() {
        if (!floorPlan.isScaleSet()) {
            if (floorPlan.getNoOfWalls() > 0) {
                int wallNo = floorPlan.incrementWallIndex();
                measurementsDetails.setVisibility(View.VISIBLE);
                fabAcceptLineMeasurement.setVisibility(View.VISIBLE);
                menuItem.getIcon().setColorFilter(Color.WHITE, mode);
                wallTitle.setText("Perete " + (wallNo + 1));
                wallLengthEditText.setText("");
            }
        } else {
            floorPlan.incrementWallIndex();
            menuItem.getIcon().setColorFilter(Color.WHITE, mode);
            measurementsValid.setVisibility(View.VISIBLE);
            fabCancelLineMeasurement.setVisibility(View.VISIBLE);
            fabCancelLineMeasurement.setVisibility(View.VISIBLE);
            measurementsValid.setVisibility(View.VISIBLE);
            measurementsContent.setText("Peretele " + (floorPlan.getCurrentWall() + 1)+ " are " +
                    floorPlan.getCurrentWallLength() + " cm");
        }
    }

    @Override
    public void deselect() {
        this.menuItem.getIcon().setColorFilter(Color.BLACK, mode);
        measurementsDetails.setVisibility(View.GONE);
        measurementsValid.setVisibility(View.GONE);
        fabAcceptLineMeasurement.setVisibility(View.GONE);
        fabCancelLineMeasurement.setVisibility(View.GONE);
        floorPlan.resetWallIndex();
    }

    @Override
    public void setMenuItem(MenuItem menuItem) {
        this.menuItem = menuItem;
    }
}
