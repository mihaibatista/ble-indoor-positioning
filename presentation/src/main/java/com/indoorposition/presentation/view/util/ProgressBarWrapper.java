package com.indoorposition.presentation.view.util;

import android.widget.ProgressBar;

import com.indoorposition.presentation.model.UIBeacon;

/**
 * Created by hayabusa on 17.05.2016.
 */
public class ProgressBarWrapper {
    private ProgressBar progressBar;
    private UIBeacon uiBeacon;

    public ProgressBarWrapper(ProgressBar progressBar,
                              UIBeacon uiBeacon) {
        this.progressBar = progressBar;
        this.uiBeacon = uiBeacon;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public UIBeacon getUiBeacon() {
        return uiBeacon;
    }
}
