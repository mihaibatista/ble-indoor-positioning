package com.indoorposition.presentation.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.indoorposition.presentation.R;
import com.indoorposition.presentation.ble.BleConnEvent;
import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.Result;
import com.indoorposition.presentation.model.UIBeacon;
import com.indoorposition.presentation.model.UIPoint;
import com.indoorposition.presentation.presenter.MainActivityPresenter;
import com.indoorposition.presentation.presenter.MainActivityView;
import com.indoorposition.presentation.view.canvas.DisplayFloorPlanCanvas;
import com.indoorposition.presentation.view.dialog.SaveDialogFragment;
import com.indoorposition.presentation.view.listener.FPCanvasInteractionListener;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hayabusa on 02.04.2016.
 */
public class MainActivity extends AppCompatActivity implements MainActivityView, SaveDialogFragment.NoticeDialogListener {

    @Bind(R.id.rl_progress)
    public RelativeLayout rl_progress;
    @Bind(R.id.display_floor_plan_canvas)
    public DisplayFloorPlanCanvas displayfloorPlanCanvas;
    @Bind(R.id.beacon_info_section)
    public CardView beaconInfoSection;
    @Bind(R.id.beacon_info_title)
    public TextView beaconInfoTitle;
    @Bind(R.id.read_beacon_info)
    public Button readBeaconInfoBtn;
    @Bind(R.id.view_rssi_history)
    public Button viewRssiHistoryBtn;
    @Bind(R.id.read_beacon_info_progress)
    public TextView readBeaconInfoProgress;
    @Bind(R.id.read_beacon_info_pb)
    public ProgressBar readBeaconInfoPb;
    @Bind(R.id.read_beacon_info_data)
    public TextView readBeaconInfoData;
    @Bind(R.id.fab_close_beacon_info)
    public FloatingActionButton closeBeaconInfoBtn;
    @Bind(R.id.next_real_position_btn)
    public Button nextPositioningBtn;
    @Bind(R.id.select_real_position_section)
    public ScrollView realPositionSection;
    @Bind(R.id.real_position_info)
    public TextView realPositionInfo;
    @Bind(R.id.real_position_title)
    public TextView realPositionTitle;
    @Bind(R.id.next_collect_point_btn)
    public Button nextCollectPointBtn;
    @Bind(R.id.cancel_and_next_position_btn)
    public Button cancelAndNextPosBtn;
    @Bind(R.id.current_collecting_pont_txt)
    public TextView currentCollectingPointTxt;
    @Bind(R.id.positioningMethodGroup)
    public RadioGroup positioningMethodGroup;

    private FloorPlan floorPlan;
    private Menu menu;

    private int collectingPoint = 0;


    // Read beacon info
    int progressCount = 0;
    int progressMax = 11;

    private MainActivityPresenter mainActivityPresenter;
    private boolean collectData = false;

    private UIPoint computedPosition;
    private UIPoint realPosition;
    private int distance;
    private ArrayList<UIBeacon> beaconsLocating;
    private ArrayList<Integer> realDistanceToBeacons = new ArrayList<>();
    private ArrayList<Integer> computedDistanceToBeacons = new ArrayList<>();
    ArrayList<Result> results = new ArrayList<>();

    // Menu
    private NavigationElement navigationElement;

    private SaveDialogFragment saveDialogFragment;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        FloorPlan floorPlanExtra = (FloorPlan)
                intent.getSerializableExtra("floorPlan");
        if (floorPlanExtra != null) {
            this.floorPlan = floorPlanExtra;
        } else {
            this.floorPlan = new FloorPlan();
        }
        displayfloorPlanCanvas.setFloorPlan(this.floorPlan);
        displayfloorPlanCanvas.setFPCanvasInteractionListener(fpCanvasInteractionListener);

        // Setup menu
        navigationElement = new NavigationElement();
        navigationElement.onCreate(this);

        final PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;

        mainActivityPresenter = new MainActivityPresenter(this, floorPlan);

        readBeaconInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivityPresenter.onButtonClicked((Button) v);
            }
        });

        closeBeaconInfoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                beaconInfoSection.setVisibility(View.GONE);
                closeBeaconInfoBtn.setVisibility(View.GONE);
            }
        });

        nextPositioningBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Result result = new Result(computedPosition, realPosition, distance,
                        beaconsLocating, realDistanceToBeacons, computedDistanceToBeacons);
                results.add(result);

                displayfloorPlanCanvas.resetRealPosition();
                displayfloorPlanCanvas.refreshCanvas();
                mainActivityPresenter.startScanner();
                realPositionSection.setVisibility(View.GONE);
                realPositionInfo.setText("");
                realPositionTitle.setVisibility(View.VISIBLE);
                nextPositioningBtn.setVisibility(View.GONE);
                nextCollectPointBtn.setVisibility(View.GONE);
                cancelAndNextPosBtn.setVisibility(View.GONE);
            }
        });

        nextCollectPointBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ++collectingPoint;
                displayfloorPlanCanvas.nextCollectPosition();
                displayfloorPlanCanvas.resetRealPosition();
                displayfloorPlanCanvas.refreshCanvas();

                mainActivityPresenter.startScanner();
                realPositionSection.setVisibility(View.GONE);
                realPositionInfo.setText("");
                realPositionTitle.setVisibility(View.VISIBLE);
                nextPositioningBtn.setVisibility(View.GONE);
                nextCollectPointBtn.setVisibility(View.GONE);
                cancelAndNextPosBtn.setVisibility(View.GONE);
            }
        });

        cancelAndNextPosBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                displayfloorPlanCanvas.resetCollectedData();
                displayfloorPlanCanvas.refreshCanvas();

                mainActivityPresenter.startScanner();
                realPositionSection.setVisibility(View.GONE);
                realPositionInfo.setText("");
                realPositionTitle.setVisibility(View.VISIBLE);
                nextPositioningBtn.setVisibility(View.GONE);
                nextCollectPointBtn.setVisibility(View.GONE);
                cancelAndNextPosBtn.setVisibility(View.GONE);
            }

        });

        positioningMethodGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.trilaterationRadio) {
                    mainActivityPresenter.setPositioningMethod(MainActivityPresenter.TRILATERATION1_METHOD);
                    displayfloorPlanCanvas.setPositioningMethod(MainActivityPresenter.TRILATERATION1_METHOD);
                    Log.d("Positioning", "Trilateration 1 choosed");
                } else if (checkedId == R.id.trilateration2Radio) {
                    mainActivityPresenter.setPositioningMethod(MainActivityPresenter.TRILATERATION2_METHOD);
                    displayfloorPlanCanvas.setPositioningMethod(MainActivityPresenter.TRILATERATION2_METHOD);
                    Log.d("Positioning", "Trilateration 2 choosed");
                } else if (checkedId == R.id.fingerprintingRadio) {
                    mainActivityPresenter.setPositioningMethod(MainActivityPresenter.FINGERPRINTING);
                    displayfloorPlanCanvas.setPositioningMethod(MainActivityPresenter.FINGERPRINTING);
                    Log.d("Positioning", "Fingerprinting choosed");
                }
            }
        });

        saveDialogFragment = new SaveDialogFragment();

        initialize();
    }

    private FPCanvasInteractionListener fpCanvasInteractionListener = new FPCanvasInteractionListener() {
        @Override
        public void onBeaconPlacedOnMap(UIBeacon beacon) {

        }

        @Override
        public void onBeaconSelected(UIBeacon beacon) {
            closeBeaconInfoBtn.setVisibility(View.VISIBLE);
            beaconInfoSection.setVisibility(View.VISIBLE);
            beaconInfoTitle.setText("Beacon " + beacon.getMajor() + "/" + beacon.getMinor());
            mainActivityPresenter.setSelectedBeacon(beacon);

            if (beacon.getFrequency() > -1) {
                String beaconDataString = "Major: " + beacon.getMajor() + "\n" +
                        "Minor: " + beacon.getMinor() + "\n" +
                        "Battery: " + beacon.getBattery() + "\n" +
                        "Frequency: " + beacon.getFrequency() + "\n" +
                        "Power: " + beacon.getPower() + "\n" +
                        "Hardware: " + beacon.getHardware() + "\n" +
                        "Firmware: " + beacon.getFirmware() + "\n" +
                        "Sysid: " + beacon.getSysId() + "\n";
                readBeaconInfoData.setVisibility(View.VISIBLE);
                readBeaconInfoData.setText(beaconDataString);
            } else {
                readBeaconInfoData.setVisibility(View.GONE);
            }
        }

        @Override
        public void onPositionSelected(UIPoint uiPoint) {
            realPosition = uiPoint;
            distance = floorPlan.getScale().computeDistanceBetweenTwoPointsInCm(computedPosition, realPosition);
            String positioningInfoString = "Pozitia calculata: x = " + computedPosition.x + ", y = " + computedPosition.y +
                    " \nPozitia reala: x = " + realPosition.x + ", y = " + realPosition.y + " \n Distanta: " + distance;
            if (beaconsLocating != null) {
                for (UIBeacon beacon : beaconsLocating) {
                    positioningInfoString += " \n Beacon " + beacon.getMajor() + "/" + beacon.getMinor() + " - distances: ";
                    for (double d : beacon.getDistanceList()) {
                        positioningInfoString += " " + d + " | ";
                    }
                }
            }
            realDistanceToBeacons.clear();
            computedDistanceToBeacons.clear();
            for (UIBeacon b : beaconsLocating) {
                int realDistToBeacon = floorPlan.getScale()
                        .computeDistanceBetweenTwoPointsInCm(b.getPositionPx(), realPosition);
                int computedDistToBeacon = floorPlan.getScale()
                        .computeDistanceBetweenTwoPointsInCm(b.getPositionPx(), computedPosition);
                realDistanceToBeacons.add(realDistToBeacon);
                computedDistanceToBeacons.add(computedDistToBeacon);
            }


            currentCollectingPointTxt.setText("Punctul de colectare curent este " + collectingPoint);

            nextCollectPointBtn.setVisibility(View.VISIBLE);
            nextPositioningBtn.setVisibility(View.VISIBLE);
            cancelAndNextPosBtn.setVisibility(View.VISIBLE);
            realPositionTitle.setVisibility(View.GONE);
            realPositionInfo.setText(positioningInfoString);
        }
    };

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        navigationElement.onPostCreate();
    }

    private void initialize() {
        Log.d("MainActivity", "Initialize presenter");
    }

    public void initializeToolbarSelection() {
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
        MenuItem displayBeaconsMenuItem = menu.findItem(R.id.display_beacons_menu);
        displayBeaconsMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
        MenuItem collectDataMenuItem = menu.findItem(R.id.collect_data_menu);
        collectDataMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
        MenuItem startTrackingMenuItem = menu.findItem(R.id.start_localization_menu);
        startTrackingMenuItem.getIcon().setColorFilter(Color.BLACK, mode);
    }

    public void onResume() {
        super.onResume();
    }

    public void onPause() {
        super.onPause();
    }

    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_display, menu);
        this.menu = menu;
        initializeToolbarSelection();
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        PorterDuff.Mode mode = PorterDuff.Mode.SRC_ATOP;
        switch (item.getItemId()) {
            case R.id.start_localization_menu:
                if (!mainActivityPresenter.toggleScanner()) {
                    item.getIcon().setColorFilter(Color.WHITE, mode);
                } else {
                    item.getIcon().setColorFilter(Color.BLACK, mode);
                }
                break;
            case R.id.collect_data_menu:
                if (collectData) {
                    item.getIcon().setColorFilter(Color.BLACK, mode);
                    collectData = false;
                    displayfloorPlanCanvas.setCollectData(false);
                    if (results.size() > 0) {
                        saveDialogFragment.setType(SaveDialogFragment.SAVE_CSV);
                        saveDialogFragment.show(getSupportFragmentManager(), "restore maps");
                        displayfloorPlanCanvas.setDisplayCollectedData(true);
                        displayfloorPlanCanvas.refreshCanvas();
                    }
                } else {
                    collectingPoint = 1;
                    displayfloorPlanCanvas.resetCollectedData();
                    displayfloorPlanCanvas.setDisplayCollectedData(false);
                    displayfloorPlanCanvas.refreshCanvas();
                    item.getIcon().setColorFilter(Color.WHITE, mode);
                    collectData = true;
                    results.clear();
                    displayfloorPlanCanvas.setCollectData(true);
                }
                break;
            case R.id.display_beacons_menu:
                if (displayfloorPlanCanvas.getBeaconVisibility()) {
                    displayfloorPlanCanvas.setBeaconsVisibility(false);
                    item.getIcon().setColorFilter(Color.BLACK, mode);
                } else {
                    displayfloorPlanCanvas.setBeaconsVisibility(true);
                    item.getIcon().setColorFilter(Color.WHITE, mode);
                }
                break;
            default:
                break;
        }

        if (navigationElement.onOptionsItemSelected(item)) {
            return true;
        }

        return true;
    }

    @Override
    public void displayUserLocation(UIPoint userLocation) {
        if (userLocation != null) {
            if (collectData) {
                realPositionSection.setVisibility(View.VISIBLE);
                mainActivityPresenter.stopScanner();
            }
            computedPosition = userLocation;
            displayfloorPlanCanvas.setUserLocation(userLocation);
            displayfloorPlanCanvas.refreshCanvas();
        }
    }

    @Override
    public void displayDataReadFromBeacon(String beaconData) {
        readBeaconInfoData.setVisibility(View.VISIBLE);
        readBeaconInfoData.setText(beaconData);
        readBeaconInfoProgress.setVisibility(View.GONE);
        readBeaconInfoPb.setVisibility(View.GONE);
    }

    @Override
    public void beaconsUsedForLocating(ArrayList<UIBeacon> beacons) {
        beaconsLocating = beacons;
    }

    @Override
    public void displayBleConnEvent(int event) {
        ++progressCount;
        readBeaconInfoPb.setVisibility(View.VISIBLE);
        readBeaconInfoProgress.setVisibility(View.VISIBLE);
        if (event == BleConnEvent.CONNECTED) {
            readBeaconInfoProgress.setText("Conectat");
        } else if (event == BleConnEvent.CONNECTING) {
            readBeaconInfoProgress.setText("Se conecteaza");
        } else if (event == BleConnEvent.SERVICE_DISCOVERED) {
            readBeaconInfoProgress.setText("Serviciul a fost descoperit");
        } else if (event == BleConnEvent.CHARS_DISCOVERED) {
            readBeaconInfoProgress.setText("Caracteristicile au fost descoperite");
        } else if (event == BleConnEvent.CHAR_READ) {
            readBeaconInfoProgress.setText("Caracteristica citita");
        } else if (event == BleConnEvent.DISCONNECTED) {
            readBeaconInfoProgress.setText("Deconectat");
            readBeaconInfoProgress.setVisibility(View.GONE);
            readBeaconInfoPb.setVisibility(View.GONE);
            progressCount = 0;
        } else if (event == BleConnEvent.BLE_CONN_DONE) {

        }
        double percent = progressCount * 100 / progressMax;
        readBeaconInfoPb.setProgress((int) percent);
    }

    @Override
    public void readBeaconDataSuccess() {
        readBeaconInfoData.setVisibility(View.GONE);
    }

    @Override
    public void readBeaconDataFailed() {
        Snackbar.make(MainActivity.this.findViewById(android.R.id.content),
                "Beaconul nu a fost gasit in proximitate. Scanati si reincercati!", Snackbar.LENGTH_LONG)
                .setActionTextColor(Color.RED)
                .show();
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String title) {
        try {
            Log.d("StoreRes", "Store in CSV file with title " + title);
            Date now = new Date();
            String baseDir = android.os.Environment.getExternalStorageDirectory().getAbsolutePath();
            File folder = new File(baseDir + File.separator + this.getPackageName());
            boolean succes = true;
            if (!folder.exists()) {
                succes = folder.mkdir();
            }
            if (succes) {
                String fileName = title;
                String filePath = folder.getAbsolutePath() + File.separator + fileName + now.getTime() + ".res";
                File f = new File(filePath);
                CSVWriter writer;

                if (f.exists() && !f.isDirectory()) {
                    FileWriter fileWriter = new FileWriter(filePath, true);
                    writer = new CSVWriter(fileWriter);
                } else {
                    writer = new CSVWriter(new FileWriter(filePath));
                }

                List<String[]> data = new ArrayList<String[]>();
                for (Result result : results) {
                    String[] row = new String[3 + 5 * result.getBeaconsUsed().size()];
                    row[0] = result.getComputedPosition().x + ";" +
                            result.getComputedPosition().y;
                    row[1] = result.getRealPosition() != null ? result.getRealPosition().x + ";" +
                            result.getRealPosition().y : "-1;-1";
                    row[2] = String.valueOf(result.getDistance());
                    int index = 0;
                    for (UIBeacon beacon : result.getBeaconsUsed()) {
                        row[3 + index * 5] = beacon.getMajor() + "/" + beacon.getMinor();
                        row[3 + index * 5 + 1] = String.valueOf(beacon.getDistanceList().size());
                        String distanceList = "";
                        for (double d : beacon.getDistanceList()) {
                            distanceList += d + ", ";
                        }
                        row[3 + index * 5 + 2] = distanceList;
                        row[3 + index * 5 + 3] = String.valueOf(result.getRealDistanceToBeacons().get(index));
                        row[3 + index * 5 + 4] = String.valueOf(result.getComputedDistanceToBeacons().get(index));
                        ++index;
                    }
                    data.add(row);

                }

                writer.writeAll(data);

                writer.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
}
