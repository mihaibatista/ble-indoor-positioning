package com.indoorposition.presentation.view.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.UIPoint;

import java.util.ArrayList;

/**
 * Created by hayabusa on 08.05.2016.
 */
public class BasicCanvas extends View {

    protected FloorPlan floorPlan;
    protected ArrayList<Path> grid = new ArrayList<Path>();
    protected ArrayList<Point> gridPoints = new ArrayList<>();
    protected int gridCellSizePx = 30;
    private final static int stroke_width = 5;
    protected Paint paint;
    protected int noOfCellsPerRow = 0;

    public BasicCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(stroke_width);
        floorPlan = new FloorPlan();
    }

    public void setFloorPlan(FloorPlan floorPlan) {
        this.floorPlan = floorPlan;
        invalidate();
    }

    public void refreshCanvas() {
        invalidate();
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw grid
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(2);
        for (Path gridPath : grid) {
            canvas.drawPath(gridPath, paint);
        }

        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(6);

        Path roomPath = new Path();
        ArrayList<UIPoint> corners = floorPlan.getCorners();
        if (corners.size() > 0) {
            roomPath.moveTo(corners.get(0).x,
                    corners.get(0).y);

            for (int i = 1; i < corners.size(); ++i) {
                UIPoint point = corners.get(i);
                roomPath.lineTo(point.x, point.y);
            }

            roomPath.lineTo(corners.get(0).x,
                    corners.get(0).y);
        }

        paint.setColor(Color.BLACK);
        canvas.drawPath(roomPath, paint);

        paint.setColor(Color.argb(100, 51, 181, 229));
        for (UIPoint p : corners) {
            canvas.drawCircle(p.x,
                    p.y, 5, paint);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        computeGrid(getWidth(), getHeight());

    }

    public void setGridCellSizePx(int gridStepPx) {
        gridCellSizePx = gridStepPx;
        computeGrid(getWidth(), getHeight());
        invalidate();
    }

    public int getGridCellSizePx() {
        return gridCellSizePx;
    }

    private void computeGrid(int canvasWidth, int canvasHeight) {
        float xGridPosition = 0;
        ArrayList<Integer> gridXList = new ArrayList<>();
        noOfCellsPerRow = 0;
        gridPoints.clear();
        grid.clear();

        while (xGridPosition < canvasWidth) {
            Path gridPath = new Path();
            gridPath.moveTo(xGridPosition, stroke_width);
            gridPath.lineTo(xGridPosition, canvasHeight - stroke_width);
            grid.add(gridPath);

            gridXList.add((int) xGridPosition);
            ++noOfCellsPerRow;
            xGridPosition += gridCellSizePx;
        }

        float yGridPosition = 0;
        while (yGridPosition < canvasHeight) {
            Path gridPath = new Path();
            gridPath.moveTo(stroke_width, yGridPosition);
            gridPath.lineTo(canvasWidth - stroke_width, yGridPosition);
            grid.add(gridPath);

            for (Integer x : gridXList) {
                gridPoints.add(new Point(x, (int) yGridPosition));
            }

            yGridPosition += gridCellSizePx;
        }
    }

}
