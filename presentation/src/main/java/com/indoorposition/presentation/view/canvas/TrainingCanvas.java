package com.indoorposition.presentation.view.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;

import com.indoorposition.presentation.model.Fingerprint;

/**
 * Created by hayabusa on 16.05.2016.
 */
public class TrainingCanvas extends BasicCanvas {

    private int cellIndex = -1;
    private boolean cellNavigation = false;

    public TrainingCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Point selectFirstCell() {
        cellIndex = 0;
        invalidate();
        int gridIndex = Math.abs(cellIndex % gridPoints.size());
        Point gridIntersection = gridPoints.get(gridIndex);
        return gridIntersection;
    }

    public Point selectRightHorizontalCell() {
        --cellIndex;
        invalidate();
        int gridIndex = Math.abs(cellIndex % gridPoints.size());
        Point gridIntersection = gridPoints.get(gridIndex);
        return gridIntersection;
    }

    public Point selectLeftHorizontalCell() {
        ++cellIndex;
        invalidate();
        int gridIndex = Math.abs(cellIndex % gridPoints.size());
        Point gridIntersection = gridPoints.get(gridIndex);
        return gridIntersection;
    }

    public Point selectDownVerticalCell() {
        cellIndex -= noOfCellsPerRow;
        invalidate();
        int gridIndex = Math.abs(cellIndex % gridPoints.size());
        Point gridIntersection = gridPoints.get(gridIndex);
        return gridIntersection;
    }

    public Point selectUpVerticalCell() {
        cellIndex += noOfCellsPerRow;
        invalidate();
        int gridIndex = Math.abs(cellIndex % gridPoints.size());
        Point gridIntersection = gridPoints.get(gridIndex);
        return gridIntersection;
    }

    public void enableCellNavigation() {
        cellNavigation = true;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint fingerprintPaint = new Paint();
        fingerprintPaint.setStyle(Paint.Style.FILL);

        for (Fingerprint fingerprint : floorPlan.getFingerprints()) {
            if (fingerprint.isReady(floorPlan.getNoOfSamples(), floorPlan.getBeacons().size())) {
                fingerprintPaint.setColor(Color.GREEN);
                canvas.drawRect(fingerprint.getxCell(), fingerprint.getyCell(),
                        fingerprint.getxCell() + gridCellSizePx,
                        fingerprint.getyCell() + gridCellSizePx, fingerprintPaint);
                fingerprintPaint.setColor(Color.BLACK);
                canvas.drawText(String.valueOf(floorPlan.getNoOfDirectionsSampledForFingerprint(fingerprint.getxCell(),
                        fingerprint.getyCell())), fingerprint.getxCellCenter(), fingerprint.getyCellCenter(), fingerprintPaint);
                /*canvas.drawCircle(fingerprint.getxCellCenter(),
                        fingerprint.getyCellCenter(), 10, fingerprintPaint);*/
            }
        }

        if (cellNavigation) {
            int gridIndex = Math.abs(cellIndex % gridPoints.size());
            Point gridIntersection = gridPoints.get(gridIndex);
            paint.setColor(Color.argb(255, 255, 64, 129));

            canvas.drawRect(gridIntersection.x, gridIntersection.y,
                    gridIntersection.x + gridCellSizePx,
                    gridIntersection.y + gridCellSizePx, paint);
        }

    }
}
