package com.indoorposition.presentation.view.activity;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.indoorposition.presentation.R;

/**
 * Created by hayabusa on 07.05.2016.
 */
public class NavigationElement {
    // Menu
    private ListView mDrawerList;
    private ArrayAdapter<String> mAdapter;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private String mActivityTitle;
    private AppCompatActivity activity;

    protected void onCreate(AppCompatActivity activity) {
        // Setup menu
        this.activity = activity;
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerLayout = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
        mActivityTitle = activity.getTitle().toString();
        mDrawerList = (ListView) activity.findViewById(R.id.navList);

        addDrawerItems();
        setupDrawer();
    }

    private void addDrawerItems() {
        String[] osArray = { "Configureaza harta", "Lista de harti" };
        mAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, osArray);
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent k = new Intent(activity,
                            ConfigureActivity.class);
                    activity.startActivity(k);
                } else if (position == 1) {
                    Intent k = new Intent(activity,
                            FloorPlanListActivity.class);
                    activity.startActivity(k);
                }
            }
        });
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(activity, mDrawerLayout,
                R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                activity.getSupportActionBar().setTitle("Meniu");
                activity.invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                activity.getSupportActionBar().setTitle(mActivityTitle);
                activity.invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        } else {
            return false;
        }
    }

    public void onPostCreate() {
        mDrawerToggle.syncState();
    }
}
