package com.indoorposition.cleanapp;

import com.indoorposition.presentation.model.FloorPlan;
import com.indoorposition.presentation.model.UIPoint;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by hayabusa on 05.07.2016.
 */
public class FloorPlanUnitTest {

    @Test
    public void testFloorPlan_ListOfBeacons_EmptyListAtInit() {
        FloorPlan floorPlan = new FloorPlan();

        Assert.assertEquals(0, floorPlan.getBeacons().size());
    }

    @Test
    public void testNoOfWalls() {
        ArrayList<UIPoint> corners = new ArrayList<>();
        corners.add(new UIPoint(1,2));
        corners.add(new UIPoint(4,5));
        FloorPlan floorPlan = new FloorPlan();
        floorPlan.setCorners(corners);

        Assert.assertEquals(1, floorPlan.getNoOfWalls());
    }

    @Test
    public void testWallIndex() {
        FloorPlan floorPlan = new FloorPlan();
        floorPlan.incrementWallIndex();
        floorPlan.incrementWallIndex();
        floorPlan.incrementWallIndex();

        Assert.assertEquals(2, floorPlan.getWallIndex());
    }

    @Test
    public void testRemoveLastCorner() {
        ArrayList<UIPoint> corners = new ArrayList<>();
        corners.add(new UIPoint(1,2));
        corners.add(new UIPoint(4,5));
        corners.add(new UIPoint(7,8));
        FloorPlan floorPlan = new FloorPlan();
        floorPlan.setCorners(corners);
        floorPlan.removeLastCorner();

        Assert.assertEquals(2, floorPlan.getCorners().size());
    }

    @Test
    public void testResetWallIndex() {
        FloorPlan floorPlan = new FloorPlan();
        floorPlan.incrementWallIndex();
        floorPlan.incrementWallIndex();
        floorPlan.incrementWallIndex();
        floorPlan.resetWallIndex();

        Assert.assertEquals(-1, floorPlan.getWallIndex());
    }

}
