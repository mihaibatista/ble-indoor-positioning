package com.indoorposition.domain_android.executor;

import rx.Scheduler;

/**
 * Created by hayabusa on 02.04.2016.
 */
public interface PostExecutionThread {
    Scheduler getScheduler();
}
