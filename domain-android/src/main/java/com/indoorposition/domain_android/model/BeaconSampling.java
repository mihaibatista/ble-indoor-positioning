package com.indoorposition.domain_android.model;

import com.indoorposition.data.ble.IBeacon;

import java.util.ArrayList;

/**
 * Created by hayabusa on 18.04.2016.
 */
public class BeaconSampling {

    private int major;
    private int minor;
    private ArrayList<Integer> rssiList = new ArrayList<>();
    private ArrayList<Double> distanceList = new ArrayList<>();

    public BeaconSampling(int major, int minor) {
        this.setMajor(major);
        this.setMinor(minor);
    }

    public boolean compare(IBeacon beacon) {
        return beacon.getMajor() == getMajor() &&
                beacon.getMinor() == getMinor();
    }

    public void addRssi(int rssi) {
        rssiList.add(rssi);
    }

    public void addDistance(double distance) {
        distanceList.add(distance);
    }


    public int getMajor() {
        return major;
    }

    public void setMajor(int major) {
        this.major = major;
    }

    public int getMinor() {
        return minor;
    }

    public void setMinor(int minor) {
        this.minor = minor;
    }


    public int getSamplingStrength() {
        return rssiList.size();
    }

}
