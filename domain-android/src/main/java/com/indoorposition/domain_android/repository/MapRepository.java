package com.indoorposition.domain_android.repository;


import com.indoorposition.domain_android.FloorMap;

import java.util.List;

import rx.Observable;

/**
 * Created by hayabusa on 03.04.2016.
 */
public interface MapRepository {

    /**
     * Get an {@link rx.Observable} which will emit a List of
     * {@link FloorMap}
     * @return
     */
    Observable<List<FloorMap>> maps();

    Observable<FloorMap> map(final int mapId);

}
