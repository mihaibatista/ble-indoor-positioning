package com.indoorposition.domain_android.interactor;

import android.content.Context;
import android.util.Log;

import com.indoorposition.data.ble.BleScanner;
import com.indoorposition.data.ble.IBeacon;

import java.util.List;

import rx.Observer;

/**
 * Created by hayabusa on 17.04.2016.
 */
public class DetectBeaconsUseCase {

    private BleScanner bleScanner;
    private Context context;
    private StartScannerUseCase startScannerUseCase;

    public DetectBeaconsUseCase(Context context) {
        this.context = context;
        startScannerUseCase = new StartScannerUseCase(context);
    }

    public void execute(Observer<List<IBeacon>> subscriber) {
        Log.d("DetectUse", "Start scanner and set subscriber");
        startScannerUseCase.execute();
        bleScanner = startScannerUseCase.getScanner();
        bleScanner.addBeaconMonitor(subscriber);
    }

}
