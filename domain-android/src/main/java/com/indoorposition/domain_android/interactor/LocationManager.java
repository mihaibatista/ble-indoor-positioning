package com.indoorposition.domain_android.interactor;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.indoorposition.data.ble.IBeacon;
import com.indoorposition.domain_android.model.BeaconSampling;

import java.util.ArrayList;
import java.util.List;

import rx.Observer;

/**
 * Created by hayabusa on 18.04.2016.
 */
public class LocationManager {
    DetectBeaconsUseCase detectBeaconsUseCase;
    ArrayList<BeaconSampling> beaconSampleList = new ArrayList<BeaconSampling>();

    public LocationManager(Context context) {
        detectBeaconsUseCase = new DetectBeaconsUseCase(context);
        refreshSampleList();
    }

    public void refreshSampleList() {
        beaconSampleList.clear();
        BeaconSampling beaconSampling24_7784 = new BeaconSampling(24, 7784);
        BeaconSampling beaconSampling87_61759 = new BeaconSampling(87, 61759);
        BeaconSampling beaconSampling24_9664 = new BeaconSampling(24, 9664);
        BeaconSampling beaconSampling24_8817 = new BeaconSampling(24, 8817);
        beaconSampleList.add(beaconSampling24_7784);
        beaconSampleList.add(beaconSampling87_61759);
        beaconSampleList.add(beaconSampling24_9664);
        beaconSampleList.add(beaconSampling24_8817);
    }

    public void computeLocation() {
        detectBeaconsUseCase.execute(beaconsDetectedObserver);
    }

    private Observer<List<IBeacon>> beaconsDetectedObserver = new Observer<List<IBeacon>>() {

        @Override
        public void onCompleted() {

            Log.d("ComputeLoc", "Beacons detection completed");
        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(List<IBeacon> beaconsList) {
            Log.d("Tril", "No of beacons for computing position is " + beaconsList.size());
            refreshSampleList();
            updateBeaconsData(beaconsList);
            boolean samplingValidForTrilateration = validForTrilateration();
            Log.d("Tril", "Valid for trilateration " + samplingValidForTrilateration);
        }
    };

    private void updateBeaconsData(List<IBeacon> beaconsList) {
        Gson gson = new Gson();
        Log.d("BcnsRcvd", "Beacons received are " + gson.toJson(beaconsList));
        for (IBeacon beacon : beaconsList) {
            for (BeaconSampling b : beaconSampleList) {
                if (b.compare(beacon)) {
                    Log.d("Equal", "Beacons are equal");
                    b.addRssi(beacon.getRssi());
                    b.addDistance(beacon.getAccuracy());
                }
            }
        }
    }

    private boolean validForTrilateration() {
        int validBeacons = 0;
        for (BeaconSampling beaconSampling : beaconSampleList) {
            Log.d("Check", "Beacon stats " + beaconSampling.getMajor() + "/" +
            beaconSampling.getMinor() + " strength " + beaconSampling.getSamplingStrength());


            if (beaconSampling.getSamplingStrength() > 0) {
                ++ validBeacons;
            }
        }

        return validBeacons >= 3;
    }
}
