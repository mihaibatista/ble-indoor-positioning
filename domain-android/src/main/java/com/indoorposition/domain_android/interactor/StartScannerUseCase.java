package com.indoorposition.domain_android.interactor;

import android.content.Context;
import android.util.Log;

import com.indoorposition.data.ble.BleScanner;

/**
 * Created by hayabusa on 17.04.2016.
 */
public class StartScannerUseCase {

    private BleScanner bleScanner;

    public StartScannerUseCase(Context context) {
        bleScanner = new BleScanner(context);
    }

    public BleScanner getScanner() {
        return bleScanner;
    }

    public void execute() {
        Log.d("StartScanner", "Start scanner and look for nearby devices");
        bleScanner.scanLeDevice(true);
    }
}
