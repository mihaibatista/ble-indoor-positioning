package com.indoorposition.domain_android.interactor;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

/**
 * Created by hayabusa on 03.04.2016.
 */
public abstract class UseCase {
    private Subscription subscription = Subscriptions.empty();

    protected UseCase() {
    }

    protected abstract Observable buildUseCaseObservable();

    public void execute(Observer useCaseSubscriber) {
        //Log.d("UseCase", "Execute with sucriber " + useCaseSubscriber);
        Observable observable = this.buildUseCaseObservable();
        //Log.d("UseCase", "Observable to be used is " + observable);
        this.subscription = this.buildUseCaseObservable()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(useCaseSubscriber);
    }

    public void unsubscribe() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }
}
