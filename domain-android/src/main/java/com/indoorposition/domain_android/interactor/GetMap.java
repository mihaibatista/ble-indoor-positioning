package com.indoorposition.domain_android.interactor;

import android.content.Context;

import com.indoorposition.data.entity.MapEntity;
import com.indoorposition.data.repository.MapDataRepository;
import com.indoorposition.data.repository.datasource.MapRepository;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by hayabusa on 03.04.2016.
 */
public class GetMap extends UseCase {

    private final int mapId;
    private final MapRepository mapRepository;

    public GetMap(int mapId, Context context) {
        super();
        //Log.d("GetMapUseCase", "Constructor");
        this.mapId = mapId;
        mapRepository = new MapDataRepository(context);
    }

    @Override
    protected Observable buildUseCaseObservable() {
        //Log.d("GetMapUseCase", "Data to be sent to view");
        Observable<MapEntity> mapEntityObservable = mapRepository.map(mapId);
        mapEntityObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<MapEntity>() {

                    @Override
                    public void call(MapEntity mapEntity) {
                        //Log.d("GetMapUseCase", "Action executed with map entity " + mapEntity);
                    }
                });
        return this.mapRepository.map(mapId);
    }
}
